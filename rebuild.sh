#!/bin/bash
export CANMODULE_AS_STATIC_AS_POSSIBLE=false
CMAKEBIN=`which cmake`

# out of source re-build
#${CMAKEBIN} -S . -B ./build -DCMAKE_TOOLCHAIN_FILE=gitlab_cal9.cmake .
cd ./build
#${CMAKEBIN} --build .
make
cd ..
