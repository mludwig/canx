set CMAKEBIN=cmake.exe
set CANMODULE_AS_STATIC_AS_POSSIBLE=false
%CMAKEBIN% -S . -B ./build -DCMAKE_TOOLCHAIN_FILE=build_w2022s.cmake . -G "Visual Studio 17 2022" && cd ./build && %CMAKEBIN% --build .
