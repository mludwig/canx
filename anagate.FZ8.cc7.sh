#!/bin/bash
#  run all CAN tests
# need all bridges from all vendors connected
#
#ip_anaduo="128.141.159.194"
#ip_anax4="128.141.159.212"
#id_peak0="8910"
#id_peak1="9054"
# must cite these as strings, can' use bash vars , to make the parallel multitask shells work

echo "== basic per vendor tests uni_mono_reserved0  =="
read -p "any key to skip : " -t 3 -n 1 skip
if [[ $? == 0 ]]; then
    	echo " ... skipping ..."
else
	echo " doing uni_mono_reserved0"
	echo "== anagate 128.141.159.194 128.141.159.212=="
	rm -f uni_mono_reserved0.cc7.anagate.txt
	./CANX-tester -T -uni_mono_reserved0 anagate 128.141.159.194 128.141.159.212  > uni_mono_reserved0.cc7.anagate.txt
	#
	echo "== peak =="
	rm -f uni_mono_reserved0.cc7.peak.txt
	./CANX-tester -T -uni_mono_reserved0 peak 8910 9054 > uni_mono_reserved0.cc7.peak.txt
	#
	echo "== systec =="
	rm -f uni_mono_reserved0.cc7.systec.txt
	./CANX-tester -T -uni_mono_reserved0 systec 0 0 > uni_mono_reserved0.cc7.systec.txt
fi

#
#
echo "== performance per vendor tests uni_mono_reserved1 =="
read -p "any key to skip : " -t 3 -n 1 skip
if [[ $? == 0 ]]; then
    	echo " ... skipping ..."
else
	echo "== anagate 128.141.159.194 128.141.159.212=="
	rm -f uni_mono_reserved1.cc7.anagate.txt
	./CANX-tester -T -uni_mono_reserved1 anagate 128.141.159.194 128.141.159.212 > uni_mono_reserved1.cc7.anagate.txt
	#
	echo "== peak =="
	rm -f uni_mono_reserved1.cc7.peak.txt
	./CANX-tester -T -uni_mono_reserved1 peak 8910 9054 > uni_mono_reserved1.cc7.peak.txt
	#
	echo "== systec =="
	rm -f uni_mono_reserved1.cc7.systec.txt
	./CANX-tester -T -uni_mono_reserved1 systec 0 0 > uni_mono_reserved1.cc7.systec.txt
fi
#
#
echo "== mixing all vendors mono-task test mixed_mono_reserved0 =="
read -p "any key to skip : " -t 3 -n 1 skip
if [[ $? == 0 ]]; then
    	echo " ... skipping ..."
else
	rm -f mixed_mono_reserved0.cc7.txt
	./CANX-tester -T -mixed_mono_reserved0 128.141.159.194 > mixed_mono_reserved0.cc7.txt
fi
#
#
echo "== sharing vendor between multi-task tests, per vendor uni_multi_shared0, reconnection  =="
read -p "any key to skip : " -t 3 -n 1 skip
if [[ $? == 0 ]]; then
    	echo " ... skipping ..."
else
	# SCENARIO 0
	# anagate
	echo "== anagate scenario0 128.141.159.194 128.141.159.212=="
	rm -f uni_multi_shared0.cc7.anagate.task0.scenario0.txt
	rm -f uni_multi_shared0.cc7.anagate.task1.scenario0.txt
	gnome-terminal -- bash -c   'echo "starting task0, wait for window to close";./CANX-tester -T -uni_multi_shared0 anagate 128.141.159.194 0 128.141.159.212 0 0 > uni_multi_shared0.cc7.anagate.task0.scenario0.txt' &
	gnome-terminal -- bash -c   'echo "starting task1, wait for window to close";./CANX-tester -T -uni_multi_shared0 anagate 128.141.159.194 1 128.141.159.212 1 0 >  uni_multi_shared0.cc7.anagate.task1.scenario0.txt' &
	sleep 10
	echo "==>> anagate  POWER OFF <<=="
	sleep 10
	echo "==>> anagate POWER ON 128.141.159.194 <<=="
	sleep 30

	#
	# peak
	echo "== peak scenario0 =="
	rm -f uni_multi_shared0.cc7.peak.task0.scenario0.txt
	rm -f uni_multi_shared0.cc7.peak.task1.scenario0.txt
	gnome-terminal -- bash -c   'echo "starting task0, wait for window to close";./CANX-tester -T -uni_multi_shared0 peak 8910 0 9054 0 0 | tee  uni_multi_shared0.cc7.peak.task0.scenario0.txt' &
	gnome-terminal -- bash -c   'echo "starting task1, wait for window to close";./CANX-tester -T -uni_multi_shared0 peak 8910 1 9054 1 0 | tee uni_multi_shared0.cc7.peak.task1.scenario0.txt' &
	sleep 10
	echo "==>> peak POWER OFF <<=="
	sleep 20
	echo "==>> peak  POWER ON <<=="
	sleep 30
	#
	# systec
	echo "== systec scenario0 =="
	rm -f uni_multi_shared0.cc7.systec.task0.scenario0.txt
	rm -f uni_multi_shared0.cc7.systec.task1.scenario0.txt
	gnome-terminal -- bash -c   'echo "starting task0, wait for window to close";./CANX-tester -T -uni_multi_shared0 systec 0 0 0 8 0 | tee uni_multi_shared0.cc7.peak.task0.scenario0.txt' &
	gnome-terminal -- bash -c   'echo "starting task1, wait for window to close";./CANX-tester -T -uni_multi_shared0 systec 0 1 0 9 0 | tee uni_multi_shared0.cc7.peak.task1.scenario0.txt' &
	sleep 10
	echo "==>> systec POWER OFF <<=="
	sleep 10
	echo "==>> systec  POWER ON <<=="
	sleep 30
fi
# SCENARIO 1
# anagate
echo "== sharing vendor between multi-task tests, per vendor, reconnection =="
read -p "any key to skip : " -t 3 -n 1 skip
if [[ $? == 0 ]]; then
    	echo " ... skipping ..."
else
	echo "== anagate scenario1 128.141.159.194 128.141.159.212=="
	rm -f uni_multi_shared0.cc7.anagate.task0.scenario1.txt
	rm -f uni_multi_shared0.cc7.anagate.task1.scenario1.txt
	gnome-terminal -- bash -c   'echo "starting task0, wait for window to close";./CANX-tester -T -uni_multi_shared0 anagate 128.141.159.194 0 128.141.159.212 0 1 | tee uni_multi_shared0.cc7.anagate.task0.scenario1.txt' &
	gnome-terminal -- bash -c   'echo "starting task1, wait for window to close";./CANX-tester -T -uni_multi_shared0 anagate 128.141.159.194 1 128.141.159.212 1 1 | tee uni_multi_shared0.cc7.anagate.task1.scenario1.txt' &
	sleep 10
	echo "==>> anagate  POWER OFF <<=="
	sleep 10
	echo "==>> anagate POWER ON 128.141.159.194 <<=="
	sleep 30
	#
	# peak
	echo "== peak scenario1 =="
	rm -f uni_multi_shared0.cc7.peak.task0.scenario1.txt
	rm -f uni_multi_shared0.cc7.peak.task1.scenario1.txt
	gnome-terminal -- bash -c   'echo "starting task0, wait for window to close";./CANX-tester -T -uni_multi_shared0 peak 8910 0 9054 0 1 | tee uni_multi_shared0.cc7.peak.task0.scenario1.txt' &
	gnome-terminal -- bash -c   'echo "starting task1, wait for window to close";./CANX-tester -T -uni_multi_shared0 peak 8910 1 9054 1 1 | tee uni_multi_shared0.cc7.peak.task1.scenario1.txt' &
	sleep 10
	echo "==>> peak POWER OFF <<=="
	sleep 20
	echo "==>> peak  POWER ON <<=="
	sleep 30
	#
	# systec
	echo "== systec scenario1 =="
	rm -f uni_multi_shared0.cc7.systec.task0.scenario1.txt
	rm -f uni_multi_shared0.cc7.systec.task1.scenario1.txt
	gnome-terminal -- bash -c   'echo "starting task0, wait for window to close";./CANX-tester -T -uni_multi_shared0 systec 0 0 0 8 1 | tee uni_multi_shared0.cc7.peak.task0.scenario1.txt' &
	gnome-terminal -- bash -c   'echo "starting task1, wait for window to close";./CANX-tester -T -uni_multi_shared0 systec 0 1 0 9 1 | tee uni_multi_shared0.cc7.peak.task1.scenario1.txt' &
	sleep 10
	echo "==>> systec POWER OFF <<=="
	sleep 10
	echo "==>> systec  POWER ON <<=="
	sleep 30
fi


# SCENARIO 2
# anagate
echo "== sharing vendor between multi-task tests, per vendor, reconnection =="
read -p "any key to skip : " -t 3 -n 1 skip
if [[ $? == 0 ]]; then
    	echo " ... skipping ..."
else
	echo "== anagate scenario2 128.141.159.194 128.141.159.212=="
	rm -f uni_multi_shared0.cc7.anagate.task0.scenario2.txt
	rm -f uni_multi_shared0.cc7.anagate.task1.scenario2.txt
	gnome-terminal -- bash -c   'echo "starting task0, wait for window to close";./CANX-tester -T -uni_multi_shared0 anagate 128.141.159.194 0 128.141.159.212 0 2 | tee uni_multi_shared0.cc7.anagate.task0.scenario2.txt' &
	gnome-terminal -- bash -c   'echo "starting task1, wait for window to close";./CANX-tester -T -uni_multi_shared0 anagate 128.141.159.194 1 128.141.159.212 1 2 | tee uni_multi_shared0.cc7.anagate.task1.scenario2.txt' &
	sleep 10
	echo "==>> anagate  POWER OFF <<=="
	sleep 10
	echo "==>> anagate POWER ON 128.141.159.194 <<=="
	sleep 30
	#
	# peak
	echo "== peak scenario2 =="
	rm -f uni_multi_shared0.cc7.peak.task0.scenario2.txt
	rm -f uni_multi_shared0.cc7.peak.task1.scenario2.txt
	gnome-terminal -- bash -c   'echo "starting task0, wait for window to close";./CANX-tester -T -uni_multi_shared0 peak 8910 0 9054 0 2 | tee uni_multi_shared0.cc7.peak.task0.scenario2.txt' &
	gnome-terminal -- bash -c   'echo "starting task1, wait for window to close";./CANX-tester -T -uni_multi_shared0 peak 8910 1 9054 1 2 | tee uni_multi_shared0.cc7.peak.task1.scenario2.txt' &
	sleep 10
	echo "==>> peak POWER OFF <<=="
	sleep 10
	echo "==>> peak  POWER ON <<=="
	sleep 30
	#
	# systec
	echo "== systec scenario2 =="
	rm -f uni_multi_shared0.cc7.systec.task0.scenario2.txt
	rm -f uni_multi_shared0.cc7.systec.task1.scenario2.txt
	gnome-terminal -- bash -c   'echo "starting task0, wait for window to close";./CANX-tester -T -uni_multi_shared0 systec 0 0 0 8 2 | tee uni_multi_shared0.cc7.peak.task0.scenario2.txt' &
	gnome-terminal -- bash -c   'echo "starting task1, wait for window to close";./CANX-tester -T -uni_multi_shared0 systec 0 1 0 9 2 | tee uni_multi_shared0.cc7.peak.task1.scenario2.txt' &
	sleep 10
	echo "==>> systec POWER OFF <<=="
	sleep 10
	echo "==>> systec  POWER ON, WAIT for test to finish until windows close <<=="
	sleep 30
fi



#
# disabled test, not really needed
exit
# uni_multi_shared1
echo "== sharing vendor between multi-task tests, per vendor, 2 bridges uni_multi_shared1 =="
read -p "any key to skip : " -t 3 -n 1 skip
if [[ $? == 0 ]]; then
    	echo " ... skipping ..."
else
	# anagate
	echo "== anagate 128.141.159.194 128.141.159.212=="
	rm -f uni_multi_shared1.cc7.anagate.task0.txt
	rm -f uni_multi_shared1.cc7.anagate.task1.txt
	gnome-terminal -- bash -c   'echo "starting task0, wait for window to close";./CANX-tester -T -uni_multi_shared1 anagate 0 128.141.159.194 128.141.159.212 | tee uni_multi_shared1.cc7.anagate.task0.txt' &
	gnome-terminal -- bash -c   'echo "starting task1, wait for window to close";./CANX-tester -T -uni_multi_shared1 anagate 1 128.141.159.194 128.141.159.212 | tee uni_multi_shared1.cc7.anagate.task1.txt' &
	sleep 10
	echo "==>> anagate  POWER OFF <<=="
	sleep 10
	echo "==>> anagate POWER ON 128.141.159.194 <<=="
	sleep 30
	#
	# peak
	echo "== peak =="
	rm -f uni_multi_shared1.cc7.peak.task0.txt
	rm -f uni_multi_shared1.cc7.peak.task1.txt
	gnome-terminal -- bash -c   'echo "starting task0, wait for window to close";./CANX-tester -T -uni_multi_shared1 peak 0 8910 9054 | tee uni_multi_shared1.cc7.peak.task0.txt' &
	gnome-terminal -- bash -c   'echo "starting task1, wait for window to close";./CANX-tester -T -uni_multi_shared1 peak 1 8910 9054 | tee uni_multi_shared1.cc7.peak.task1.txt' &
	sleep 10
	echo "==>> peak POWER OFF <<=="
	sleep 20
	echo "==>> peak  POWER ON <<=="
	sleep 30
	#
	# systec
	echo "== systec =="
	rm -f uni_multi_shared1.cc7.systec.task0.txt
	rm -f uni_multi_shared1.cc7.systec.task1.txt
	gnome-terminal -- bash -c   'echo "starting task0, wait for window to close";./CANX-tester -T -uni_multi_shared1 systec 0 0 0 | tee uni_multi_shared1.cc7.peak.task0.txt' &
	gnome-terminal -- bash -c   'echo "starting task1, wait for window to close";./CANX-tester -T -uni_multi_shared1 systec 1 0 0 | tee uni_multi_shared1.cc7.peak.task1.txt' &
	sleep 10
	echo "==>> systec POWER OFF <<=="
	sleep 10
	echo "==>> systec  POWER ON <<=="
fi


echo on


