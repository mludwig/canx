==========
Quickstart
==========

CANX-tester help 
----------------


2023-07-20 10:44:40.078418 [main.cpp:268, INF, CANX] ./build/bin/CANX-tester2.1.2 Jul 20 2023 10:43:37 logging for component CANX
2023-07-20 10:44:40.078577 [main.cpp:277, INF, CanModule]  CanModule version 2.1.2 logging for component CanModule
/home/mludwig/projects/canx-devel2.2.1/src/main.cpp 289 ***  LogIt component CANX level= 0
/home/mludwig/projects/canx-devel2.2.1/src/main.cpp 289 ***  LogIt component CanModule level= 0
CANX-tester: send and receive CAN messages on ports,
generate specific multi-threaded scenarios and test sequences.
cross platform, multiple vendors.
USAGE:
 loglevel: [-T,-D,-I,-W,-E] = T(TRC), D(DBG), I(INF), W(WRN), E(ERR)
 configuration: [-cfg <config.xml>]
 special commands: [-anagateSoftwareReset <ip>]
 -uni_mono_reserved0 [anagate <ip0> <ip1> | systec 0 0 | peak <ext.id0> <ext.id1> ]
 -uni_mono_reserved1 [anagate <ip0> <ip1> | systec 0 0 | peak <ext.id0> <ext.id1> ]
 -mixed_mono_reserved0 <ip0>
 -uni_multi_shared0 [anagate <ip0> <port0> <ip1> <port1> | systec <port0> <port1> | peak <ext.id0> <port0> <ext.id1> <port1> ]
 -uni_multi_shared1 [anagate <ip0> <port0> <ip1> <port1> | systec <port0> <port1> | peak <ext.id0> <port0> <ext.id1> <port1> ]
 -uni_mono_errors [anagate <ip0> | systec | peak <ext.id0> ]
 ===VENDOR SPECIFIC ANAGATE===
 -anagate0 <ip0> <ip1> : anagate specific test 2 bridges each CAN0

 -api_canaccess [anagate <ip0> <ip1> | systec 0 0 | peak <ext.id0> <ext.id1> ]
 -uni_mono_signals [ anagate | systec | peak ] ip0 : error handler tests on one bridge.
 -pingpong [ anagate | anagate2 | systec | peak ] ip0 ip1 parameters portA portB delay_us kmax: torture pingpong on 2 ports with optional delay between sending.
[mludwig@localhost canx-devel2.2.1]$ 

standard mode
-------------
CANX-tester is run with a config.xml (-cfg) file as configuration input. This is described in the testing paragraph.

special mode
------------
CANX-tester is run with specific command-line parameters, which call specifically coded test methods to run specialized tests which can ot easily be standardized. Usually you need to look into the code to understand what a special test actually does, sometimes multi-tasked runing is needed as well.
