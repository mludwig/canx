=======
Testing
=======

CanModule VALIDATION tests are done in the lab using real physical CAN bridges 
from all vendors connected to physical workstations with all the supported
OS.

Tests are reported as "validation reports" in EDMS under https://edms.cern.ch/document/2786844/1
Each validation checks a specific aspect of CanModule, a bridge, vendor, OS version, firmware and API version.

config.xml
==========

An xml file is loaded which specifies the vendor(s) and port(s), the data which is sent, data reception options, inside a synchronizing and repetition grid.
This permits to configure complex test in a repeatable way: each test has it's own testX.xml file (use the -cfg flag to specify the filename).

The format of the xml is in fact simpler as it sounds. Everything is subject to central and global clock ticks which have all the same duration. The following snippet will produce three ticks with a duration of 1100ms each without any repetion (a single beam with 3 bunches if you like metaphores). Please also use a validating xml-editor
against the config/Configuration.xsd : copy the Configuration.xsd into the same directory as your config.xml .

global section
--------------

.. code-block:: xml
   
   <global 
	nbTicks="3" 
	tickTime="1100"
	repeat="0"
    />

Here we specify 3 clock ticks which each last for 1100ms and which are repeated 3 times in a sequence. When the sequence is executed CANX-tester stops.

- nbTicks: int [1... maxint32]
- tickTime: int [1... maxint32] in milliseconds
- repeat: int [0... maxint32], 0 means "single shot"


A number of threads are executed inside each tick, and all threads are synchronised together at the tick end. Executing inside a tick is asynchronous. Each thread corresponds to one CAN port communication. A port has three configuration sections: thread for the hardware module, for sending and for receiving. The thread section is therefore vendor-implementation specific.

anagate[2] thread section
-------------------------

Here is an xml-snippet for one anagate bridge (which used vendor-implementation "anagate2" here) :

.. code-block:: xml
   
   ...
   <thread port="0" bitrate="250000"  threadIndex="0" moduleType="anagate2" ana_ip="128.141.159.194"
	ana_operationMode="0" 
	ana_termination="1" 
	ana_highSpeed="0" 
	ana_timeStamp="0" 
	ana_syncMode="1"
	ana_timeout="6000" >
	...
   ...

The thread module section is vendor specific of course, but it also -explicitly- specifies a thread number which must be unique. Here we are setting configuring a thread (which is a CAN port) for the implementation "anagate2": 
- port: the CAN port number, start counting from 0
- bitrate: in bits/s, the hardware bitrate which must be the same for all nodes the whole CAN bus, otherwise the comms will not work at all
- ana_termination: specific SW control for anagate, termination resistance, boolean
- ana_highSpeed: specific anagate, boolean, should be true for >= 250000 bitrate (but is checked and corrected by CanModule)
- ana_timeStamp: specific anagate, boolean, see anagate documentation (it is unused for us)
- ana_syncMode: specific anagate, boolean, see anagate documentation (leave it on, but it is unused for us)
- ana_timeout: specific anagate, unsigned int, millisecond delay before the module considers itself disconnected, see anagate documentation. 6 seconds is a good value



peak thread/hardware module section
-----------------------------------
.. code-block:: xml

   <thread port="0" bitrate="125000"  threadIndex="0" moduleType="peak" pk_usb="0">
   ...

the peak CAN port is 0 and we are talking to USB0 as well, in this case. This is evident, I hope.
The parameters for anagate, "ana_" are unused in this case (set them to 0).

systec thread/hardware module section
-------------------------------------
.. code-block:: xml

   <thread port="0" bitrate="125000"  threadIndex="0" moduleType="systec" st_usb="0">
   ...

the systec CAN port is 0 and we are talking to USB0 as well, in this case. This is also evident, I hope.
The parameters for anagate, "ana_" are unused in this case (set them to 0).


sender section
--------------

each thread has a sender section (and a receiver, see further down).

.. code-block:: xml

   <sender 
	messageGeneration="counting" 
	generatedNbFramesPerTick="1000"	
	waitBetweenFrames="1000"
	messageStandard="standard"
	enabled="true"
   ></sender>

CANX-tester can generate CAN standard frames in five modes: { MSG_RANDOM=0, MSG_COUNTING, MSG_FIXED, MSG_ASCONFIGURED, MSG_UNUSED } 

- random: the id varies randomly within the interval [0..2048], the data payload is a random as well within hexadecimal syntax limits.
- counting: the id increases (and cycles) within the interval [0..128], with the first 4 bits as thread number. The data payload 
  increases (and cycles) within hexadecimal syntax limits.
- fixed: a hardcoded fixed frame  "001.0.0.0.8.ff00ff11ff22ff33.0000.1" is sent without modifications
- as configured: the frame contents for each frame are specified for each clock tick. This looks like:

.. code-block:: xml

	<sender 
	messageGeneration="asConfigured" 
	generatedNbFramesPerTick="0"	
	waitBetweenFrames="1000"
	messageStandard="standard"
	enabled="true">
		<tick index="0">
			<!-- the frame is specified in human readable format using chars [a-f][0-9] and dots to delimit for readability:
			35 chars: ID.RTR.IDE.R0.DLC.DATA.CRC.ACK 
			this gives e.g. as hex: "700.0.0.0.8.00ff33ff55ff77.8000.1.1"
			we have 8 frames here -->
			<frame>"700.0.0.0.8.00ff33ff55ff77.8000.1.1"</frame>
			<frame>"701.0.0.0.8.00ff33ff55ff77.8000.1.1"</frame>
			<frame>"702.0.0.0.8.00ff33ff55ff77.8000.1.1"</frame>
			<frame>"703.0.0.0.8.00ff33ff55ff77.8000.1.1"</frame>
			<frame>"704.0.0.0.8.00ff33ff55ff77.8000.1.1"</frame>
			<frame>"705.0.0.0.8.00ff33ff55ff77.8000.1.1"</frame>
			<frame>"706.0.0.0.8.00ff33ff55ff77.8000.1.1"</frame>
			<frame>"707.0.0.0.8.00ff33ff55ff77.8000.1.1"</frame>
		</tick>
		<tick index="1">
			<!-- the frame is specified in human readable format using chars [a-f][0-9] and dots to delimit for readability:
			35 chars: ID.RTR.IDE.R0.DLC.DATA.CRC.ACK 
			this gives e.g. as hex: "700.0.0.0.8.00ff33ff55ff77.8000.1.1" 
			also 8 frames, but with different IDs-->
			<frame>"600.0.0.0.8.00ff33ff55ff77.8000.1.1"</frame>
			<frame>"601.0.0.0.8.00ff33ff55ff77.8000.1.1"</frame>
			<frame>"602.0.0.0.8.00ff33ff55ff77.8000.1.1"</frame>
			<frame>"603.0.0.0.8.00ff33ff55ff77.8000.1.1"</frame>
			<frame>"604.0.0.0.8.00ff33ff55ff77.8000.1.1"</frame>
			<frame>"605.0.0.0.8.00ff33ff55ff77.8000.1.1"</frame>
			<frame>"606.0.0.0.8.00ff33ff55ff77.8000.1.1"</frame>
			<frame>"607.0.0.0.8.00ff33ff55ff77.8000.1.1"</frame>
		</tick>
		<tick index="2">
		<!-- the frame is specified in human readable format using chars [a-f][0-9] and dots to delimit for readability:
		35 chars: ID.RTR.IDE.R0.DLC.DATA.CRC.ACK 
		this gives e.g. as hex: "700.0.0.0.8.00ff33ff55ff77.8000.1.1" 
		only 2 frames inside this clock tick. Once they are executed nothing happens while the tick waits 
		for the next synchronisation-->
			<frame>"500.0.0.0.8.00ff33ff55ff77.8000.1.1"</frame>
			<frame>"501.0.0.0.8.00ff33ff55ff77.8000.1.1"</frame>
		</tick>
	</sender>	







examples
--------
.. literalinclude:: ../config/anagatetest.xml
   :language: xml
   :lineos:
   
