.. _contents:

CANX developer documentation
============================

CANX is a cross-platform CanModule tester, which can be used for lab testing of hardware and CAN bridges.

- w2019s, w2022s
- CC7, CAL9
- Ubuntu (unsupported)
    
.. toctree::
   :maxdepth: 3

   quickstart
   overview
   configuration
   building
   running
   testing  
   support


alphabetical index
------------------

* :ref:`genindex`

.. * :ref:`search`
.. * :ref:`modindex`


