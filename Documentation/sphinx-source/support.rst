=======
Support
=======

Support for the server is given CERN wide.

**Problems, Issues and Requests** should be created as CERN `Jira`_ Tickets:

* Project= OPC UA in JCOP (OPCUA)
* Components= CANX
* Assignee= Michael Ludwig

Please provide traces of your situation and information about your project context. We will sort it out together.

More **personal** ways to get help or report problems:

* You can send me an `Email`_ or call me 163095 or visit me for a coffee.


.. _Jira: https://its.cern.ch/jira/secure/Dashboard.jspa
.. _Email: mailto:michael.ludwig@cern.ch?subject=CANX_issue_found&body=Hello Michael,