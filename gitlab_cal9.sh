#!/bin/bash
export CANMODULE_AS_STATIC_AS_POSSIBLE=false
CMAKEBIN=`which cmake`

# out of source build
${CMAKEBIN} -S . -B ./build -DCMAKE_TOOLCHAIN_FILE=gitlab_cal9.cmake .
cd ./build && ${CMAKEBIN} --build .
#
# cp bins and libs to ./run
cd ../
echo "--- build is finished, copy bins to ./run---"
mkdir -p ../run
find ./ -name "CANX-tester" -exec cp -v {} ./run \;
find ./ -name "*.so" -exec cp -v {} ./run \;
find ./ -name "*.lib" -exec cp -v {} ./run \;

