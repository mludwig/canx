#!/bin/bash
export CANMODULE_AS_STATIC_AS_POSSIBLE=false
CMAKEBIN=`which cmake`

# out of source build
${CMAKEBIN} -S . -B ./build -DCMAKE_TOOLCHAIN_FILE=gitlab_cs9.cmake .
cd ./build && ${CMAKEBIN} --build .
#
# cp bins and libs to ./run
echo "--- build is finished, copy bins to ./run---"
cd ../
mkdir -p ../run
find ./ -name "CANX-tester" -exec cp -fv {} ./run \;
find ./ -name "*.so" -exec cp -fv {} ./run \;
find ./ -name "*.lib" -exec cp -fv {} ./run \;

