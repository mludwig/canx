# toolchain for cc7 CANX
# cmake -S . -B ./build -DCMAKE_TOOLCHAIN_FILE= <toolchainname>.cmake .
# cd ./build
# cmake --build .
#
# the toolchain just sets variables, but does not actually DO anything
# using build image podman : Dockerfile.can.cc7
message( STATUS "[${CMAKE_CURRENT_LIST_FILE}:${CMAKE_CURRENT_LIST_LINE}]: ---toolchain start--- ")
message( STATUS "[${CMAKE_CURRENT_LIST_FILE}:${CMAKE_CURRENT_LIST_LINE}]: CC7 build for CANX" )
#
# boost
# compile e.g.: ./b2 link=static,shared threading=multi -j7 -- cxxflags="-fPIC" 
#
SET ( BOOST_PATH_LIBS "/opt/3rdPartySoftware/boost/boost_1_75_0/stage/lib" )
SET ( BOOST_PATH_HEADERS   "/opt/3rdPartySoftware/boost/boost_1_75_0" )
IF ($ENV{CANMODULE_AS_STATIC_AS_POSSIBLE} )
	message( STATUS "[${CMAKE_CURRENT_LIST_FILE}:${CMAKE_CURRENT_LIST_LINE}]: as static as possible" )
	SET ( BOOST_LIBS
	libboost_log.a 
	libboost_log_setup.a 
	libboost_system.a 
	libboost_chrono.a 
	libboost_thread.a 
	libboost_date_time.a 
	libboost_filesystem.a 
	libboost_program_options.a 
	)
ELSE()
	message( STATUS "[${CMAKE_CURRENT_LIST_FILE}:${CMAKE_CURRENT_LIST_LINE}]: shared libs" )
	SET ( BOOST_LIBS
	-lboost_log 
	-lboost_log_setup 
	-lboost_system
	-lboost_chrono 
	-lboost_thread
	-lboost_date_time 
	-lboost_filesystem 
	-lboost_program_options 
  )
ENDIF()

# 
# LogIt, used by CANX directly as well
#
#SET ( LOGIT_HEADERS   "CanModule/LogIt/include" )
#SET ( LOGIT_PATH_LIBS "CanModule/LogIt/lib" )
#SET ( LOGIT_LIBS "-lLogIt" )
SET ( LOGIT_BACKEND_STOUTLOG ON CACHE BOOL "The basic backend to stdout" )
SET ( LOGIT_BACKEND_BOOSTLOG ON CACHE BOOL "Rotating file log with boost" )
SET ( LOGIT_BACKEND_UATRACELOG OFF CACHE BOOL "Unified Automation tookit logger" )
SET ( LOGIT_BACKEND_WINDOWS_DEBUGGER OFF CACHE BOOL "Windows debugger logger" )
message( STATUS "[${CMAKE_CURRENT_LIST_FILE}:${CMAKE_CURRENT_LIST_LINE}]: LogIt Backend LOGIT_BACKEND_STOUTLOG= ${LOGIT_BACKEND_STOUTLOG} ]" )
message( STATUS "[${CMAKE_CURRENT_LIST_FILE}:${CMAKE_CURRENT_LIST_LINE}]: LogIt Backend LOGIT_BACKEND_BOOSTLOG= ${LOGIT_BACKEND_BOOSTLOG} ]" )
message( STATUS "[${CMAKE_CURRENT_LIST_FILE}:${CMAKE_CURRENT_LIST_LINE}]: LogIt Backend LOGIT_BACKEND_UATRACELOG= ${LOGIT_BACKEND_UATRACELOG} ]" )
message( STATUS "[${CMAKE_CURRENT_LIST_FILE}:${CMAKE_CURRENT_LIST_LINE}]: LogIt Backend LOGIT_BACKEND_WINDOWS_DEBUGGER= ${LOGIT_BACKEND_WINDOWS_DEBUGGER} ]" )

#
# xerces-c, build from sources, plus pthreads
#
link_directories( /usr/local/lib )
SET ( XERCES_LIBS "-lxml2 -lxerces-c  -lpthread" )

#message(STATUS "[${CMAKE_CURRENT_LIST_FILE}:${CMAKE_CURRENT_LIST_LINE}]: XERCES_HEADERS= ${XERCES_HEADERS} ")
#message(STATUS "[${CMAKE_CURRENT_LIST_FILE}:${CMAKE_CURRENT_LIST_LINE}]: XERCES_PATH_LIBS= ${XERCES_PATH_LIBS} ")

# CanModule build behaviour:
# CANMODULE_BUILD_VENDORS OFF or not specified: only build mockup, do not build any vendor libs (default phony)
# CANMODULE_BUILD_VENDORS ON, nothing else: build mockup and all vendor libs (default all on)
# CANMODULE_BUILD_VENDORS ON, CANMODULE_BUILD_SYSTEC OFF: build mockup and all vendor libs except systec (drop systec)
# CANMODULE_BUILD_VENDORS ON, CANMODULE_BUILD_ANAGATE OFF: build mockup and all vendor libs except anagate (drop anagate)
# CANMODULE_BUILD_VENDORS ON, CANMODULE_BUILD_PEAK OFF: build mockup and all vendor libs except peak (drop peak)
SET(CANMODULE_BUILD_VENDORS ON )
# disable a vendor
#SET(CANMODULE_BUILD_SYSTEC OFF)
#SET(CANMODULE_BUILD_ANAGATE OFF)
#SET(CANMODULE_BUILD_PEAK OFF)
#
# we build CanModule from the sources, and we need headers and libs from the vendors
#
# linux: we use socketcan for systec and peak, and we always use the static lib, it is not big anyway
#
SET( SOCKETCAN_HEADERS "/opt/3rdPartySoftware/CAN/CAN_libsocketcan/include" )
SET( SOCKETCAN_LIB_PATH "/opt/3rdPartySoftware/CAN/CAN_libsocketcan/src/.libs" )
SET( SOCKETCAN_LIB_FILE "libsocketcan.a" )
#
SET( SYSTEC_HEADERS ${SOCKETCAN_HEADERS} )
SET( SYSTEC_LIB_PATH ${SOCKETCAN_LIB_PATH} )
SET( SYSTEC_LIB_FILE ${SOCKETCAN_LIB_FILE} )
#
SET( PEAK_HEADERS ${SOCKETCAN_HEADERS} )
SET( PEAK_LIB_PATH ${SOCKETCAN_LIB_PATH} )
SET( PEAK_LIB_FILE ${SOCKETCAN_LIB_FILE} )
#
# beta v6 from dec 2022, static libs converted into shared libs
SET ( ANAGATE_LIB_PATH "/opt/3rdPartySoftware/CAN/cc7/anagate/beta.v6/lib" )
SET ( ANAGATE_HEADERS  "/opt/3rdPartySoftware/CAN/cc7/anagate/beta.v6/include" )	
SET ( ANAGATE_LIB_FILE  -lCANDLLStaticRelease64 -lAnaGateStaticRelease -lAnaGateExtStaticRelease )

# beta v6 from dec 2022, static libs converted into shared libs
SET ( ANAGATE2_LIB_PATH "/opt/3rdPartySoftware/CAN/cc7/anagate/beta.v6/lib" )
SET ( ANAGATE2_HEADERS  "/opt/3rdPartySoftware/CAN/cc7/anagate/beta.v6/include" )	
SET ( ANAGATE2_LIB_FILE  -lCANDLLStaticRelease64 -lAnaGateStaticRelease -lAnaGateExtStaticRelease )

# api until dec 2022, shared in the originals
#SET ( ANAGATE_LIB_PATH "/opt/3rdPartySoftware/CAN/cc7/anagate/2.06-25.march.2021/lib" )
#SET ( ANAGATE_HEADERS  "/opt/3rdPartySoftware/CAN/cc7/anagate/2.06-25.march.2021/include" )
#SET ( ANAGATE_LIB_FILE "-lAPIRelease64 -lCANDLLRelease64" )

#
# special functions not using CanModule
#
SET ( SPECIAL_PATH_LIBS ${ANAGATE_PATH_LIBS} ) 
SET ( SPECIAL_HEADERS  ${ANAGATE_HEADERS} ) 
#SET ( SPECIAL_LIB_FILES ${ANAGATE_LIB_FILE} -lAnaGateExtRelease -lAnaGateRelease )
SET ( SPECIAL_LIB_FILES ${ANAGATE_LIB_FILE} )
message(STATUS "[${CMAKE_CURRENT_LIST_FILE}:${CMAKE_CURRENT_LIST_LINE}]: ---toolchain end--- ")

