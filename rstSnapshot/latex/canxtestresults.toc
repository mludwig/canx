\babel@toc {english}{}
\contentsline {chapter}{\numberline {1}Overview}{1}{chapter.1}%
\contentsline {chapter}{\numberline {2}Test Environment}{3}{chapter.2}%
\contentsline {chapter}{\numberline {3}Linux Anagate}{5}{chapter.3}%
\contentsline {chapter}{\numberline {4}Linux Anagate Summary}{7}{chapter.4}%
\contentsline {chapter}{\numberline {5}Linux Systec}{9}{chapter.5}%
\contentsline {section}{\numberline {5.1}Hardware}{9}{section.5.1}%
\contentsline {section}{\numberline {5.2}OS version}{10}{section.5.2}%
\contentsline {section}{\numberline {5.3}Testing versions}{10}{section.5.3}%
\contentsline {chapter}{\numberline {6}Linux Systec Results}{11}{chapter.6}%
\contentsline {section}{\numberline {6.1}abbreviations}{11}{section.6.1}%
\contentsline {section}{\numberline {6.2}tables}{11}{section.6.2}%
\contentsline {chapter}{\numberline {7}Linux Systec Summary}{27}{chapter.7}%
\contentsline {section}{\numberline {7.1}125000 bits/s}{27}{section.7.1}%
\contentsline {section}{\numberline {7.2}250000 bits/s}{27}{section.7.2}%
\contentsline {chapter}{\numberline {8}Recommendation}{29}{chapter.8}%
\contentsline {chapter}{\numberline {9}Linux Peak}{31}{chapter.9}%
\contentsline {section}{\numberline {9.1}Hardware}{31}{section.9.1}%
\contentsline {section}{\numberline {9.2}OS version}{31}{section.9.2}%
\contentsline {section}{\numberline {9.3}Testing versions}{31}{section.9.3}%
\contentsline {chapter}{\numberline {10}Linux Peak Results}{33}{chapter.10}%
\contentsline {section}{\numberline {10.1}abbreviations}{33}{section.10.1}%
\contentsline {section}{\numberline {10.2}tables}{33}{section.10.2}%
\contentsline {chapter}{\numberline {11}Linux Peak Summary}{39}{chapter.11}%
\contentsline {section}{\numberline {11.1}125000 bits/s}{39}{section.11.1}%
\contentsline {section}{\numberline {11.2}250000 bits/s}{39}{section.11.2}%
\contentsline {section}{\numberline {11.3}bitrates \textgreater {} 250000}{39}{section.11.3}%
\contentsline {chapter}{\numberline {12}Recommendation}{41}{chapter.12}%
\contentsline {chapter}{\numberline {13}Configurations}{43}{chapter.13}%
\contentsline {chapter}{\numberline {14}Summary}{45}{chapter.14}%
\contentsline {chapter}{\numberline {15}alphabetical index}{47}{chapter.15}%
