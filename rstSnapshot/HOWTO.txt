HOWTO
=====

run the tests and create the documentation

1. prepare xml templates for the module and vendor

2. adapt and run the changeParamsInXml.sh
2.1 so it reads the correct template
2.2 so it runs the parameters
2.3 run it and let it spit out a CANX xml config file. Name them i.e. peak_br125000_delay8000_ports2_frames50000.xml

3. connect the module, run CANX with the script runpeakTests.sh
3.1 i.e. sudo CANX-tester -I -cfg peak_br25000_delay5000_ports2_frames50000.xml
3.2. the script will copy the resuls and avoid overwriting them
3.3 go for a coffee and hope nothing crashes
3.4 this will create lost of csv files

4. use script collectTables.sh
4.1. to prouce a rst snippet
4.2. which can be directly linked into the rst tree (see index.rst)
4.3. or edited manually

5. copy the results into the RTS directory
5.1. csv tables
5.2 config xml as well

6. run documentation.sh, which produces an html tree and a pdf (alma9 only)
6.1 check, correct, repeat
6.2 think about the test results, write a summary

7. rename the PDF accordingly and push it to EDMS forever





