Linux Peak Summary
==================

125000 bits/s
-------------
the limit framerate for a 

- continuous transmission is 120Hz, corresponding to a framerate delay of 8000us
- burst is 160Hz, corresponding to a framerate delay of 5000us

without any delay the maximum framerate will be around 140Hz, with frames lost and the restransmission logic taking time on the bus

The recommended delay for a bitrate of 125000 is therefore 10000us or 100Hz, at about 20% of the theroretical maximum bus load.


250000 bits/s
-------------
the limit framerate for a 

- continuous transmission is between 830Hz and 970Hz, corresponding to a framerate delay of 1000us and a hardware busload of over 70%
- burst is 500Hz, corresponding to a framerate delay of 800us for rather long bursts of 5000 frames

without any delay the maximum framerate will be around 500Hz, with frames lost and the restransmission logic taking time on the bus

The recommended delay for a bitrate of 250000 is therefore 1000us or 1kHz, at about 70% of the theroretical maximum bus load. The module is 10x faster at 250000bits/s than at only half the bitrate, which is somewhat unexpected.

bitrates > 250000
-----------------
the module produces a Bus Error and a protocol violation

Recommendation
==============
Dual port peak usb-can bridges can be conveniently used for small scale testing up to 250000 bits/sec, where they perform well. Due to the complex workaround for the udev mapping problem under linux, the peak modules are not recommended for production under linux. Higher bitrates are not possible even though the module specs state them.









