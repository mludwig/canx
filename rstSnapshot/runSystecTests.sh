#!/bin/bash
# run tests for systec
CANXBIN=/home/mludwig/CAN/devel2.2.4/CANX-tester
TESTDIR=/home/mludwig/CAN/devel2.2.4/systectests

FILES=${TESTDIR}/*.xml

for f in $FILES
do
	echo "=== processing "${f}"==="
	# output goes into BusStatistics.csv
	${CANXBIN} -I -cfg ${f} 
	mv -v BusStatistics.csv ${f}.BusStatistics.csv
done
