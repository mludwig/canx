# powershell script
# run tests for systec
$CANXBIN=".\CANX-tester.exe"
$TESTDIR=".\systectests"

& $CANXBIN -h

Get-ChildItem $TESTDIR -Filter *.xml | 
Foreach-Object {
    $content = Get-Content $_.FullName
    $name = $_.FullName

    echo "processing "$name
    & $CANXBIN -I -cfg $name
    
    # move the result to a specific filename to match the config
    $dest=$name  + ".BusStatistics.csv"
    echo "moving BusStatistics.csv to "$dest
    Move-Item .\BusStatistics.csv $dest
}
