====================
Linux Systec Results
====================

abbreviations
-------------
- port.BR = port bitrate bits/s, the CAN bus speed setting
- tot.tx  = total count of sent frames of that port
- tot.rx  = total count of received frames of that port
- tx.FR   = sending framerate /Hz
- rx.FR   = receiving framerate /Hz
- hw.BL   = hardware bus load in % of theoretical maximum
 
the name of each table shows the bitrate, the delay (in us), the number of ports used and the number of frames sent
 
tables
------
 
.. csv-table:: ./results_systec/systec_br125000_delay1000000_ports2_frames5.xml.BusStatistics.csv
   :file: ./results_systec/systec_br125000_delay1000000_ports2_frames5.xml.BusStatistics.csv
   :widths: 50,50,50,50,50,50,50
   :header-rows: 2
 
.. csv-table:: ./results_systec/systec_br125000_delay100000_ports16_frames10000.xml.BusStatistics.csv
   :file: ./results_systec/systec_br125000_delay100000_ports16_frames10000.xml.BusStatistics.csv
   :widths: 50,50,50,50,50,50,50
   :header-rows: 2
 
.. csv-table:: ./results_systec/systec_br125000_delay100000_ports16_frames20000.xml.BusStatistics.csv
   :file: ./results_systec/systec_br125000_delay100000_ports16_frames20000.xml.BusStatistics.csv
   :widths: 50,50,50,50,50,50,50
   :header-rows: 2
 
.. csv-table:: ./results_systec/systec_br125000_delay100000_ports16_frames50000.xml.BusStatistics.csv
   :file: ./results_systec/systec_br125000_delay100000_ports16_frames50000.xml.BusStatistics.csv
   :widths: 50,50,50,50,50,50,50
   :header-rows: 2
 
.. csv-table:: ./results_systec/systec_br125000_delay10000_ports16_frames1000.xml.BusStatistics.csv
   :file: ./results_systec/systec_br125000_delay10000_ports16_frames1000.xml.BusStatistics.csv
   :widths: 50,50,50,50,50,50,50
   :header-rows: 2
 
.. csv-table:: ./results_systec/systec_br125000_delay15000_ports16_frames1000.xml.BusStatistics.csv
   :file: ./results_systec/systec_br125000_delay15000_ports16_frames1000.xml.BusStatistics.csv
   :widths: 50,50,50,50,50,50,50
   :header-rows: 2
 
.. csv-table:: ./results_systec/systec_br125000_delay20000_ports16_frames1000.xml.BusStatistics.csv
   :file: ./results_systec/systec_br125000_delay20000_ports16_frames1000.xml.BusStatistics.csv
   :widths: 50,50,50,50,50,50,50
   :header-rows: 2
 
.. csv-table:: ./results_systec/systec_br125000_delay20000_ports16_frames100.xml.BusStatistics.csv
   :file: ./results_systec/systec_br125000_delay20000_ports16_frames100.xml.BusStatistics.csv
   :widths: 50,50,50,50,50,50,50
   :header-rows: 2
 
.. csv-table:: ./results_systec/systec_br125000_delay20000_ports16_frames50000.xml.BusStatistics.csv
   :file: ./results_systec/systec_br125000_delay20000_ports16_frames50000.xml.BusStatistics.csv
   :widths: 50,50,50,50,50,50,50
   :header-rows: 2
 
.. csv-table:: ./results_systec/systec_br125000_delay25000_ports16_frames1000.xml.BusStatistics.csv
   :file: ./results_systec/systec_br125000_delay25000_ports16_frames1000.xml.BusStatistics.csv
   :widths: 50,50,50,50,50,50,50
   :header-rows: 2
 
.. csv-table:: ./results_systec/systec_br125000_delay25000_ports16_frames100.xml.BusStatistics.csv
   :file: ./results_systec/systec_br125000_delay25000_ports16_frames100.xml.BusStatistics.csv
   :widths: 50,50,50,50,50,50,50
   :header-rows: 2
 
.. csv-table:: ./results_systec/systec_br125000_delay30000_ports16_frames1000.xml.BusStatistics.csv
   :file: ./results_systec/systec_br125000_delay30000_ports16_frames1000.xml.BusStatistics.csv
   :widths: 50,50,50,50,50,50,50
   :header-rows: 2
 
.. csv-table:: ./results_systec/systec_br125000_delay30000_ports16_frames100.xml.BusStatistics.csv
   :file: ./results_systec/systec_br125000_delay30000_ports16_frames100.xml.BusStatistics.csv
   :widths: 50,50,50,50,50,50,50
   :header-rows: 2
 
.. csv-table:: ./results_systec/systec_br125000_delay30000_ports16_frames50000.xml.BusStatistics.csv
   :file: ./results_systec/systec_br125000_delay30000_ports16_frames50000.xml.BusStatistics.csv
   :widths: 50,50,50,50,50,50,50
   :header-rows: 2
 
.. csv-table:: ./results_systec/systec_br125000_delay50000_ports16_frames10000.xml.BusStatistics.csv
   :file: ./results_systec/systec_br125000_delay50000_ports16_frames10000.xml.BusStatistics.csv
   :widths: 50,50,50,50,50,50,50
   :header-rows: 2
 
.. csv-table:: ./results_systec/systec_br125000_delay50000_ports16_frames1000.xml.BusStatistics.csv
   :file: ./results_systec/systec_br125000_delay50000_ports16_frames1000.xml.BusStatistics.csv
   :widths: 50,50,50,50,50,50,50
   :header-rows: 2
 
.. csv-table:: ./results_systec/systec_br125000_delay50000_ports16_frames50000.xml.BusStatistics.csv
   :file: ./results_systec/systec_br125000_delay50000_ports16_frames50000.xml.BusStatistics.csv
   :widths: 50,50,50,50,50,50,50
   :header-rows: 2
 
.. csv-table:: ./results_systec/systec_br125000_delay50000_ports16_frames5000.xml.BusStatistics.csv
   :file: ./results_systec/systec_br125000_delay50000_ports16_frames5000.xml.BusStatistics.csv
   :widths: 50,50,50,50,50,50,50
   :header-rows: 2
 
.. csv-table:: ./results_systec/systec_br125000_delay60000_ports16_frames50000.xml.BusStatistics.csv
   :file: ./results_systec/systec_br125000_delay60000_ports16_frames50000.xml.BusStatistics.csv
   :widths: 50,50,50,50,50,50,50
   :header-rows: 2
 
.. csv-table:: ./results_systec/systec_br125000_delay60000_ports16_frames5000.xml.BusStatistics.csv
   :file: ./results_systec/systec_br125000_delay60000_ports16_frames5000.xml.BusStatistics.csv
   :widths: 50,50,50,50,50,50,50
   :header-rows: 2
 
.. csv-table:: ./results_systec/systec_br250000_delay10000_ports16_frames1000.xml.BusStatistics.csv
   :file: ./results_systec/systec_br250000_delay10000_ports16_frames1000.xml.BusStatistics.csv
   :widths: 50,50,50,50,50,50,50
   :header-rows: 2
 
.. csv-table:: ./results_systec/systec_br250000_delay10000_ports16_frames50000.xml.BusStatistics.csv
   :file: ./results_systec/systec_br250000_delay10000_ports16_frames50000.xml.BusStatistics.csv
   :widths: 50,50,50,50,50,50,50
   :header-rows: 2
 
.. csv-table:: ./results_systec/systec_br250000_delay10000_ports16_frames5000.xml.BusStatistics.csv
   :file: ./results_systec/systec_br250000_delay10000_ports16_frames5000.xml.BusStatistics.csv
   :widths: 50,50,50,50,50,50,50
   :header-rows: 2
 
.. csv-table:: ./results_systec/systec_br250000_delay15000_ports16_frames50000.xml.BusStatistics.csv
   :file: ./results_systec/systec_br250000_delay15000_ports16_frames50000.xml.BusStatistics.csv
   :widths: 50,50,50,50,50,50,50
   :header-rows: 2
 
.. csv-table:: ./results_systec/systec_br250000_delay15000_ports16_frames5000.xml.BusStatistics.csv
   :file: ./results_systec/systec_br250000_delay15000_ports16_frames5000.xml.BusStatistics.csv
   :widths: 50,50,50,50,50,50,50
   :header-rows: 2
 
.. csv-table:: ./results_systec/systec_br250000_delay20000_ports16_frames50000.xml.BusStatistics.csv
   :file: ./results_systec/systec_br250000_delay20000_ports16_frames50000.xml.BusStatistics.csv
   :widths: 50,50,50,50,50,50,50
   :header-rows: 2
 
.. csv-table:: ./results_systec/systec_br250000_delay20000_ports16_frames5000.xml.BusStatistics.csv
   :file: ./results_systec/systec_br250000_delay20000_ports16_frames5000.xml.BusStatistics.csv
   :widths: 50,50,50,50,50,50,50
   :header-rows: 2
 
.. csv-table:: ./results_systec/systec_br250000_delay25000_ports16_frames1000.xml.BusStatistics.csv
   :file: ./results_systec/systec_br250000_delay25000_ports16_frames1000.xml.BusStatistics.csv
   :widths: 50,50,50,50,50,50,50
   :header-rows: 2
 
.. csv-table:: ./results_systec/systec_br250000_delay25000_ports16_frames50000.xml.BusStatistics.csv
   :file: ./results_systec/systec_br250000_delay25000_ports16_frames50000.xml.BusStatistics.csv
   :widths: 50,50,50,50,50,50,50
   :header-rows: 2
 
.. csv-table:: ./results_systec/systec_br250000_delay25000_ports16_frames5000.xml.BusStatistics.csv
   :file: ./results_systec/systec_br250000_delay25000_ports16_frames5000.xml.BusStatistics.csv
   :widths: 50,50,50,50,50,50,50
   :header-rows: 2
 
