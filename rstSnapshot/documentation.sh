#!/bin/bash
# documentation.sh - make all the doc sphinx alma9
# dnf install python3-sphinx latexmk texconfig texlive* textlive*extra
# sudo texconfig rehash
DOC_ROOT=`pwd`
SPHINX_BUILD=`which sphinx-build`

${SPHINX_BUILD} -b html ${DOC_ROOT} ${DOC_ROOT}/html

mkdir -p ${DOC_ROOT}/latex
${SPHINX_BUILD} -b latex ${DOC_ROOT} ${DOC_ROOT}/latex

# pdf from latex
cd ${DOC_ROOT}/latex 
pdflatex canxtestresults.tex
mkdir -p ${DOC_ROOT}/pdf
find ${DOC_ROOT}/latex -name "canxtestresults.pdf" -exec mv -v {} ${DOC_ROOT}/pdf \;

#mkdir -p ${DOC_ROOT}/qthelp
#${SPHINX_BUILD} -b qthelp ${DOC_ROOT} ${DOC_ROOT}/qthelp

