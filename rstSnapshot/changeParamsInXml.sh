#!/bin/bash
# read reference xml and change params
refxml="anagateProto16Template.xml"

#  change the bitrate also in the reconnection supervisor
bitrate=20000
# this delays intelligently Canmodule
framerateLimitfactor=30000
generatedNbFramesPerTick=50000
# this delays CANX
waitBetweenFrames=0
# fx 4 ports
#ip="128.141.159.165"  
# fz proto 16 ports
ip0="ANACANFZ16-01F4-131E.cern.ch"
ip1="ANACANFZ16-01F4-131F.cern.ch"  
ip2="ANACANFZ16-01F8-1320.cern.ch"  

cat ${refxml} \
| sed "s/bitrate=\"125000\"/bitrate=\"${bitrate}\"/g" \
| sed "s/framerateLimitFactor=\"1000\"/framerateLimitFactor=\"${framerateLimitfactor}\"/g" \
| sed "s/generatedNbFramesPerTick=\"50000\"/generatedNbFramesPerTick=\"${generatedNbFramesPerTick}\"/g" \
| sed "s/ip0/${ip0}/g" \
| sed "s/ip1/${ip1}/g" \
| sed "s/ip2/${ip2}/g" 

