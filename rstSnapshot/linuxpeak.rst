Linux Peak
==========

Hardware
--------
- Vendor: PEAK
- Model: PEAK-PCAN-USB PRO FD 
- Model details: PCAN-USB Pro FD (MCU01h PCB02h) fw v3.2.0 bl v2.0.0
- Implementation: device ID=9054, specify as extended port identifier for sock
- Firmware: 3.2.0

OS version
----------
- Operating System: AlmaLinux 9.3 (Shamrock Pampas Cat) 
- CPE OS Name: cpe:/o:almalinux:almalinux:9::baseos
- Kernel: Linux 5.14.0-362.8.1.el9_3.x86_64
- Architecture: x86-64
- Peak USB driver: 8.15.2

Testing versions
----------------
- CANX: 2.2.4 ( march.2024 )
- CanModule: 2.2.4 ( march.2024 )
- Implementation: sock


