#!/bin/bash
# collect the existing tables and produce RST snippets to include them
RESULTDIR=./results_anagate
FILES=${RESULTDIR}/*.csv
OUTFILE=./linuxanagateresults.rst
rm -fv ${OUTFILE}

echo "=====================" >> ${OUTFILE}
echo "Linux Anagate Results" >> ${OUTFILE}
echo "=====================" >> ${OUTFILE}
echo "" >> ${OUTFILE}
echo "abbreviations" >> ${OUTFILE}
echo "-------------" >> ${OUTFILE}
echo "- port.BR = port bitrate bits/s, the CAN bus speed setting" >> ${OUTFILE}
echo "- tot.tx  = total count of sent frames of that port" >> ${OUTFILE}
echo "- tot.rx  = total count of received frames of that port" >> ${OUTFILE}
echo "- tx.FR   = sending framerate /Hz" >> ${OUTFILE}
echo "- rx.FR   = receiving framerate /Hz" >> ${OUTFILE}
echo "- hw.BL   = hardware bus load in % of theoretical maximum" >> ${OUTFILE}
echo " " >> ${OUTFILE}
echo "the name of each table shows the bitrate, the delay (in us), the number of ports used and the number of frames sent" >> ${OUTFILE}
echo " " >> ${OUTFILE}

echo "tables" >> ${OUTFILE}
echo "------" >> ${OUTFILE}
echo " " >> ${OUTFILE}

for f in $FILES
do
	echo "=== processing "${f}"==="
	echo ".. csv-table:: "${f} >> ${OUTFILE}		
	echo ".. csv-table:: "${tablename} >> ${OUTFILE}
	echo "   :file: "${f} >> ${OUTFILE}
	echo "   :widths: 50,50,50,50,50,50,50" >> ${OUTFILE}
	echo "   :header-rows: 2" >> ${OUTFILE}
	echo " "  >> ${OUTFILE}
done
echo "written to "${OUTFILE}
