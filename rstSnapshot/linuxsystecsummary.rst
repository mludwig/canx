Linux Systec Summary
====================

The tests are asymmetric, with framerates tx * 15 = rx . 

125000 bits/s
-------------
the limit framerates are

- continuous transmission is 32Hz, with a reception at 490Hz .
- the corresponding sending delay for continuous tranmission is 30000us
- short bursts are not faster, for 1000 frames max-tx=20Hz, rx=320Hz (delay=25000us) . 

This is surprising but the measurement was repeated several times and is confirmed.

The recommended delay is 30000us or 30Hz sending framerate .

250000 bits/s
-------------
the limit framerates are

- continuous transmission is 39Hz, with a reception at 580Hz (delay=25000 us).
- short bursts are not faster 

The recommended delay is 25000us or 40Hz sending framerate .


The limit framerates do not improve linearly with the bitrates, it seems therefore that the electronic is the limiting factor and not the time on the CAN bus.


Recommendation
==============
16-port systec usb-can bridges can be used for production (Atlas) with linux alma9.3 but framerates should be limited in any case to avoid frame losses. Slower than 125000 bits/s bitrates have not been investigated but it is expected that the delays will increase linearly as the time on the CAN bus will be the limiting factor and not the electronics. Bitrates above 250000 bits/s do not increase data troughput on a busy CAN bus since the electronics of the bridge can not keep up.






