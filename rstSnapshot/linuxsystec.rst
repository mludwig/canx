Linux Systec
============


Hardware
--------
- Vendor: SysTec
- Model: Systec16 port
- Model details: systec16 serial=240717
- Implementation: sock
- serial=24071001 can0
- fw=V5.17r71
- usbcan32.dll:	V6.05.0154.0
- usbcan64.dll:	V6.05.0154.0
- usbcancp.cpl:	V6.05.0071.0
- usbcancp.exe:	V6.05.0071.0
- usbcanlex.sys:	V6.05.0001.0
- ucannet.sys:	V6.05.0025.0

- CANX readout (under windows only):
	- UcanInitHardware returns [ 0x0]
	- dwVersion=[ 0x605]
	- type= 1 dwVersionEx=[ 0x605]
	- type= 2 dwVersionEx=[ 0x0]
	- type= 4 dwVersionEx=[ 0x190506]
	- type= 5 dwVersionEx=[ 0x0]
	- type= 6 dwVersionEx=[ 0x0]
	- type= 7 dwVersionEx=[ 0x0]
	- type= 8 dwVersionEx=[ 0x0]
	- type= 9 dwVersionEx=[ 0x0]
	- UcanGetFwVersion=[ 0x471105]
	- HwInfo.m_dwSerialNr= [0xe590cd0]

OS version
----------
- Operating System: AlmaLinux 9.3 (Shamrock Pampas Cat) 
- CPE OS Name: cpe:/o:almalinux:almalinux:9::baseos
- Kernel: Linux 5.14.0-362.8.1.el9_3.x86_64
- Architecture: x86-64
- Systec USB driver: https://gitlab.cern.ch/atlas-dcs-common-software/systec_can.git branch alma9_3 [march 2024]

Testing versions
----------------
- CANX: 2.2.4 ( march.2024 )
- CanModule: 2.2.4 ( march.2024 )
- Implementation: sock







