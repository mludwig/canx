Overview
========

We are .. testing bridge performance until frame losses occur in a standardized lab test. we do detailed bookkeeping for all send and received frames in order to detect any frame losses. Frames are generated with IDs spanning over the entire range and with varying frame contents.

This permits to determine a frame rate limit and a comparison to other modules (and vendors, operating systems...) and provides valuable input for designing CAN buses.

CANX is a cross-platform CanModule tester, which can be used for lab testing of hardware and CAN bridges.

- w2019s, w2022s
- CC7, CAL9
- Ubuntu (unsupported)
    
