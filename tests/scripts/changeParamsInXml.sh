#!/bin/bash
# read reference xml and change params
refxml="peak9054Template_linux.xml"

#  change the bitrate also in the reconnection supervisor
bitrate=125000
# this delays intelligently Canmodule
framerateLimitfactor=8000
generatedNbFramesPerTick=50000
# this delays CANX
waitBetweenFrames=0
# fx 4 ports
ip="128.141.159.165"  
# fz proto 16 ports
#ip0="128.141.159.242"  
#ip1="128.141.159.233"  
#ip2="128.141.159.218"  

cat ${refxml} \
| sed "s/bitrate=\"125000\"/bitrate=\"${bitrate}\"/g" \
| sed "s/framerateLimitFactor=\"1000\"/framerateLimitFactor=\"${framerateLimitfactor}\"/g" \
| sed "s/generatedNbFramesPerTick=\"50000\"/generatedNbFramesPerTick=\"${generatedNbFramesPerTick}\"/g" \
| sed "s/waitBetweenFrames=\"0\"/waitBetweenFrames=\"${waitBetweenFrames}\"/g" 

