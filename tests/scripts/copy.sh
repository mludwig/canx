#!/bin/bash
SRCDIR=/home/mludwig/projects/canx-master
find ${SRCDIR} -name "CANX-tester*" -exec cp -v {} ./ \;
find ${SRCDIR} -name "*an2can*.so*" -exec cp -v {} ./ \;
find ${SRCDIR} -name "*ancan*.so*" -exec cp -v {} ./ \;
find ${SRCDIR} -name "*sockcan*.so*" -exec cp -v {} ./ \;
find ${SRCDIR} -name "*CanModule.so*" -exec cp -v {} ./ \;
find ${SRCDIR} -name "*CanModule.a*" -exec cp -v {} ./ \;
