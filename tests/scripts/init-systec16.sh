#!/bin/bash

for i in {0..15}; 
do
  DEV="can"${i}
  echo "dev= " ${DEV}
  ifconfig $DEV down
  sleep 0.1
  ip link set $DEV type can bitrate 125000
  ifconfig $DEV up
done
