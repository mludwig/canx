Testing
=======

This stability test is designed to measure the CAN bridge performance in a special setting for sending and receiving frames without loosing any frame. Frame losses are generally avoided by buffering and re-transmitting them according to CAN bus design, and any occurring retransmissions can be detected by reading the Rx and Tx counters. Nevertheless, if the (reception) frame rate exceeds a certain limit on a port, frames will be lost and this is dangerous in a production system and not readily detectable in all cases.

This limit framerate strongly depends on the specific implementation of a given CAN bus: bitrate, termination, cable length, number of nodes, node electronics, bridge electronics and low-level software, sending and receiving patterns and all other parameters which have an influence on the buffering behavior on the CAN bus. The limiting framerate should never be exceeded in a production setting under worst-case peak load. The limiting framerate for intermittent short bursts of 100 frames is usually higher than for a continuous high load.

In this test we measure the limit framerate for a constant load during many minutes, with all ports of one given module connected to each others on one CAN bus: every port receives all the frames sent from any other port. The limit (reception-) framerate obtained permits a performance comparison between different modules/vendors and provides indications for a stable CAN bus design in production.

The sending of frames is performed in an un-synchronized multi-threaded way with each port having an independent sender and receiver thread. For a multi-port bridge the framerate limit is therefore primarily determined by the reception speed of the ports. This corresponds to an asymmetric communication pattern observed in many DAQ systems: a request for data is sent (a few frames) and data is returned (many frames).

The test is performed by continuously sending generated frames (50000 frames) from each port with a sending framerate specified. The frame IDs are cyclic and span over all possible IDs, with a cyclic varying data content and no special flags. All sending and receiving on all ports is monitored so that any losses are detected and the overall bus statistics at the end of each run is recorded. The host computer resources are monitored to avoid any limiting influence on the tests.


CANX is a cross-platform CanModule tester, which can be used for lab testing of hardware and CAN bridges.

- w2019s, w2022s
- CC7, CAL9
- Ubuntu (unsupported)
    
