# toolchain for w2022s
# mludwig at cern dot ch
option(64BIT "64 bit build" ON) 

#
# boost
#	
SET ( BOOST_PATH_LIBS "C:/3rdParty/boost/boost_1_81_0/stage/lib" )
SET ( BOOST_PATH_HEADERS "C:/3rdParty/boost/boost_1_81_0" )
SET ( BOOST_LIBS 
	-lboost_log 
	-lboost_log_setup 
	-lboost_filesystem 
	-lboost_system
	-lboost_chrono 
	-lboost_date_time 
	-lboost_thread  )
	
message( STATUS "[${CMAKE_CURRENT_LIST_FILE}:${CMAKE_CURRENT_LIST_LINE}] toolchain defines [BOOST_PATH_LIBS:${BOOST_PATH_LIBS}]" )
message( STATUS "[${CMAKE_CURRENT_LIST_FILE}:${CMAKE_CURRENT_LIST_LINE}] toolchain defines [BOOST_PATH_HEADERS:${BOOST_PATH_HEADERS}]" )
message( STATUS "[${CMAKE_CURRENT_LIST_FILE}:${CMAKE_CURRENT_LIST_LINE}] toolchain defines [BOOST_LIBS:${BOOST_LIBS}]" )


#
# xerces-c
#
SET ( XERCES_PATH_LIBS "C:/3rdParty/xerces/src/Debug" )
SET ( XERCES_HEADERS "C:/3rdParty/xerces/src" )
SET ( XERCES_LIBS "xerces-c_3D.lib" )

# CanModule build behaviour:
# CANMODULE_BUILD_VENDORS OFF or not specified: only build mockup, do not build any vendor libs (default phony)
# CANMODULE_BUILD_VENDORS ON, nothing else: build mockup and all vendor libs (default all on)
# CANMODULE_BUILD_VENDORS ON, CANMODULE_BUILD_SYSTEC OFF: build mockup and all vendor libs except systec (drop systec)
# CANMODULE_BUILD_VENDORS ON, CANMODULE_BUILD_ANAGATE OFF: build mockup and all vendor libs except anagate (drop anagate)
# CANMODULE_BUILD_VENDORS ON, CANMODULE_BUILD_PEAK OFF: build mockup and all vendor libs except peak (drop peak)
SET(CANMODULE_BUILD_VENDORS "ON" )
# disable a vendor
#SET(CANMODULE_BUILD_SYSTEC "OFF")
#SET(CANMODULE_BUILD_ANAGATE "OFF")
#SET(CANMODULE_BUILD_PEAK "OFF")
#
# systec
SET( SYSTEC_LIB_FILE "USBCAN64.lib")
#SET( SYSTEC_HEADERS  "C:/3rdParty/CAN/windows/systec/6.08r1/include")
#SET( SYSTEC_LIB_PATH "C:/3rdParty/CAN/windows/systec/6.08r1/lib" )
SET( SYSTEC_HEADERS  "C:/3rdParty/CAN/windows/systec/27.sept.2019/include")
SET( SYSTEC_LIB_PATH "C:/3rdParty/CAN/windows/systec/27.sept.2019/lib" )

# anagate
SET( ANAGATE_LIB_FILE "AnaGateCanDll64.lib")
SET( ANAGATE_HEADERS "C:/3rdParty/CAN/windows/anagate/beta.v6/include" )
SET( ANAGATE_LIB_PATH "C:/3rdParty/CAN/windows/anagate/beta.v6/lib" )
#
SET( ANAGATE2_LIB_FILE ${ANAGATE_LIB_FILE} )
SET( ANAGATE2_HEADERS ${ANAGATE_HEADERS}  )
SET( ANAGATE2_LIB_PATH ${ANAGATE_LIB_PATH}  )


# peak
# version PCAN Basic 4.3.2
SET( PEAK_LIB_FILE "PCANBasic.lib")
SET( PEAK_HEADERS  "C:/3rdParty/CAN/windows/peak/16.july.2022/Include" )
SET( PEAK_LIB_PATH "C:/3rdParty/CAN/windows/peak/16.july.2022/x64/VC_LIB" )




