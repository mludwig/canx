# toolchain for cc7 CANX
# cmake -DCMAKE_TOOLCHAIN_FILE= <toolchainname>.cmake .
#
# the toolchain just sets variables, but does not actually DO anything
message( STATUS "[${CMAKE_CURRENT_LIST_FILE}:${CMAKE_CURRENT_LIST_LINE}]: ubuntu18.04LTS build for CANX" )
#
# boost
#
IF ( $ENV{CANMODULE_AS_STATIC_AS_POSSIBLE} )
	# for static relocatable libs build like 
	#   ./b2 clean && ./b2 link=static threading=multi cxxflags="-fPIC"
	SET ( BOOST_PATH_LIBS "/opt/3rdPartySoftware/boost/boost_1_71_0/stage/lib" )
	SET ( BOOST_PATH_HEADERS   "/opt/3rdPartySoftware/boost/boost_1_71_0" )
	SET ( BOOST_LIBS 
		${BOOST_PATH_LIBS}/libboost_log.a 
		${BOOST_PATH_LIBS}/libboost_filesystem.a 
		${BOOST_PATH_LIBS}/libboost_program_options.a 
		${BOOST_PATH_LIBS}/libboost_system.a 
		${BOOST_PATH_LIBS}/libboost_chrono.a 
		${BOOST_PATH_LIBS}/libboost_date_time.a  
		${BOOST_PATH_LIBS}/libboost_thread.a 
		${BOOST_PATH_LIBS}/libboost_log_setup.a   )
ELSE()
	# keep dyn and static libs totally separated. these are the dyns
	SET ( BOOST_PATH_LIBS "/opt/3rdPartySoftware/boost/boost_1_71_0d/stage/lib" )
	SET ( BOOST_PATH_HEADERS   "/opt/3rdPartySoftware/boost/boost_1_71_0d" )
	SET ( BOOST_LIBS 
		-lboost_log 
		-lboost_log_setup 
		-lboost_filesystem 
		-lboost_program_options 
		-lboost_system
		-lboost_chrono 
		-lboost_date_time 
		-lboost_thread  )
ENDIF()

# 
# LogIt, used by CANX directly as well
#
SET ( LOGIT_HEADERS   "CanModule/LogIt/include" )
SET ( LOGIT_PATH_LIBS "CanModule/LogIt/lib" )
SET ( LOGIT_LIBS "-lLogIt" )
#
# xerces-c
#
SET ( XERCES_PATH_LIBS "/usr/local/lib" )
SET ( XERCES_HEADERS "/usr/local/include/xercesc" )
SET ( XERCES_LIBS "-lxerces-c" )
message(STATUS "[${CMAKE_CURRENT_LIST_FILE}:${CMAKE_CURRENT_LIST_LINE}]: XERCES_HEADERS= ${XERCES_HEADERS} ")
message(STATUS "[${CMAKE_CURRENT_LIST_FILE}:${CMAKE_CURRENT_LIST_LINE}]: XERCES_PATH_LIBS= ${XERCES_PATH_LIBS} ")

# CanModule build behaviour:
# CANMODULE_BUILD_VENDORS OFF or not specified: only build mockup, do not build any vendor libs (default phony)
# CANMODULE_BUILD_VENDORS ON, nothing else: build mockup and all vendor libs (default all on)
# CANMODULE_BUILD_VENDORS ON, CANMODULE_BUILD_SYSTEC OFF: build mockup and all vendor libs except systec (drop systec)
# CANMODULE_BUILD_VENDORS ON, CANMODULE_BUILD_ANAGATE OFF: build mockup and all vendor libs except anagate (drop anagate)
# CANMODULE_BUILD_VENDORS ON, CANMODULE_BUILD_PEAK OFF: build mockup and all vendor libs except peak (drop peak)
SET(CANMODULE_BUILD_VENDORS "ON" )
# disable a vendor
#SET(CANMODULE_BUILD_SYSTEC "OFF")
#SET(CANMODULE_BUILD_ANAGATE "OFF")
#SET(CANMODULE_BUILD_PEAK "OFF")
#
# we build CanModule from the sources, and we need headers and libs from the vendors
#
# linux: we use socketcan for systec and peak
#
SET( SOCKETCAN_HEADERS "/opt/3rdPartySoftware/CAN_libsocketcan/include" )
SET( SOCKETCAN_LIB_PATH "/opt/3rdPartySoftware/CAN_libsocketcan/src/.libs" )
SET( SOCKETCAN_LIB_FILE "-lsocketcan" )
#
SET( SYSTEC_HEADERS ${SOCKETCAN_HEADERS} )
SET( SYSTEC_LIB_PATH ${SOCKETCAN_LIB_PATH} )
SET( SYSTEC_LIB_FILE ${SOCKETCAN_LIB_FILE} )
#
SET( PEAK_HEADERS ${SOCKETCAN_HEADERS} )
SET( PEAK_LIB_PATH ${SOCKETCAN_LIB_PATH} )
SET( PEAK_LIB_FILE ${SOCKETCAN_LIB_FILE} )
#
# anagate: we use a TCP convenience library from anagate
#
SET ( ANAGATE_LIB_PATH 
	"/opt/3rdPartySoftware/Anagate/CAN/anagate-api-2.13/linux64/gcc4_6" 
	"/opt/3rdPartySoftware/Anagate/CAN/libAnaGateExt-1.0.3/linux64/gcc4_6"
	"/opt/3rdPartySoftware/Anagate//CAN/libAnaGate-1.0.9/linux64/gcc4_6"
)
SET ( ANAGATE_HEADERS "/opt/3rdPartySoftware/Anagate/CAN/anagate-api-2.13/linux64/gcc4_6/include" )
SET ( ANAGATE_LIB_FILE "-lAPIRelease64 -lCANDLLRelease64" )

#
# special functions not using CanModule
#
SET ( SPECIAL_PATH_LIBS ${ANAGATE_PATH_LIBS} ) 
SET ( SPECIAL_HEADERS  ${ANAGATE_HEADERS} ) 
SET ( SPECIAL_LIB_FILES ${ANAGATE_LIB_FILE} -lAnaGateExtRelease -lAnaGateRelease )

