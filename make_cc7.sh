#!/bin/bash
# build canx with cc7, as static as possible
#export CANMODULE_AS_STATIC_AS_POSSIBLE=true
export CANMODULE_AS_STATIC_AS_POSSIBLE=false
CMAKEBIN=/usr/local/bin/cmake

# out of source build
#${CMAKEBIN} -S . -B ./build -DCMAKE_TOOLCHAIN_FILE=build_cc7.cmake .
cd ./build && ${CMAKEBIN} --build .
#
# cp bins and libs to ./run
echo "--- build is finished, copy bins to ./run---"
mkdir -p ../run
find ./ -name "CANX-tester" -exec cp -v {} ./run \;
find ./ -name "*.so" -exec cp -v {} ./run \;
find ./ -name "*.lib" -exec cp -v {} ./run \;

