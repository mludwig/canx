#!/bin/bash
# build canx with cc7, as static as possible
#export CANMODULE_AS_STATIC_AS_POSSIBLE=true
export CANMODULE_AS_STATIC_AS_POSSIBLE=false
CMAKEBIN=/usr/local/bin/cmake

${CMAKEBIN} -S . -B ./build -DCMAKE_TOOLCHAIN_FILE=gitlab_cc7.cmake .
cd ./build && ${CMAKEBIN} --build . -- -j5
#
# show the results
#
cd ../
find ./ -name "CANX-tester" -exec ls -l {} \;
find ./ -name "*.so" -exec ls -l {}  \;
find ./ -name "*.lib" -exec ls -l {} \;

# cp bins and libs to ./run
echo "--- build is finished, copy bins to ./run---"
mkdir -p ../run
find ./ -name "CANX-tester" -exec cp -vf {} ./run \;
find ./ -name "*.so" -exec cp -vf {} ./run \;
find ./ -name "*.lib" -exec cp -vf {} ./run \;

