#!/bin/bash
# build canx with cal9
export CANMODULE_AS_STATIC_AS_POSSIBLE=false

CMAKEBIN=`which cmake`

${CMAKEBIN} -S . -B ./build -DCMAKE_TOOLCHAIN_FILE=build_ubuntu.cmake .
cd ./build && make -j 5
cd ..


