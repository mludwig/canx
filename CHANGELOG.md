# Change Log CANX
All notable changes to this project will be documented in this file.
The format is based on [Keep a Changelog](http://keepachangelog.com/).

### 2.2.4 [5.march.2024, 19.march.2024]
- take out # from statistics CSV files, they are not supported for import
- CanModule:devel2.2.4
-  .yaml: take out docker tags, deliver also CanModule.a
- added lots of scripts for automatic testing to generate validation reports for stability, using sphinx, into rstSnaphot. testing is in progress.

### 2.2.3 [Jan.2024]
- detailled info from hardware direct for systec & windows
- some build cleanup for w2022

### 2.2.2 [21.nov.2023]
- C++11
- debugging reception handler overflow pthread @ anagate: not a bug
- cleanup linked libs/CMakeLists.txt
- fix wrong lossless/bool overloaded method signature
- set reconnection behavior to "never" for canx runs configured by xml. We still have the special tests.


### 2.2.1 [9.aug.2023]
- add a specific test for performance torture: play pingpong with 2 ports
- add hardwareBusLoad to statistics: calculate bits/sec bus load on the transmitted CAN frames, in percent of max. bitrate, per port
- the existing "busLoad" calculation is wrong because it calculated based on bytes and the stat run time window is not taken into account. It is not a "bus load" therefore but rather a "bus usage". leave it in for the moment, but this should be suppressed.
- renovate the code a bit to use the canx classical methoods, load of execution sequence per thread via xml, also for anagate2.
- canx sequence per thread can be used to play "pingping" with 2 ports in an unsynchronized way. This is harder than pingpong and leads to lower possible framerates
- modified the "counting" frames to have the thread number as part of the ID so to avoid unralistic ID collisions on the bus
- checked the extended hw diagnostics and logs in detail
- added framerateLimitFactor to the xml config for all vendors, needs to be tested . 
  F<=0 no delay, F>0 reference delay is multiplied with F, F=1.0 reference delay (obviously)
- showStatistics readability in trace improved, hwBitRate
- dump stats to a file, nicely formatted as table for further use


### 2.0.25 [3.march.2022 ]
- testing CanOpenNG changes in CanModule:: devel2.0.25
- need cmake >= 3.24.0 for out-of-source build LogIt
- CI/CD builds podman-erized with new images cc7, cs9, cal9


### 2.0.20 [20.april.2022]
- fix bugs found by QA: error message mem leak in anagate and systec


## 2.0.19 [19.april.2022]
- chrono cleanup, small fixes

## 2.0.17 [16.march.2022]
- build chain revamped, out-of-source build
- cloning behaves slightly different linux and windwos, adapt jenkins jobs. Should not be like that but does not matter either
- no functional changes 

## 2.0.16 [23.feb.2022]
- reconnection attempts in an extra thread to have CanModule::sendMessage non blocking
  need to do all reconnection tests again, but should have no impact on API
  check also reconnection behavior "none" and correct exit if N failures

### 2.0.13 [ 1.july.2021 ]
- std::mutex OPCUA-2331 https://its.cern.ch/jira/browse/OPCUA-2331
- adding anagate timeout as an optional parameter to the list (in progress) CANT-44


### 2.0.12 [ 29 june 2021 ]
- fixed double dtor call, which crashed when calling stopBus() by the user
- see https://gitlab.cern.ch/bfarnham/canmoduleexitexample.git for error demo
- fixed for windows and cc7.8
- added c8 and w2019s testing, plus scripts
- tests can be skipped now, especially the last one: uni__multi_shared1
- improved ERR/test reporting and lots of small fixes

### 2.0.9
- sleep from C++ for cc7 and windows, instead of cpu delay loop. Suppress class accordingly as well

### 2.0.8
- adding CanModule filimonov wrapper again to solve https://its.cern.ch/jira/browse/OPCUA-2098
- testing wrapper accordingly

### 2.0.6 [ 12.jan.2021 ]
- all implementations/vendors testing
- error signalling
- 2.0.6 CanModule
- lots of bug fixes

### 1.1.9.4 [14.oct.2019]
- CanModule stats per port working now
- sync flag suppressed
- a couple of bug fixes


### [1.1.8] 16.july.2019
- back to single component LogIt "Canmodule"

###[1.1.7] 21.june.2019
- build chain update for optional vendor switch off
- CMakeLists.txt consistent cmake variable naming

### [1.1.5]
- sphinx/breathe documentation updated
- minor fixes in build chain
- look at possibilities to show full driver info and module info like
  i.e. firmware versions etc, without going through CanModule in the first place
- integrate peak modules/windows
- systec/socketCan reconnect
### to change
### to fix
- CanModule stats per port

## [1.0.3] - 4-april-2019
### Added
### Changed
- LogIt WRN level for statistics
- recorder: allocate on heap for slightly more performance
### Fixed


## [1.0.2] - 3-april-2019
### Added
- total message counters for send/receive per CAN port     
### Changed
### Fixed

## [1.0.1] - 21-march-2019
### Added
- based on CanModule 1.0.1, with anagate full reconnect
- added msg id increment ( id++ ) for mirror messages into the config of the receiver:
  add the (optional) attribute mirrorIncrementMsgId="true" 
  to the receive config, with mirror=true, to have each mirrored CAN message
  with a msg id incremented.
- added command line option "-anagateSoftwareReset" to perform an anagate firmware
  reset, using anagates API. This is a one-shot action which does not communicate
  to the module via CanModule at all, it just calls the API. Another client using 
  CanModule (i.e. another instance of CANX doing send/receive in parallel, or an OPC-UA server ...)
  should recuperate the anagate connection automatically (CanModule takes care of that).     
### Changed
### Fixed

## [0.0.15] - 14-sept-2018
### Added
- 32/64bit for w2008r2, w2012r2, w2016s (cc7 stays on 64bit)
- support for different vendor modules in config
- using cmake-3.1 or higher (actually 3.11.3) to be compatible with googletest

### Changed
### Fixed


## [0.0.14] - 26-june-2018
### Added

### Changed
- CanxThreads superclass, sync mechanism

### Fixed
- clock tick threading behaviour using boost::barrier
- repeat sequence

## [0.0.13] - 19-june-2018
### Added
- xml configurable
- receiver with mirror and recorder

### Changed
- many small fixes and changes, start to be useable

### Fixed
- can message sending and receiving
- LogIt ok now

## [0.0.12] - 16-may-2018
### Added
- CHANGELOG.md

### Changed

### Fixed

### Removed

