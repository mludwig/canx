#!/bin/bash
# cleanup stale files after cmake
#make clean
find ./ -name "CMakeCache.txt" -exec rm -vf {} \;
find ./ -name "Makefile" -exec rm -vf {} \;
find ./ -name "cmake_install.cmake" -exec rm -vf {} \;
find ./ -type d -name "CMakeFiles" -exec rm -rvf {} \;

# get rid of the cloned sources , plus LogIt underneath
find ./ -type d -name "CanModule" -exec rm -rvf {} \;
find ./ -type d -name "CanInterfaceImplementations" -exec rm -rvf {} \;

# toplevel artifacts
find ./ -type d -name "bin" -exec rm -rvf {} \;
find ./ -type d -name "lib" -exec rm -rvf {} \;
find ./ -type d -name "build" -exec rm -rvf {} \;

# refresh all VERSION.h
find ./ -type d -name "VERSION.h" -exec rm -vf {} \;

# kill bins and so
find ./ -type f -name "*.so" -exec rm -vf {} \;
find ./ -type f -name "CANX-tester*" -exec rm -vf {} \;
