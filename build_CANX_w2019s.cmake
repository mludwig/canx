# toolchain for w2019s on pcbe17294 
# mludwig at cern dot ch
option(64BIT "64 bit build" ON) 

#
# boost
#	
# 1.81.0 seems to have a problem, so try 1.79.0:
# First chance exception on 00007FFD1040EF5C (E06D7363, CPP_EH_EXCEPTION)!
#EXCEPTION_DEBUG_INFO:
#           dwFirstChance: 1
#           ExceptionCode: E06D7363 (CPP_EH_EXCEPTION)
#          ExceptionFlags: 00000081
#        ExceptionAddress: kernelbase.00007FFD1040EF5C
#        NumberParameters: 4
#ExceptionInformation[00]: 0000000019930520
#ExceptionInformation[01]: 0000000000000000
#ExceptionInformation[02]: 0000000000000000
#ExceptionInformation[03]: 0000000000000000
#First chance exception on 00007FFD1040EF5C (E06D7363, CPP_EH_EXCEPTION)!
SET ( BOOST_PATH_LIBS "C:/3rdPartySoftware/boost/boost_1_79_0/stage/lib" )
SET ( BOOST_PATH_HEADERS "C:/3rdPartySoftware/boost/boost_1_79_0" )
SET ( BOOST_LIBS 
	-lboost_log 
	-lboost_log_setup 
	-lboost_filesystem 
	-lboost_system
	-lboost_chrono 
	-lboost_date_time 
	-lboost_thread  )
	
message( STATUS "[${CMAKE_CURRENT_LIST_FILE}:${CMAKE_CURRENT_LIST_LINE}] toolchain defines [BOOST_PATH_LIBS:${BOOST_PATH_LIBS}]" )
message( STATUS "[${CMAKE_CURRENT_LIST_FILE}:${CMAKE_CURRENT_LIST_LINE}] toolchain defines [BOOST_PATH_HEADERS:${BOOST_PATH_HEADERS}]" )
message( STATUS "[${CMAKE_CURRENT_LIST_FILE}:${CMAKE_CURRENT_LIST_LINE}] toolchain defines [BOOST_LIBS:${BOOST_LIBS}]" )


#
# xerces-c
#
SET ( XERCES_PATH_LIBS "C:/3rdPartySoftware/xerces-c-3.2.4/src/Debug" )
SET ( XERCES_HEADERS "C:/3rdPartySoftware/xerces-c-3.2.4/src" )
SET ( XERCES_LIBS "xerces-c_3D.lib" )

# CanModule build behaviour:
# CANMODULE_BUILD_VENDORS OFF or not specified: only build mockup, do not build any vendor libs (default phony)
# CANMODULE_BUILD_VENDORS ON, nothing else: build mockup and all vendor libs (default all on)
# CANMODULE_BUILD_VENDORS ON, CANMODULE_BUILD_SYSTEC OFF: build mockup and all vendor libs except systec (drop systec)
# CANMODULE_BUILD_VENDORS ON, CANMODULE_BUILD_ANAGATE OFF: build mockup and all vendor libs except anagate (drop anagate)
# CANMODULE_BUILD_VENDORS ON, CANMODULE_BUILD_PEAK OFF: build mockup and all vendor libs except peak (drop peak)
SET(CANMODULE_BUILD_VENDORS "ON" )
# disable a vendor
#SET(CANMODULE_BUILD_SYSTEC "OFF")
#SET(CANMODULE_BUILD_ANAGATE "OFF")
#SET(CANMODULE_BUILD_PEAK "OFF")
#
# systec
# 6.june.2006
SET( SYSTEC_LIB_FILE "USBCAN64.lib")
SET( SYSTEC_HEADERS  "C:/3rdPartySoftware/CAN/windows/systec/6.june.2006/include")
SET( SYSTEC_LIB_PATH "C:/3rdPartySoftware/CAN/windows/systec/6.june.2006/lib" )

# anagate
# 2.06-25.march.2021
SET( ANAGATE_LIB_FILE "AnaGateCanDll64.lib")
SET( ANAGATE_HEADERS "C:/3rdPartySoftware/CAN/windows/anagate/2.06-25.march.2021/include" )
SET( ANAGATE_LIB_PATH "C:/3rdPartySoftware/CAN/windows/anagate/2.06-25.march.2021/lib" )
#
# beta.v6, hacked header AnaGateDll.h to force #define ANAGATEDLL_API __declspec(dllexport)
#SET( ANAGATE_LIB_FILE "AnaGateCanDll64.lib")
#SET( ANAGATE_HEADERS "C:/3rdPartySoftware/CAN/windows/anagate/beta.v6/include" )
#SET( ANAGATE_LIB_PATH "C:/3rdPartySoftware/CAN/windows/anagate/beta.v6/lib" )

# peak
# version PCAN Basic 4.3.2
SET( PEAK_LIB_FILE "PCANBasic.lib")
SET( PEAK_HEADERS  "C:/3rdPartySoftware/CAN/windows/peak/16.july.2022/Include" )
SET( PEAK_LIB_PATH "C:/3rdPartySoftware/CAN/windows/peak/16.july.2022/x64/VC_LIB" )




