# toolchain for cal9 CANX
# mludwig at cern dot ch
# cmake -S . -B ./build -DCMAKE_TOOLCHAIN_FILE=gitlab_cal9.cmake .
# cd ./build
# cmake --build .
# 
# using build image podman : Dockerfile.can.al9
message( STATUS "[${CMAKE_CURRENT_LIST_FILE}:${CMAKE_CURRENT_LIST_LINE}]: ---toolchain start--- ")
message(STATUS "[${CMAKE_CURRENT_LIST_FILE}:${CMAKE_CURRENT_LIST_LINE}]: Cal9 build for CANX-tester")
#
# boost: 1.75.0 as installed in image plus 1.81.0
#
#   ./b2 clean && ./b2 link=static threading=multi cxxflags="-fPIC"
SET ( BOOST_HOME   "/opt/3rdPartySoftware/boost/boost_1_81_0" )
SET ( BOOST_PATH_LIBS ${BOOST_HOME}/stage/lib )
SET ( BOOST_PATH_HEADERS  ${BOOST_HOME} )
IF ( $ENV{CANMODULE_AS_STATIC_AS_POSSIBLE} )
	SET ( BOOST_LIBS 
		${BOOST_PATH_LIBS}/libboost_log.a 
		${BOOST_PATH_LIBS}/libboost_filesystem.a 
		${BOOST_PATH_LIBS}/libboost_program_options.a 
		${BOOST_PATH_LIBS}/libboost_system.a 
		${BOOST_PATH_LIBS}/libboost_chrono.a 
		${BOOST_PATH_LIBS}/libboost_date_time.a  
		${BOOST_PATH_LIBS}/libboost_thread.a 
		${BOOST_PATH_LIBS}/libboost_log_setup.a   )
ELSE()
	SET ( BOOST_LIBS 
		-lboost_log 
		-lboost_log_setup 
		-lboost_filesystem 
		-lboost_program_options 
		-lboost_system
		-lboost_chrono 
		-lboost_date_time 
		-lboost_thread  )
ENDIF()

# 
# LogIt, used by CANX directly as well
#
SET ( LOGIT_BACKEND_STOUTLOG ON CACHE BOOL "The basic backend to stdout" )
SET ( LOGIT_BACKEND_BOOSTLOG ON CACHE BOOL "Rotating file log with boost" )
SET ( LOGIT_BACKEND_UATRACELOG OFF CACHE BOOL "Unified Automation tookit logger" )
SET ( LOGIT_BACKEND_WINDOWS_DEBUGGER OFF CACHE BOOL "Windows debugger logger" )
message( STATUS "[${CMAKE_CURRENT_LIST_FILE}:${CMAKE_CURRENT_LIST_LINE}]: LogIt Backend LOGIT_BACKEND_STOUTLOG= ${LOGIT_BACKEND_STOUTLOG} ]" )
message( STATUS "[${CMAKE_CURRENT_LIST_FILE}:${CMAKE_CURRENT_LIST_LINE}]: LogIt Backend LOGIT_BACKEND_BOOSTLOG= ${LOGIT_BACKEND_BOOSTLOG} ]" )
message( STATUS "[${CMAKE_CURRENT_LIST_FILE}:${CMAKE_CURRENT_LIST_LINE}]: LogIt Backend LOGIT_BACKEND_UATRACELOG= ${LOGIT_BACKEND_UATRACELOG} ]" )
message( STATUS "[${CMAKE_CURRENT_LIST_FILE}:${CMAKE_CURRENT_LIST_LINE}]: LogIt Backend LOGIT_BACKEND_WINDOWS_DEBUGGER= ${LOGIT_BACKEND_WINDOWS_DEBUGGER} ]" )
#SET ( LOGIT_HEADERS   "$ENV{JENKINSWS}/CanModule/LogIt/include" )
#SET ( LOGIT_PATH_LIBS "$ENV{JENKINSWS}/CanModule/LogIt/lib" )
#SET ( LOGIT_LIBS "-lLogIt" )
#message(STATUS "[${CMAKE_CURRENT_LIST_FILE}:${CMAKE_CURRENT_LIST_LINE}]: LOGIT_HEADERS= ${LOGIT_HEADERS} ")
#message(STATUS "[${CMAKE_CURRENT_LIST_FILE}:${CMAKE_CURRENT_LIST_LINE}]: LOGIT_PATH_LIBS= ${LOGIT_PATH_LIBS} ")

#
# xerces-c: must be installed on local machine
#
SET ( XERCES_LIBS "-lxerces-c" )
#SET ( XERCES_PATH_LIBS "/usr/local/lib" )
#SET ( XERCES_HEADERS "/usr/local/include" )
#SET ( XERCES_LIBS "-lxerces-c" )
#message(STATUS "[${CMAKE_CURRENT_LIST_FILE}:${CMAKE_CURRENT_LIST_LINE}]: XERCES_HEADERS= ${XERCES_HEADERS} ")
#message(STATUS "[${CMAKE_CURRENT_LIST_FILE}:${CMAKE_CURRENT_LIST_LINE}]: XERCES_PATH_LIBS= ${XERCES_PATH_LIBS} ")
#
# CanModule build behaviour:
# CANMODULE_BUILD_VENDORS OFF or not specified: only build mockup, do not build any vendor libs (default phony)
# CANMODULE_BUILD_VENDORS ON, nothing else: build mockup and all vendor libs (default all on)
# CANMODULE_BUILD_VENDORS ON, CANMODULE_BUILD_SYSTEC OFF: build mockup and all vendor libs except systec (drop systec)
# CANMODULE_BUILD_VENDORS ON, CANMODULE_BUILD_ANAGATE OFF: build mockup and all vendor libs except anagate (drop anagate)
# CANMODULE_BUILD_VENDORS ON, CANMODULE_BUILD_PEAK OFF: build mockup and all vendor libs except peak (drop peak)
SET(CANMODULE_BUILD_VENDORS "ON" )
# disable a vendor
#SET(CANMODULE_BUILD_SYSTEC "OFF")
#SET(CANMODULE_BUILD_ANAGATE "OFF")
#SET(CANMODULE_BUILD_PEAK "OFF")
#
# linux: we use socketcan for systec and peak, and we always use the static lib, it is not big anyway
#
SET( SOCKETCAN_HEADERS "/opt/3rdPartySoftware/CAN/CAN_libsocketcan/include" )
SET( SOCKETCAN_LIB_PATH "/opt/3rdPartySoftware/CAN/CAN_libsocketcan/src/.libs" )
SET( SOCKETCAN_LIB_FILE "libsocketcan.a" )
#
SET( SYSTEC_HEADERS ${SOCKETCAN_HEADERS} )
SET( SYSTEC_LIB_PATH ${SOCKETCAN_LIB_PATH} )
SET( SYSTEC_LIB_FILE ${SOCKETCAN_LIB_FILE} )
#
SET( PEAK_HEADERS ${SOCKETCAN_HEADERS} )
SET( PEAK_LIB_PATH ${SOCKETCAN_LIB_PATH} )
SET( PEAK_LIB_FILE ${SOCKETCAN_LIB_FILE} )
#
# beta v6 from dec 2022, static libs converted into shared libs
SET ( ANAGATE_LIB_PATH "/opt/3rdPartySoftware/CAN/cal9/anagate/beta.v6/lib" )
SET ( ANAGATE_HEADERS  "/opt/3rdPartySoftware/CAN/cal9/anagate/beta.v6/include" )	
SET ( ANAGATE_LIB_FILE  -lCANDLLStaticRelease64 -lAnaGateStaticRelease -lAnaGateExtStaticRelease )

# api until dec 2022, shared in the originals
#SET ( ANAGATE_LIB_PATH "/opt/3rdPartySoftware/CAN/cc7/anagate/2.06-25.march.2021/lib" )
#SET ( ANAGATE_HEADERS  "/opt/3rdPartySoftware/CAN/cc7/anagate/2.06-25.march.2021/include" )
#SET ( ANAGATE_LIB_FILE "-lAPIRelease64 -lCANDLLRelease64" )

#
# special functions not using CanModule
#
SET ( SPECIAL_PATH_LIBS ${ANAGATE_PATH_LIBS} ) 
SET ( SPECIAL_HEADERS  ${ANAGATE_HEADERS} ) 
#SET ( SPECIAL_LIB_FILES ${ANAGATE_LIB_FILE} -lAnaGateExtRelease -lAnaGateRelease )
SET ( SPECIAL_LIB_FILES ${ANAGATE_LIB_FILE} )
message(STATUS "[${CMAKE_CURRENT_LIST_FILE}:${CMAKE_CURRENT_LIST_LINE}]: ---toolchain end--- ")

