:: run all CAN tests
:: need all bridges from all vendors connected
::
echo off
echo "== basic per vendor tests uni_mono_reserved0  =="
echo "== anagate =="
del uni_mono_reserved0.w2016.anagate.txt
CANX-tester -T -uni_mono_reserved0 anagate 128.141.159.194 128.141.159.207 > uni_mono_reserved0.w2016.anagate.txt
::
echo "== peak =="
del uni_mono_reserved0.w2016.peak.txt
CANX-tester -T -uni_mono_reserved0 peak 8910 9054 > uni_mono_reserved0.w2016.peak.txt
::
echo "== systec =="
del uni_mono_reserved0.w2016.systec.txt
CANX-tester -T -uni_mono_reserved0 systec 0 0 > uni_mono_reserved0.w2016.systec.txt
::
::
echo "== performance per vendor tests uni_mono_reserved1 =="
echo "== anagate =="
del uni_mono_reserved1.w2016.anagate.txt
CANX-tester -T -uni_mono_reserved1 anagate 128.141.159.194 128.141.159.207 > uni_mono_reserved1.w2016.anagate.txt
::
echo "== peak =="
del uni_mono_reserved1.w2016.peak.txt
CANX-tester -T -uni_mono_reserved1 peak 8910 9054 > uni_mono_reserved1.w2016.peak.txt
::
echo "== systec =="
del uni_mono_reserved1.w2016.systec.txt
CANX-tester -T -uni_mono_reserved1 systec 0 0 > uni_mono_reserved1.w2016.systec.txt
::
::
echo "== mixing all vendors mono-task test mixed_mono_reserved0 =="
del mixed_mono_reserved0.w2016.txt
CANX-tester -T -mixed_mono_reserved0 128.141.159.194 > mixed_mono_reserved0.w2016.txt
::
::
echo "== sharing vendor between multi-task tests, per vendor uni_multi_shared0, reconnection  =="
:: SCENARIO 0
:: anagate
echo "== anagate scenario0 =="
del uni_multi_shared0.w2016.anagate.task0.scenario0.txt
del uni_multi_shared0.w2016.anagate.task1.scenario0.txt
start powershell -command "echo \"starting task0, wait for window to close\";.\CANX-tester -T -uni_multi_shared0 anagate 128.141.159.194 0 128.141.159.194 0 0 > uni_multi_shared0.w2016.anagate.task0.scenario0.txt"
start powershell -command "echo \"starting task1, wait for window to close\";.\CANX-tester -T -uni_multi_shared0 anagate 128.141.159.194 1 128.141.159.194 1 0 > uni_multi_shared0.w2016.anagate.task1.scenario0.txt"
timeout /T 10
echo "==>> anagate  POWER OFF <<=="
timeout /T 10
echo "==>> anagate POWER ON 128.141.159.194 <<=="
timeout /T 150
::
:: peak
echo "== peak scenario0 =="
del uni_multi_shared0.w2016.peak.task0.scenario0.txt
del uni_multi_shared0.w2016.peak.task1.scenario0.txt
start powershell -command "echo \"starting task0, wait for window to close\";.\CANX-tester -T -uni_multi_shared0 peak 8910 0 9054 0 0 > uni_multi_shared0.w2016.peak.task0.scenario0.txt"
start powershell -command "echo \"starting task1, wait for window to close\";.\CANX-tester -T -uni_multi_shared0 peak 8910 1 9054 1 0 > uni_multi_shared0.w2016.peak.task1.scenario0.txt"
timeout /T 10
echo "==>> peak POWER OFF <<=="
timeout /T 20
echo "==>> peak  POWER ON <<=="
timeout /T 150
::
:: systec
echo "== systec scenario0 =="
del uni_multi_shared0.w2016.systec.task0.scenario0.txt
del uni_multi_shared0.w2016.systec.task1.scenario0.txt
start powershell -command "echo \"starting task0, wait for window to close\";.\CANX-tester -T -uni_multi_shared0 systec 0 0 0 8 0 > uni_multi_shared0.w2016.peak.task0.scenario0.txt"
start powershell -command "echo \"starting task1, wait for window to close\";.\CANX-tester -T -uni_multi_shared0 systec 0 1 0 9 0 > uni_multi_shared0.w2016.peak.task1.scenario0.txt"
timeout /T 10
echo "==>> systec POWER OFF <<=="
timeout /T 10
echo "==>> systec  POWER ON <<=="
timeout /T 150

:: SCENARIO 1
:: anagate
echo "== anagate scenario1 =="
echo "== sharing vendor between multi-task tests, per vendor, reconnection =="
del uni_multi_shared0.w2016.anagate.task0.scenario1.txt
del uni_multi_shared0.w2016.anagate.task1.scenario1.txt
start powershell -command "echo \"starting task0, wait for window to close\";.\CANX-tester -T -uni_multi_shared0 anagate 128.141.159.194 0 128.141.159.194 0 1 > uni_multi_shared0.w2016.anagate.task0.scenario1.txt"
start powershell -command "echo \"starting task1, wait for window to close\";.\CANX-tester -T -uni_multi_shared0 anagate 128.141.159.194 1 128.141.159.194 1 1 > uni_multi_shared0.w2016.anagate.task1.scenario1.txt"
timeout /T 10
echo "==>> anagate  POWER OFF <<=="
timeout /T 10
echo "==>> anagate POWER ON 128.141.159.194 <<=="
timeout /T 150
::
:: peak
echo "== peak scenario1 =="
del uni_multi_shared0.w2016.peak.task0.scenario1.txt
del uni_multi_shared0.w2016.peak.task1.scenario1.txt
start powershell -command "echo \"starting task0, wait for window to close\";.\CANX-tester -T -uni_multi_shared0 peak 8910 0 9054 0 1 > uni_multi_shared0.w2016.peak.task0.scenario1.txt"
start powershell -command "echo \"starting task1, wait for window to close\";.\CANX-tester -T -uni_multi_shared0 peak 8910 1 9054 1 1 > uni_multi_shared0.w2016.peak.task1.scenario1.txt"
timeout /T 10
echo "==>> peak POWER OFF <<=="
timeout /T 20
echo "==>> peak  POWER ON <<=="
timeout /T 150
::
:: systec
echo "== systec scenario1 =="
del uni_multi_shared0.w2016.systec.task0.scenario1.txt
del uni_multi_shared0.w2016.systec.task1.scenario1.txt
start powershell -command "echo \"starting task0, wait for window to close\";.\CANX-tester -T -uni_multi_shared0 systec 0 0 0 8 1 > uni_multi_shared0.w2016.peak.task0.scenario1.txt"
start powershell -command "echo \"starting task1, wait for window to close\";.\CANX-tester -T -uni_multi_shared0 systec 0 1 0 9 1 > uni_multi_shared0.w2016.peak.task1.scenario1.txt"
timeout /T 10
echo "==>> systec POWER OFF <<=="
timeout /T 10
echo "==>> systec  POWER ON <<=="
timeout /T 150


:: SCENARIO 2
:: anagate
echo "== anagate scenario2 =="
echo "== sharing vendor between multi-task tests, per vendor, reconnection =="
del uni_multi_shared0.w2016.anagate.task0.scenario2.txt
del uni_multi_shared0.w2016.anagate.task1.scenario2.txt
start powershell -command "echo \"starting task0, wait for window to close\";.\CANX-tester -T -uni_multi_shared0 anagate 128.141.159.194 0 128.141.159.194 0 2 > uni_multi_shared0.w2016.anagate.task0.scenario2.txt"
start powershell -command "echo \"starting task1, wait for window to close\";.\CANX-tester -T -uni_multi_shared0 anagate 128.141.159.194 1 128.141.159.194 1 2 > uni_multi_shared0.w2016.anagate.task1.scenario2.txt"
timeout /T 10
echo "==>> anagate  POWER OFF <<=="
timeout /T 10
echo "==>> anagate POWER ON 128.141.159.194 <<=="
timeout /T 150
::
:: peak
echo "== peak scenario2 =="
del uni_multi_shared0.w2016.peak.task0.scenario2.txt
del uni_multi_shared0.w2016.peak.task1.scenario2.txt
start powershell -command "echo \"starting task0, wait for window to close\";.\CANX-tester -T -uni_multi_shared0 peak 8910 0 9054 0 2 > uni_multi_shared0.w2016.peak.task0.scenario2.txt"
start powershell -command "echo \"starting task1, wait for window to close\";.\CANX-tester -T -uni_multi_shared0 peak 8910 1 9054 1 2 > uni_multi_shared0.w2016.peak.task1.scenario2.txt"
timeout /T 10
echo "==>> peak POWER OFF <<=="
timeout /T 20
echo "==>> peak  POWER ON <<=="
timeout /T 150
::
:: systec
echo "== systec scenario2 =="
del uni_multi_shared0.w2016.systec.task0.scenario2.txt
del uni_multi_shared0.w2016.systec.task1.scenario2.txt
start powershell -command "echo \"starting task0, wait for window to close\";.\CANX-tester -T -uni_multi_shared0 systec 0 0 0 8 2 > uni_multi_shared0.w2016.peak.task0.scenario2.txt"
start powershell -command "echo \"starting task1, wait for window to close\";.\CANX-tester -T -uni_multi_shared0 systec 0 1 0 9 2 > uni_multi_shared0.w2016.peak.task1.scenario2.txt"
timeout /T 10
echo "==>> systec POWER OFF <<=="
timeout /T 10
echo "==>> systec  POWER ON, WAIT for test to finish until windows close <<=="
timeout /T 300

::
::
:: uni_multi_shared1
echo "== sharing vendor between multi-task tests, per vendor, 2 bridges uni_multi_shared1 =="
:: anagate
echo "== anagate =="
del uni_multi_shared1.w2016.anagate.task0.txt
del uni_multi_shared1.w2016.anagate.task1.txt
start powershell -command "echo \"starting task0, wait for window to close\";.\CANX-tester -T -uni_multi_shared1 anagate 0 128.141.159.194 128.141.159.194 > uni_multi_shared1.w2016.anagate.task0.txt"
start powershell -command "echo \"starting task1, wait for window to close\";.\CANX-tester -T -uni_multi_shared1 anagate 1 128.141.159.194 128.141.159.194 > uni_multi_shared1.w2016.anagate.task1.txt"
timeout /T 10
echo "==>> anagate  POWER OFF <<=="
timeout /T 10
echo "==>> anagate POWER ON 128.141.159.194 <<=="
timeout /T 150
::
:: peak
echo "== peak =="
del uni_multi_shared1.w2016.peak.task0.txt
del uni_multi_shared1.w2016.peak.task1txt
start powershell -command "echo \"starting task0, wait for window to close\";.\CANX-tester -T -uni_multi_shared1 peak 0 8910 9054 > uni_multi_shared1.w2016.peak.task0.txt"
start powershell -command "echo \"starting task1, wait for window to close\";.\CANX-tester -T -uni_multi_shared1 peak 1 8910 9054 > uni_multi_shared1.w2016.peak.task1.txt"
timeout /T 10
echo "==>> peak POWER OFF <<=="
timeout /T 20
echo "==>> peak  POWER ON <<=="
timeout /T 300
::
:: systec
echo "== systec =="
del uni_multi_shared1.w2016.systec.task0.txt
del uni_multi_shared1.w2016.systec.task1.txt
start powershell -command "echo \"starting task0, wait for window to close\";.\CANX-tester -T -uni_multi_shared1 systec 0 0 0 > uni_multi_shared1.w2016.peak.task0.txt"
start powershell -command "echo \"starting task1, wait for window to close\";.\CANX-tester -T -uni_multi_shared1 systec 1 0 0 > uni_multi_shared1.w2016.peak.task1.txt"
timeout /T 10
echo "==>> systec POWER OFF <<=="
timeout /T 10
echo "==>> systec  POWER ON <<=="




echo on


