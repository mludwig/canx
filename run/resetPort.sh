#!/bin/bash
for n in `seq 0 16`;
do
 	echo "reset port $n, br=125000"
	port="can"$n

	ifconfig $port down
	ip link set $port type can bitrate 125000
	ip link set $port type can restart-ms 100
	ifconfig $port up
	ip addr ls $port
done
