/*
 * SpecialCommands.h
 *
 *  Created on: Feb 21, 2019
 *      Author: mludwig
 */

#ifndef SRC_SPECIALCOMMANDS_H_
#define SRC_SPECIALCOMMANDS_H_

#include <string>
#include <iostream>
#include <fstream>
#include <sstream>
#include <chrono>
#include <thread>
#include <iomanip>
#include <ctime>
#include <sstream>

#ifdef _WIN32
	#include "AnaGateDllCan.h"
	#include "AnaGateDll.h"
	#include "tchar.h"
	#include "Winsock2.h"
	#include "windows.h"
#else
	#include <AnaGateDLL.h>
	#include <AnaGateDllCan.h>
	typedef unsigned long DWORD;
#endif

#include <boost/bind/bind.hpp>
#include <boost/signals2.hpp>


#include <CanMessage.h>
#include <CanLibLoader.h>
#include <CCanAccess.h>
#include <CanBusAccess.h>   // wrapper (filimonov)
#include <CanModuleUtils.h> // globalErrorSignal

namespace SpecialCommands_ns {

class SpecialCommands {
public:
	SpecialCommands();
	virtual ~SpecialCommands();

	AnaInt32 anagateSoftwareReset( std::string ip );

	void uni_mono_reserved0( std::string vendor, std::string ip0, std::string ip1 );
	void uni_mono_reserved1(std::string vendor, std::string ip0, std::string ip1 );
	void mixed_mono_reserved0(std::string ip0 );
	void uni_multi_shared0(std::string vendor,  std::string ip0, int porta, std::string ip1, int portb,int scenario );
	void uni_multi_shared1(std::string vendor, int thread /* 0 or 1 */, std::string ip0, std::string ip1 );
	void mixed_multi_shared0(int threadNo /* 0 or 1 */, std::string ip0, std::string ip1 );
	void uni_mono_signals(std::string vendor, std::string ip0 );



	/**
	 * API tests for syntax conformity, and wrapper
	 */
	void api_canaccess( std::string vendor, std::string ip0, std::string ip1 );


	/**
	 * anagate beta.v6 specific test
	 * diagnostic functionality for anagate2
	 */
	void showHwDiag(CanModule::HARDWARE_DIAG_t d);
	void showHwCounters(CanModule::PORT_COUNTERS_t c );
	void showHwLog(std::vector<CanModule::PORT_LOG_ITEM_t> log);

	void anagate0( std::string ip0, std::string ip1 );
	void anagate_diag0( int p, std::string ip0 );
	void anagate_diag1( int pstart, int pend, int frameDelay_ms, std::string ip0, std::string ip1 );

	// performance torture
	void pingpong(std::string vendor,
			std::string ip0,  std::string ip1,
			std::string parameters,
			unsigned int portA, unsigned int portB,
			float delay_us,
			int kmax);
	void winversionsystec();

	CanMessage createCanMsg();

	/**
	 * reception handlers
	 */
	static void receptionAll0(const CanMsgStruct/*&*/ message);
	static void receptionAll1(const CanMsgStruct/*&*/ message);
	static void reception0(const CanMsgStruct/*&*/ message);
	static void reception1(const CanMsgStruct/*&*/ message);
	static void reception2(const CanMsgStruct/*&*/ message);
	static void reception3(const CanMsgStruct/*&*/ message);
	static void reception4(const CanMsgStruct/*&*/ message);
	static void reception5(const CanMsgStruct/*&*/ message);
	static void reception6(const CanMsgStruct/*&*/ message);
	static void reception7(const CanMsgStruct/*&*/ message);
	static void reception8(const CanMsgStruct/*&*/ message);
	static void reception9(const CanMsgStruct/*&*/ message);
	static void reception10(const CanMsgStruct/*&*/ message);
	static void reception11(const CanMsgStruct/*&*/ message);
	static void reception12(const CanMsgStruct/*&*/ message);
	static void reception13(const CanMsgStruct/*&*/ message);
	static void reception14(const CanMsgStruct/*&*/ message);
	static void reception15(const CanMsgStruct/*&*/ message);

	static void receptionMute0(const CanMsgStruct/*&*/ message);
	static void receptionMute1(const CanMsgStruct/*&*/ message);
	static void receptionMute2(const CanMsgStruct/*&*/ message);
	static void receptionMute3(const CanMsgStruct/*&*/ message);

	/**
	 * error handlers
	 * Example: myCCanAccessPointer->canMessageError.connect(&myErrorRecievedHandler);
	 * boost::signals2::signal<void (const int,const char *,timeval &) > canMessageError;
	 */
	static void globalErrorHandler0( const int , const char *, timeval );
	static void globalErrorHandler1( const int , const char *, timeval );
	static void errorAll0(const int, const char *, timeval );
	static void errorAll1(const int, const char *, timeval );
	static void error0(const int, const char *, timeval );
	static void error1(const int, const char *, timeval );

	/**
	 * port status changed handlers
	 * Example: myCCanAccessPointer->canPortStateChanged.connect(&myPortSateChangedRecievedHandler);
	 * boost::signals2::signal<void (const int,const char *,timeval &) > canPortStateChanged;
	 */
	static void portStatusChangedAll0(const int, const char *, timeval );
	static void portStatusChangedAll1(const int, const char *, timeval );
	static void portStatusChanged0(const int, const char *, timeval );
	static void portStatusChanged1(const int, const char *, timeval );

	static void dumpBusVectorStatsToFile( std::vector<CanModule::CCanAccess*> cca_vect);


private:
	void showBusStats( CanModule::CCanAccess* cca );

	void showConnections();
	void ms_sleep( int ms ){
		std::chrono::milliseconds delay( ms );
		std::this_thread::sleep_for( delay );
	}
	void us_sleep( int us ){ // microseconds
		std::chrono::microseconds delay( us );
		std::this_thread::sleep_for( delay );
	}
	timeval timevalTestSignalCanMessageError();
	int16_t m_id;


};

} /* namespace SpecialCommands_ns */

#endif /* SRC_SPECIALCOMMANDS_H_ */
