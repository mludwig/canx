/*
 * CanxThreads.h
 *
 *  Created on: Aug 31, 2017
 *      Author: mludwig
 */

#ifndef CANXTHREADS_H_
#define CANXTHREADS_H_
#include <boost/thread/thread.hpp>
#include <LogIt.h>

// #include <CanBusAccess.h> // do not use the wrapper here
#include <CanLibLoader.h>


#ifndef _WIN32
#include <unistd.h>
#endif

#ifdef _WIN32
#include <windows.h>
#else
#include <sys/time.h>
#endif

#include <string>     // std::string, std::to_string
#include <chrono>
#include <thread>



using boost::this_thread::get_id;

namespace canxthreads_ns {

#define CLOCK_TICK_MAX_REPEAT_MS 10
#define CLOCK_TICK_MAX_MS 100

inline static void us_sleep( int us ){
	std::chrono::microseconds delay( us );
	std::this_thread::sleep_for( delay );
}


/**
 * superclass for threads with objects, plus execution timing. Both
 * sender and receiver classes inherit from this, and the synchronization
 * is kept separate for both classes. The instances inherit threadData
 * for configuration and setup, we have an inherited methods to make the
 * sender tick, launch and initialize the sender and the receiver. Essentially
 * provides "the hairy common thread and sync stuff" to both.
 */
class CanxThreads {
public:
	CanxThreads();
	virtual ~CanxThreads();

	static std::string ms_synchronizationMessage;
	static std::vector<std::string> ms_openCanPorts;

	typedef enum { VENDOR_SYSTEC=0, VENDOR_ANAGATE, VENDOR_ANAGATE2, VENDOR_PEAK, VENDOR_UNKNOWN } Vendor_t;
	typedef enum { MSG_RANDOM=0, MSG_COUNTING, MSG_FIXED, MSG_ASCONFIGURED, MSG_UNUSED } MsgGenerationType_t;
	typedef enum { CANID_STANDARD=0 /* id=11bit=2048 ids */, CANID_EXTENDED /* id=29bit=536mio ids */} CanIDType_t;
	// typedef enum { CANBR_125kbs=0 , CANBR_1000kbs, CANBR_800kbs, CANBR_500kbs, CANBR_250kbs, CANBR_100kbs, CANBR_50kbs, CANBR_20kbs, CANBR_10kbs, CANBR_undefined } CanBitrates_t;
	typedef enum { CANBR_10kbs, CANBR_20kbs, CANBR_50kbs, CANBR_100kbs, CANBR_125kbs, CANBR_250kbs, CANBR_500kbs, CANBR_800kbs, CANBR_1000kbs, CANBR_undefined } CanBitrates_t;

	// specifies for each thread==can port, which frame contents
	// are sent at which clock tick and in which order.
	// If the thread finds no frames for a given tick it does nothing
	typedef struct {
		unsigned int tick;     // main clock tick can have several frames, just keep same order
		unsigned int id;       // 11, or up to 29bits
		unsigned int wait_us;  // wait before each tick

		/** ID.RTR.IDE.R0.DLC.DATA.CRC.ACK
		 * ID.RTR.IDE.R0.DLC.DATA.CRC.ACK
		 * ID  = 3 chars representing hex range 000...7ff for ID, 11 bit
		 * RTR = 1 char representing a boolean RTR, 1 bit
		 * IDE = 1 char representing a boolean IDE, 1 bit
		 * R0  = 1 char representing a boolean R0, 1 bit
		 * DLC = 1 char representing hex range 0...f, 4 bit, data length in bytes==8
		 * DATA = 16 chars representing in hex 8 byte, 64bit
		 * CRC = 4 chars representing hex range 0000...ffff, 16 bit
		 * ACK = 1 char for 1 bit
		 */
		std::string canFrame;       // the whole CAN message frame with all flags: 35 chars: ID.RTR.IDE.R0.DLC.DATA.CRC.ACK
	} CanThreadFrame_t;


	/**
	 * all parameters needed to identify a CAN connection,
	 * via USB, ethernet, etc etc, depending on implementation.
	 * there are all sorts of CAN adaptors out there, M.2, PCIe, ISA,...
	 */
	typedef struct {
		int usbport;  // logical = physical, on the module, for now, I hope
		int canLport; // logical according to driver, generally derived by sw/driver
		int canPport; // physical according to hw type, this is what the user sees and cares about
		std::string socketName;
		std::string ipNumber;  // ethernet, wifi...
		bool mirrorIncrementMsgId;// when re-sending can msg, should we increment the msg id each time?
	} CanConnection_t;

	/**
	 * peak specific parameters
	 */
	typedef struct {
		//int usbPort;
		int deviceId;
	} PeakParameters_t;

	/**
	 * systec specific parameters
	 */
	typedef struct {
		//int usbPort;
	} SystecParameters_t;

	/**
	 * anagate specific parameters
	 */
	typedef struct {
		int operationalMode;
		int termination;
		int highSpeed;
		int timeStamp;
		int syncMode;
		int timeout; // default=6000ms configure board
	} AnagateParameters_t;


	/**
	 * this wonderful piece of art can surely be cleaned up and structured better:
	 *  - common part
	 *  - parts for each vendor
	 */
	typedef struct {
		Vendor_t vendor;
		AnagateParameters_t anagate;
		SystecParameters_t systec;
		PeakParameters_t peak;

		std::string threadID;
		boost::thread::id tid;
		unsigned int objIndex;

		CanConnection_t connection;
		CanConnection_t connection_mirror;

		// sender/receiver behavior
		bool sender_enabled;
		bool receiver_enabled;
		MsgGenerationType_t msgGenerationType;
		CanIDType_t canIdType;
		CanBitrates_t bitrate;
		std::string sbitrate;
		int receiver_delay_us; // dead time: delay until we are ready to read the next message
		bool receiverIsRecorder;
		bool receiverIsMirror;
		unsigned long int nbMsgToGeneratePerTick;
		unsigned long int sender_delay_us; // slow down sending
		float framerate_limit_factor; // slow down sending by using the reference delays
	} ThreadData_t; // fixed size allocation

	static bool isCanPortOpen( std::string port );
	static void addOpenPort( std::string port );
	static std::string codeBR_2_string( CanBitrates_t br );
	static CanBitrates_t codeString_2_BR( std::string sbr );

	// re-implement in children
	virtual bool executeClockTick( unsigned int c ){ return false; };
	virtual void executeStartTick( void ){};
	virtual void initialize( int usbport, int canport, std::string PortBitRate, std::string filename ){};
	virtual void initialize( int usbport, int canport, std::string PortBitRate ){};


	std::string getPeakSocketName( int usbport, int canPport, int deviceId);
	std::string getSystecSocketName( int usbport, int canPport );
	std::string getAnagateIpNumber( ThreadData_t td  );
	std::string getAnagatePport( ThreadData_t td  );
	void waitForMessage( std::string wm );
	bool detectMessage( std::string wm );
	std::string id( void ){ return( threadData().threadID ); }
	void setLogLevel( Log::LOG_LEVEL ll ){	Log::setNonComponentLogLevel( ll ); };


	ThreadData_t threadData(){ return s_threadData; }
	void setThreadData(ThreadData_t td){ s_threadData = td; }


private:
	static int s_instanceCounter;
	static CanModule::CanLibLoader *s_libloader;
	static LogItInstance *st_logIt;
	static Log::LogComponentHandle st_canxHandle;
	static Log::LogComponentHandle st_canModuleHandle;

	ThreadData_t s_threadData;

	int s_instanceNbCanx;
	bool s_threadLaunched;
	bool s_stop;
	void _clearThreadData( void );
};

} /* namespace canxthreads_ns */

#endif /* CANXTHREADS_H_ */
