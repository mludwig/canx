#!/bin/bash

CMAKEBIN=/usr/local/bin/cmake

export CANMODULE_AS_STATIC_AS_POSSIBLE=false
#export CANMODULE_AS_STATIC_AS_POSSIBLE=true
${CMAKEBIN} -S . -B ./build -DCMAKE_TOOLCHAIN_FILE=build_c9.cmake .
cd ./build && ${CMAKEBIN} --build . -- -j5
#
# cp bins and libs to ./run
echo "--- build is finished, copy bins to ./run---"
echo "pwd= "`pwd`
find ./ -name "CANX-tester" -exec cp -v {} ../run \;
find ./ -name "*.a" -exec cp -v {} ../run \;
find ./ -name "*.so" -exec cp -v {} ../run \;
