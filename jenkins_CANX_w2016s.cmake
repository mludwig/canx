# toolchain for w2016s CANX-tester for CI jenkins
# mludwig at cern dot ch
# cmake -DCMAKE_TOOLCHAIN_FILE=jenkins_CanModule_w2016s.cmake .
# 64bit all
option(64BIT "64 bit build" ON) 
#
#
# boost
#	
# bin download from sl	
SET ( BOOST_PATH_LIBS "M:/3rdPartySoftware/boost_1_59_0-msvc-14/lib64" )
SET ( BOOST_PATH_HEADERS "M:/3rdPartySoftware/boost_1_59_0-msvc-14" )
SET ( BOOST_LIBS 
	-lboost_log 
	-lboost_log_setup 
	-lboost_filesystem 
	-lboost_system
	-lboost_chrono 
	-lboost_date_time 
	-lboost_thread  )
# explicit static link with full paths
SET ( STATIC_BOOST_LIBS 
	M:/3rdPartySoftware/boost_1_59_0-msvc-14/lib64/boost_log-vc140-mt-1_59.lib 
	M:/3rdPartySoftware/boost_1_59_0-msvc-14/lib64/libboost_log_setup-vc140-mt-1_59.lib 
	M:/3rdPartySoftware/boost_1_59_0-msvc-14/lib64/libboost_filesystem-vc140-mt-1_59.lib
	M:/3rdPartySoftware/boost_1_59_0-msvc-14/lib64/libboost_system-vc140-mt-1_59.lib
	M:/3rdPartySoftware/boost_1_59_0-msvc-14/lib64/libboost_chrono-vc140-mt-1_59.lib 
	M:/3rdPartySoftware/boost_1_59_0-msvc-14/lib64/libboost_date_time-vc140-mt-1_59.lib 
	M:/3rdPartySoftware/boost_1_59_0-msvc-14/lib64/libboost_thread-vc140-mt-1_59.lib  )
	
message( STATUS "[${CMAKE_CURRENT_LIST_FILE}:${CMAKE_CURRENT_LIST_LINE}] toolchain defines [BOOST_PATH_LIBS:${BOOST_PATH_LIBS}]" )
message( STATUS "[${CMAKE_CURRENT_LIST_FILE}:${CMAKE_CURRENT_LIST_LINE}] toolchain defines [BOOST_HEADERS:${BOOST_HEADERS}]" )
message( STATUS "[${CMAKE_CURRENT_LIST_FILE}:${CMAKE_CURRENT_LIST_LINE}] toolchain defines [BOOST_LIBS:${BOOST_LIBS}]" )

# 
# LogIt, used by CANX directly as well
#
SET ( LOGIT_HEADERS   "$ENV{JENKINSWS}/CanModule/LogIt/include" )
SET ( LOGIT_PATH_LIBS "$ENV{JENKINSWS}/CanModule/LogIt/lib" )
SET ( LOGIT_LIBS "-lLogIt" )

#
# xerces-c
#
SET ( XERCES_PATH_LIBS "M:/3rdPartySoftware/xerces-c-3.2.0_64bit/src/Debug" )
SET ( XERCES_HEADERS "M:/3rdPartySoftware/xerces-c-3.2.0_64bit/src" )
SET ( XERCES_LIBS "M:/3rdPartySoftware/xerces-c-3.2.0_64bit/src/Debug/xerces-c_3D.lib" ) # full path needed, for some obscure reason

#
# CanModule build behaviour:
# CANMODULE_BUILD_VENDORS OFF or not specified: only build mockup, do not build any vendor libs (default phony)
# CANMODULE_BUILD_VENDORS ON, nothing else: build mockup and all vendor libs (default all on)
# CANMODULE_BUILD_VENDORS ON, CANMODULE_BUILD_SYSTEC OFF: build mockup and all vendor libs except systec (drop systec)
# CANMODULE_BUILD_VENDORS ON, CANMODULE_BUILD_ANAGATE OFF: build mockup and all vendor libs except anagate (drop anagate)
# CANMODULE_BUILD_VENDORS ON, CANMODULE_BUILD_PEAK OFF: build mockup and all vendor libs except peak (drop peak)
SET(CANMODULE_BUILD_VENDORS "ON" )
# disable a vendor
#SET(CANMODULE_BUILD_SYSTEC "OFF")
#SET(CANMODULE_BUILD_ANAGATE "OFF")
#SET(CANMODULE_BUILD_PEAK "OFF")
#
# systec
SET( SYSTEC_LIB_FILE "USBCAN64.lib")
SET( SYSTEC_HEADERS "M:/3rdPartySoftware/CAN/windows/systec/27.sept.2019/include")
SET( SYSTEC_LIB_PATH "M:/3rdPartySoftware/CAN/windows/systec/27.sept.2019/lib" )

# anagate
SET( ANAGATE_LIB_FILE "AnaGateCanDll64.lib")
SET( ANAGATE_HEADERS "M:/3rdPartySoftware/CAN/windows/anagate/beta.v6/include" )
SET( ANAGATE_LIB_PATH "M:/3rdPartySoftware/CAN/windows/anagate/beta.v6/lib" )
#SET( ANAGATE_HEADERS "M:/3rdPartySoftware/AnaGateCAN/win64/vc8/include" )
#SET( ANAGATE_LIB_PATH "M:/3rdPartySoftware/AnaGateCAN/win64/vc8/Release" )

# anagate2
SET( ANAGATE2_LIB_FILE "AnaGateCanDll64.lib" )
SET( ANAGATE2_HEADERS "M:/3rdPartySoftware/CAN/windows/anagate/beta.v6/include" )
SET( ANAGATE2_LIB_PATH "M:/3rdPartySoftware/CAN/windows/anagate/beta.v6/lib" )

# peak
SET(PEAK_LIB_FILE "PCANBasic.lib")
SET(PEAK_HEADERS "M:/3rdPartySoftware/CAN/windows/peak/16.july.2022/include")
SET(PEAK_LIB_PATH "M:/3rdPartySoftware/CAN/windows/peak/16.july.2022/lib")

