#!/bin/bash
# script to prepare for clang
# execute in the top level dir of your src project, where the top-level 
# CMakeLists.txt is
#
find ./ -name "CMakeCache.txt" -exec rm -f {} \;
find ./ -name "Makefile" -exec rm -f {} \;
export CXX=/mnt/pcbe13632disk2t/llvm-clang.7.1.0/llvm-7.1.0.src/build/bin/clang++
export CC =/mnt/pcbe13632disk2t/llvm-clang.7.1.0/llvm-7.1.0.src/build/bin/clang
export PATH=/mnt/pcbe13632disk2t/llvm-clang.7.1.0/llvm-7.1.0.src/build/bin:$PATH
export LD_LIBRARY_PATH=/mnt/pcbe13632disk2t/llvm-clang.7.1.0/llvm-7.1.0.src/build/lib:$LD_LIBRARY_PATH
#cmake -DCMAKE_USER_MAKE_RULES_OVERRIDE=./ClangOverrides.txt -DCMAKE_TOOLCHAIN_FILE=./build_CANX_cc7_pcbe13632.cmake -D_CMAKE_TOOLCHAIN_PREFIX=llvm- -stdlib=libc++ -nostdinc++ -std=c++11 -I/mnt/pcbe13632disk2t/llvm-clang.7.1.0/llvm-7.1.0.src/build/lib/clang/7.1.0/include .
#make -nostdinc++
cmake -DCMAKE_USER_MAKE_RULES_OVERRIDE=./ClangOverrides.txt -DCMAKE_TOOLCHAIN_FILE=./build_CANX_cc7_pcbe13632.cmake -D_CMAKE_TOOLCHAIN_PREFIX=llvm- -std=c++11 -I/mnt/pcbe13632disk2t/llvm-clang.7.1.0/llvm-7.1.0.src/build/lib/clang/7.1.0/include .
make -nostdinc++
