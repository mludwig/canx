/*
 * SpecialCommands.cpp
 *
 * do some one-time shot stuff directly to CAN bridges. DO NOT use CanModule.
 * this class exists just for labo convenience
 *
 *  Created on: Feb 21, 2019
 *      Author: mludwig
 *
 */

#include <time.h>

#include "SpecialCommands.h"
#include <boost/thread/thread.hpp> // for sleep

#include <Diag.h>
#include <generated/VERSION.h> // CanModule_VERSION

#ifdef _WIN32
#include "AnaGateDllCan.h"
#include "AnaGateDll.h"
#include <Usbcan32.h>

#include "tchar.h"
#include "Winsock2.h"
#include "windows.h"
#else
#include "AnaGateDLL.h"
#include "AnaGateDllCan.h"
typedef unsigned long DWORD;
#endif

namespace SpecialCommands_ns {

using namespace CanModule;

SpecialCommands::SpecialCommands():
	m_id(0)
{}

SpecialCommands::~SpecialCommands() {}

AnaInt32 SpecialCommands::anagateSoftwareReset( std::string ip ){
	AnaInt32 timeout = 10000; // 10secs
	std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__
			<< " calling CANRestart timeout= " << timeout << " ms" << std::endl;
	AnaInt32 anaRet = CANRestart( ip.c_str(), timeout );
	std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__
			<< " CANRestart anaRet= 0x" << std::hex << (int) anaRet << std::dec << std::endl;
	switch( anaRet ){
	case 0x0:{
		std::cout << __FUNCTION__ << " OK CANRestart" << std::endl;
		break;
	}
	case 0x20000:{
		std::cout << __FUNCTION__ << " ERROR CANRestart: EERR_TCPIP_SOCKE: Socket  error  occurred  in  TCP/IP layer." << std::endl;
		break;
	}
	case 0x30000:{
		std::cout << __FUNCTION__ << " ERROR CANRestart: ERR_TCPIP_NOTCONNECTED: Connection to TCP/IP partner can't be established or is disconnected." << std::endl;
		break;
	}
	default:{
		std::cout << __FUNCTION__ << " ERROR CANRestart: 0x" << std::hex << anaRet << std::dec << std::endl;
		break;
	}
	}

	AnaInt32 canModuleHandle;
	anaRet = CANOpenDevice( &canModuleHandle, FALSE, TRUE, 0, ip.c_str(), timeout );
	switch( anaRet ){
	case 0x0:{
		std::cout << __FUNCTION__ << " OK CANOpenDevice" << std::endl;
		break;
	}
	default:{
		std::cout << __FUNCTION__ << " ERROR CANOpenDevice: 0x" << std::hex << anaRet << std::dec << std::endl;
		break;
	}
	}
	AnaInt32 state = CANDeviceConnectState( canModuleHandle );
	std::cout << __FUNCTION__ << " CANDeviceConnectState: device connect state= " << state << std::endl;
	switch ( state ){
	case 1: std::cout << "1 = DISCONNECTED : The connection to the AnaGate is disconnected." << std::endl;  break;
	case 2: std::cout << "2 = CONNECTING : The connection is connecting." << std::endl;  break;
	case 3: std::cout << "3 = CONNECTED : The connection is established." << std::endl;  break;
	case 4: std::cout << "4 = DISCONNECTING : The connection is disconnecting." << std::endl;  break;
	case 5: std::cout << "5 = NOT_INITIALIZED : The network protocol is not successfully initialized." << std::endl;  break;
	}

	anaRet = CANCloseDevice( canModuleHandle );
	std::cout << __FUNCTION__ << " CANCloseDevice: ret= 0x" << std::hex << anaRet << std::dec<< std::endl;

	return anaRet;
}



/**
 * diagnostic tests for CanModule v2.0
 *
 * runnable tests on CanModule
 * names of tests classify more or less what is tested:
 * uni|mixed: bridges from a single vendor or bridges from various vendors used
 * mono|multi: single task or multi-task parallel execution
 * reserved|shared: all ports of a bridge used by the same task (reserved) or
 *    by different tasks (shared)
 *
 * instance: port numbering, access, multiple ports, multiple instances on same
 *     bridge and port, and syntax tests
 * access: port numbering and naming, over several devices, vendors and bridges
 * reconnect: error and reconnection behavior
 *
 * all tests take parameters: vendor, port, ip and others
 * all tests run under all supported OSes
 *
 * bridge related parameters like ipnumbers and ports are hardcoded for lab use now.
 * Of course they can easily be made configurable later.
 */
void SpecialCommands::showBusStats( CanModule::CCanAccess* cca ){
	CanStatistics stats;
	cca->getStatistics( stats );
	std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__
			<< " bus= " << cca->getBusName()
			<< " timeSinceOpened= " << stats.timeSinceOpened() << " ms"
			<< " timeSinceReceived= " << stats.timeSinceReceived() << " ms"
			<< " timeSinceTransmitted= " << stats.timeSinceTransmitted() << " ms"
			<< " unified port status= 0x" << hex << cca->getPortStatus() << dec
			<< " portBitrate= " << cca->getPortBitrate()
			<< " total tx= " << stats.totalTransmitted()
			<< " total rx= " << stats.totalReceived()
			<< " tx frame rate= " << stats.txRate()
			<< " rx frame rate= " << stats.rxRate()
			<< " busLoad= " << stats.busLoad()
			<< " hardwareBusLoad= " << stats.hardwareBusLoad() << std::endl;
}

/**
 * dump all stats from the vector of connections to one single file, as CSV
 * we take the first bus name as postfix for the filename
 */
/* static */ void SpecialCommands::dumpBusVectorStatsToFile( std::vector<CanModule::CCanAccess*> cca_vect){

	std::string fname = "BusStatistics.csv";
	std::vector<CanModule::CCanAccess*>::iterator it = cca_vect.begin();
	//fname += (*it)->getBusName() + ".csv";
	std::ofstream fout( fname );

	auto t = std::time(nullptr);
	auto tm = *std::localtime(&t);
	fout << "# canx, statistics, dump, version " << CANX_VERSION << ", "
			<< tm.tm_mday << "-"
			<< tm.tm_mon << "-"
			<< tm.tm_year+1900 << ", "
			<< tm.tm_hour << "h"
			<< tm.tm_min << "m"
			<< tm.tm_sec << "s"
			<< std::endl;

	std::string delim = ",\t";

	fout << "bus" << delim << "port.BR "
			<< delim << "tot.tx "
			<< delim << "tot.rx "
			<< delim << "tx.FR "
			<< delim << "rx.FR "
			<< delim << "hw.BL "
			<< std::endl;
	for ( it = cca_vect.begin() ; it != cca_vect.end(); ++it ){
		CanStatistics stats;
		(*it)->getStatistics( stats );

		fout << (*it)->getBusName() << delim << (*it)->getPortBitrate() << delim
				<< stats.totalTransmitted() << delim
				<< stats.totalReceived() << delim
				<< stats.txRate() << delim
				<< stats.rxRate()<< delim
				<< stats.hardwareBusLoad() << std::endl;
	}
	std::cout << "wrote bus statistics to file " << fname << std::endl;
}

void SpecialCommands::showConnections(){
	vector<Diag::CONNECTION_DIAG_t> connections = Diag::get_connections();
	std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " have " << connections.size() << " connections"<< std::endl;
	for ( std::vector<Diag::CONNECTION_DIAG_t>::iterator it = connections.begin() ; it != connections.end(); ++it ){
		std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__
				<< " bus= " << it->bus
				<< " lib= " << it->lib
				<< " parameter= " << it->parameter
				<< std::endl;
	}
}

/**
 * reception handlers
 */
void SpecialCommands::receptionAll0(const CanMsgStruct/*&*/ message){
	static uint64_t count = 0;
	std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " handler count= " << dec << count++ << " message.c_id= " << message.c_id << std::endl;
}
void SpecialCommands::receptionAll1(const CanMsgStruct/*&*/ message){
	static uint64_t count = 0;
	std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " handler count= " << dec << count++ << " message.c_id= " << message.c_id << std::endl;
}

void SpecialCommands::reception0(const CanMsgStruct/*&*/ message){
	static uint64_t count = 0;
	std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " handler count= " << dec << count++ << std::endl;
}
void SpecialCommands::reception1(const CanMsgStruct/*&*/ message){
	static uint64_t count = 0;
	std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " handler count= " << dec << count++ << std::endl;
}
void SpecialCommands::reception2(const CanMsgStruct/*&*/ message){
	static uint64_t count = 0;
	std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " handler count= " << dec << count++ << std::endl;
}
void SpecialCommands::reception3(const CanMsgStruct/*&*/ message){
	static uint64_t count = 0;
	std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " handler count= " << dec << count++ << std::endl;
}
void SpecialCommands::reception4(const CanMsgStruct/*&*/ message){
	static uint64_t count = 0;
	std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " handler count= " << dec << count++ << std::endl;
}
void SpecialCommands::reception5(const CanMsgStruct/*&*/ message){
	static uint64_t count = 0;
	std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " handler count= " << dec << count++ << std::endl;
}
void SpecialCommands::reception6(const CanMsgStruct/*&*/ message){
	static uint64_t count = 0;
	std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " handler count= " << dec << count++ << std::endl;
}
void SpecialCommands::reception7(const CanMsgStruct/*&*/ message){
	static uint64_t count = 0;
	std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " handler count= " << dec << count++ << std::endl;
}
void SpecialCommands::reception8(const CanMsgStruct/*&*/ message){
	static uint64_t count = 0;
	std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " handler count= " << dec << count++ << std::endl;
}
void SpecialCommands::reception9(const CanMsgStruct/*&*/ message){
	static uint64_t count = 0;
	std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " handler count= " << dec << count++ << std::endl;
}
void SpecialCommands::reception10(const CanMsgStruct/*&*/ message){
	static uint64_t count = 0;
	std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " handler count= " << dec << count++ << std::endl;
}
void SpecialCommands::reception11(const CanMsgStruct/*&*/ message){
	static uint64_t count = 0;
	std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " handler count= " << dec << count++ << std::endl;
}
void SpecialCommands::reception12(const CanMsgStruct/*&*/ message){
	static uint64_t count = 0;
	std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " handler count= " << dec << count++ << std::endl;
}
void SpecialCommands::reception13(const CanMsgStruct/*&*/ message){
	static uint64_t count = 0;
	std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " handler count= " << dec << count++ << std::endl;
}
void SpecialCommands::reception14(const CanMsgStruct/*&*/ message){
	static uint64_t count = 0;
	std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " handler count= " << dec << count++ << std::endl;
}
void SpecialCommands::reception15(const CanMsgStruct/*&*/ message){
	static uint64_t count = 0;
	std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " handler count= " << dec << count++ << std::endl;
}
void SpecialCommands::receptionMute0(const CanMsgStruct/*&*/ message){}
void SpecialCommands::receptionMute1(const CanMsgStruct/*&*/ message){}
void SpecialCommands::receptionMute2(const CanMsgStruct/*&*/ message){}
void SpecialCommands::receptionMute3(const CanMsgStruct/*&*/ message){}

void SpecialCommands::globalErrorHandler0( const int code, const char *msg, timeval tv ){
	static uint64_t count = 0;
	std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " handler count= " << dec << count++ << std::endl;
	std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " handler code= " << code << " msg= " << msg << std::endl;
}
void SpecialCommands::globalErrorHandler1( const int code, const char *msg, timeval tv ){
	static uint64_t count = 0;
	std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " handler count= " << dec << count++ << std::endl;
	std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " handler code= " << code << " msg= " << msg << std::endl;
}

void SpecialCommands::errorAll0( const int code, const char *msg, timeval tv ){
	static uint64_t count = 0;
	std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " handler count= " << dec << count++ << std::endl;
	std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " handler code= " << code << " msg= "
			<< msg << " tv= " << tv.tv_sec << "." << tv.tv_usec << std::endl;
}
void SpecialCommands::errorAll1( const int code, const char *msg, timeval tv ){
	static uint64_t count = 0;
	std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " handler count= " << dec << count++ << std::endl;
	std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " handler code= " << code << " msg= "
			<< msg << " tv= " << tv.tv_sec << "." << tv.tv_usec << std::endl;
}
void SpecialCommands::error0( const int code, const char *msg, timeval tv ){
	static uint64_t count = 0;
	std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " handler count= " << dec << count++ << std::endl;
	std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " handler code= " << code << " msg= "
			<< msg << " tv= " << tv.tv_sec << "." << tv.tv_usec << std::endl;
}
void SpecialCommands::error1( const int code, const char *msg, timeval tv ){
	static uint64_t count = 0;
	std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " handler count= " << dec << count++ << std::endl;
	std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " handler code= " << code << " msg= "
			<< msg << " tv= " << tv.tv_sec << "." << tv.tv_usec << std::endl;
}

void SpecialCommands::portStatusChangedAll0(const int code, const char *msg , timeval tv ){
	static uint64_t count = 0;
	std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " handler count= " <<  count++ << std::endl;
	std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " handler code= " << code << " msg= "
			<< msg << " tv= " << tv.tv_sec << "." << tv.tv_usec << std::endl;
}
void SpecialCommands::portStatusChangedAll1(const int code, const char *msg , timeval tv ){
	static uint64_t count = 0;
	std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " handler count= " <<  count++ << std::endl;
	std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " handler code= " << code << " msg= "
			<< msg << " tv= " << tv.tv_sec << "." << tv.tv_usec << std::endl;
}
void SpecialCommands::portStatusChanged0(const int code, const char *msg , timeval tv ){
	static uint64_t count = 0;
	std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " handler count= " <<  count++ << std::endl;
	std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " handler code= " << code << " msg= "
			<< msg << " tv= " << tv.tv_sec << "." << tv.tv_usec << std::endl;
}
void SpecialCommands::portStatusChanged1(const int code, const char *msg , timeval tv ){
	static uint64_t count = 0;
	std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " handler count= " << dec << count++ << std::endl;
	std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " handler code= " << code << " msg= "
			<< msg << " tv= " << tv.tv_sec << "." << tv.tv_usec << std::endl;
}


/**
 * **objectives:**
 * * access two bridges of the same vendor
 * * make sure that one can have many lib instances and many can access objects which point to the same physical port. The (canmodule-) lib instances will (usualy) all point to the same shared lib in the OS. The can port objects are instantiated and counted, but they point to the same port accordingly.
 * * check the correct port numbering for peak
 * * sending messages, receiving messages, statistics, using short circuit by flatband cable
 * * all vendors work with all OSes
 * * opening and closing ports, using several pointers to libs and ports
 * * correct port numbering
 * * check boost signal for the reception handler
 * * no reconnection tests, just a few slow messages
 */
void SpecialCommands::uni_mono_reserved0( std::string vendor, std::string ip0, std::string ip1 ){
	std::cout << __FUNCTION__ << std::endl;
	std::string vlib;
	std::string name0;
	std::string name1;
	std::string param = "Unspecified"; // defaults
	if ( vendor == "systec"){
		std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__
				<< " OK need two systec bridges "
				<< " (or one systec16) with 2 USB" << std::endl;
		std::cout << __FUNCTION__ << " using can0 and can8" << std::endl;
#ifdef _WIN32
		vlib = "st";
		name0 = "st:0"; // 1st port first systec16
		name1 = "st:8"; // 1st port second systec16
#else
		vlib = "sock";
		name0 = "sock:0";
		name1 = "sock:8";
#endif
	} else if ( vendor == "peak"){
		std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " OK need two peak duo bridges "
				<< " with 2 USB" << std::endl;
		std::cout << __FUNCTION__ << " using can0 and can2" << std::endl;
#ifdef _WIN32
		vlib = "pk";
		name0 = "pk:0"; // 1st port first peak
		name1 = "pk:2"; // 1st port second peak
#else
		vlib = "sock";
		// need extended deviceID for peak, use decimal number and LOCAL PORT NUMBERS
		name0 = "sock:0:device"+ip0;
		name1 = "sock:0:device"+ip1;
#endif
	} else if ( vendor == "anagate"){
		std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " OK need two anagate bridges " << std::endl;
		std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " OK using " << ip0 << " and " << ip1  << std::endl;
		vlib = "an";
		name0 = "an:0:" + ip0; // 1st port first anagate (duo)
		name1 = "an:0:" + ip1; // 1st port second anagate (x8)
	} else {
		std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " ERR vendor " << vendor << " unknown, exiting." << std::endl;
	}


	std::vector<CanModule::CCanAccess*> cca_vect;
	CanModule::CanLibLoader* lib0 = CanModule::CanLibLoader::createInstance( vlib );
	CanModule::CCanAccess* can0 = lib0->openCanBus( name0, param );
	CanModule::CCanAccess* can1 = lib0->openCanBus( name1, param );
	if ( can0 ){
		cca_vect.push_back( can0 );
	} else {
		std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " ERR can't open port " << name0 << ", test failed" << std::endl;
	}

	if ( can1 ) {
		cca_vect.push_back( can1 );
	} else {
		std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " ERR can't open port " << name1 << ", test failed" << std::endl;
	}

	for (std::vector<CanModule::CCanAccess*>::iterator it = cca_vect.begin() ; it != cca_vect.end(); ++it){
		if ( !(*it) ) 	{
			std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " ERR openCanBus failed" << std::endl;
		}
	}


	// connect reception methods
	std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " OK connecting reception methods" << std::endl;
	can0->canMessageCame.connect( &SpecialCommands::reception0 );
	can1->canMessageCame.connect( &SpecialCommands::reception1 );

	// disable reconnection behavior
	can0->setReconnectBehavior( CanModule::ReconnectAutoCondition::never, CanModule::ReconnectAction::singleBus );
	can1->setReconnectBehavior( CanModule::ReconnectAutoCondition::never, CanModule::ReconnectAction::singleBus );


	// test them by sending the signal with a fake message
	std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " OK test can0 reception0 with direct signal" << std::endl;
	CanMessage msg = createCanMsg();
	can0->canMessageCame(msg);
	ms_sleep( 10 );
	std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " OK test can1 reception1 with direct signal" << std::endl;
	msg = createCanMsg();
	can1->canMessageCame(msg);
	ms_sleep( 10 );

	// check
	{
		showConnections();
		for ( std::vector<CanModule::CCanAccess*>::iterator it = cca_vect.begin() ; it != cca_vect.end(); ++it ){
			showBusStats( *it );
		}
	}

	ms_sleep( 10 );
	std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " OK start sending/receiving frames both ports" << std::endl;

	// go at 5Hz, receptions should be in the same order as sends
	std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__
		<< " sending msg to opened ports" << std::endl;
	for ( int i = 0; i < 3; i++ ){
		msg = createCanMsg();
		if ( !can0->sendMessage( &msg ) ){
			std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " ERR send failed can0" << std::endl;
		} else {
			std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " OK send can0 " << i << std::endl;
		}

		ms_sleep( 200 );

		if ( !can1->sendMessage( &msg ) ){
			std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " ERR send failed can1" << std::endl;
		} else {
			std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " OK send can1 " << i << std::endl;
		}
		ms_sleep( 200 );
	}

	// check
	{
		showConnections();
		for ( std::vector<CanModule::CCanAccess*>::iterator it = cca_vect.begin() ; it != cca_vect.end(); ++it ){
			showBusStats( *it );
		}
	}

	// second lib instance with two more ports, but they get skipped
	std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__
			<< " load second lib instance with 2 more port objects" << std::endl;
	CanModule::CanLibLoader* libx = CanModule::CanLibLoader::createInstance( vlib );
	CanModule::CCanAccess* canx0 = libx->openCanBus( name0, param );
	CanModule::CCanAccess* canx1 = libx->openCanBus( name1, param );

	if ( !canx0 || !canx1 ){
		std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " ERR openCanBus failed" << std::endl;
	}

	std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " OK connecting new reception methods" << std::endl;
	canx0->canMessageCame.connect( &SpecialCommands::reception2 );
	canx1->canMessageCame.connect( &SpecialCommands::reception3 );

	// test them by sending the signal with a fake message
	std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " OK test reception methods with fake message" << std::endl;
	canx0->canMessageCame(msg);
	canx1->canMessageCame(msg);

	// check
	{
		showConnections();
		for ( std::vector<CanModule::CCanAccess*>::iterator it = cca_vect.begin() ; it != cca_vect.end(); ++it ){
			showBusStats( *it );
		}
	}

	// can we use all libs and instances? no... we have several objects
	// but some got skipped when opening the port because they point to the same hardware/can port.
	// for anagate, you can have several objects on the same port.
	// only reception handlers 2 and 3 will work since
	// handlers 1 and 2 are overwritten: a signal can only have 1 handler (in this boost setup: for
	// qt this is different!)
	//    can0->reception2
	//    can1->reception3
	//    canx0->reception2
	//    canx1->reception3
	// since we have the cable crossed over two ports, the ports receive from each others:
	// send can0->receive can1 (reception3) etc
	std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__
		<< " sending msg to double objetcs/same ports" << std::endl;

	for ( int i = 0; i < 3; i++ ){
		if ( !can0->sendMessage( &msg ) ){
			std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " ERR send failed can0" << std::endl;
		} else {
			std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " OK send can0" << std::endl;
		}
		ms_sleep( 10 );

		if ( !can1->sendMessage( &msg ) ){
			std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " ERR send failed can1" << std::endl;
		} else {
			std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " OK send can1" << std::endl;
		}
		ms_sleep( 10 );

		if ( !canx0->sendMessage( &msg ) ){
			if ( vendor == "anagate" ){
				std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " ERR send failed canx0" << std::endl;
			} else {
				std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " OK send failed canx0" << std::endl;
			}
		} else {
			if ( vendor == "anagate" ){
				std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " OK send canx0" << std::endl;
			} else {
				std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " ERR send canx0" << std::endl;
			}
		}
		ms_sleep( 10 );

		if ( !canx1->sendMessage( &msg ) ){
			if ( vendor == "anagate" ){
				std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " ERR send failed canx0" << std::endl;
			} else {
				std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " OK send failed canx0" << std::endl;
			}
		} else {
			if ( vendor == "anagate" ){
				std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " OK send canx0" << std::endl;
			} else {
				std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " ERR send canx0" << std::endl;
			}
		}
		ms_sleep( 10 );
	}

	// check
	{
		showConnections();
		for ( std::vector<CanModule::CCanAccess*>::iterator it = cca_vect.begin() ; it != cca_vect.end(); ++it ){
			showBusStats( *it );
		}
	}

	// we can close all of them
	lib0->closeCanBus( can0 );
	lib0->closeCanBus( can1 );
	libx->closeCanBus( canx0 ); // skipped
	libx->closeCanBus( canx1 ); // skipped

	ms_sleep( 1000 );

	// can we send msg to closed buses? we should not be able to do this...
	std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ 
		<< " sending msg to closed ports" << std::endl;
	for ( int i = 0; i < 3; i++ ){
		if ( !can0->sendMessage( &msg ) ){
			std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " OK send failed can0" << std::endl;
		} else {
			std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " ERR send can0" << std::endl;
		}
		ms_sleep( 10 );

		if ( !can1->sendMessage( &msg ) ){
			std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << "OK  send failed can1" << std::endl;
		} else {
			std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " ERR send can1" << std::endl;
		}
		ms_sleep( 10 );

		if ( !canx0->sendMessage( &msg ) ){
			std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " OK send failed canx0" << std::endl;
		} else {
			std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " ERR send can0" << std::endl;
		}
		ms_sleep( 10 );

		if ( !canx1->sendMessage( &msg ) ){
			std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " OK send failed canx1" << std::endl;
		} else {
			std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " ERR send can1" << std::endl;
		}
		ms_sleep( 10 );
	}


	// can we open them again? using any lib pointers ?
	canx0 = lib0->openCanBus( name0, param );
	canx1 = libx->openCanBus( name1, param );
	if ( !canx0 || !canx1 ){
		std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << "  ERRopenCanBus failed" << std::endl;
	}
	// can we NOW send msg to reopened? we should...
	std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__
		<< " sending msg to reopened ports" << std::endl;

	for ( int i = 0; i < 3; i++ ){
		if ( !canx0->sendMessage( &msg ) ){
			std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " ERR send failed canx0" << std::endl;
		} else {
			std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " OK send canx0" << std::endl;
		}
		ms_sleep( 10 );

		if ( !canx1->sendMessage( &msg ) ){
			std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " ERR send failed canx1" << std::endl;
		} else {
			std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " OK send canx1" << std::endl;
		}
		ms_sleep( 10 );
	}

	// we should be able to close them...
	lib0->closeCanBus( canx0 );
	libx->closeCanBus( canx1 );

	// can we NOW send msg to reopened and again closed? NOOOO...
	std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__
		<< " sending msg to re-closed ports" << std::endl;
	if ( !canx0->sendMessage( &msg ) ){
		std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " OK send failed canx0" << std::endl;
	} else {
		std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " ERR send can0" << std::endl;
	}
	ms_sleep( 10 );

	if ( !canx1->sendMessage( &msg ) ){
		std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " OK send failed canx1" << std::endl;
	} else {
		std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " ERR send can1" << std::endl;
	}

	// checking correct execution of dtors with scope
	{
		CanModule::CCanAccess* canz0 = NULL;
		CanModule::CCanAccess* canz1 = NULL;
		{
			std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__
					<< " recreate 2 more port objects inside a local scope" << std::endl;
			CanModule::CanLibLoader* libz = CanModule::CanLibLoader::createInstance( vlib );
			// this should work since the previous buses are closed
			canz0 = libz->openCanBus( name0, param );
			canz1 = libz->openCanBus( name1, param );

			if ( !canz0 || !canz1 ){
				std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << "ERR  openCanBus failed" << std::endl;
			}
			if ( !canz0->sendMessage( &msg ) ){
				std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " ERR send failed canz0" << std::endl;
			} else {
				std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " OK send canz0" << std::endl;
			}
			ms_sleep( 10 );

			if ( !canz1->sendMessage( &msg ) ){
				std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " ERR send failed canz1" << std::endl;
			} else {
				std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " OK send canz1" << std::endl;
			}

			canz0->stopBus();
			canz1->stopBus();
		} // dtors are NOT called, just for the lib

		// now we call them
		delete canz0;
		delete canz1;
	} // now they are out of scope

	std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " OK finished" << std::endl;
	exit(0);
}

/**
 * **objectives:**
 * * send messages on all available ports on 2 modules of the same vendor
 * * like uni_mono_reserved0, but performance testing
 * * check the boost signal for the reception handler
 * * check statistics on open, send and receive for all ports
 * * check the bitrate settings for each port
 * * vary the speed for frame-transmission (frame frequency, NOT bitrate) from
 * *   safe (no msg lost, 20Hz)
 * *   to unsafe (some msg lost (1kHz)
 * *   to max (lots of msg lost)
 * * we are NOT testing reconnection yet, this is supposed to go quickly. Nevertheless we set sendFail counter to 20
 * * connect just two reception handlers
 *
 * for peak@windows, we take more time because we DO TTEST reconnection, since the peak modules can not be shared
 * between tasks under windows.
 */
void SpecialCommands::uni_mono_reserved1(std::string vendor, std::string ip0, std::string ip1 ){
	std::string param = "Unspecified"; // defaults
	std::string vlib;
	std::vector<std::string> namesv;
	int indexA = -1;
	int indexB = -1;
	if ( vendor == "systec"){
		indexA = 0; indexB = 8; // global port numbering, for the reception handlers
#ifdef _WIN32
		vlib = "st";
#else
		vlib = "sock";
#endif


		for ( int p = 0; p < 16; p++){
			namesv.push_back( vlib + ":" + std::to_string(p));
		}
	} else if ( vendor == "peak"){
		indexA = 0; indexB = 2; // global port numbering, for the reception handlers
#ifdef _WIN32
		vlib = "pk";
		for ( int p = 0; p < 4; p++){
			namesv.push_back( vlib + ":" + std::to_string(p));
		}
#else
		vlib = "sock";

		// need extended deviceID for peak, use decimal number and LOCAL PORT NUMBERS
		//name0 = "sock:0:device:"+ip0;
		//name1 = "sock:0:device:"+ip1;
		for ( int p = 0; p < 2; p++)
			namesv.push_back( vlib + ":" + std::to_string(p) + ":device" + ip0);
		for ( int p = 0; p < 2; p++)
			namesv.push_back( vlib + ":" + std::to_string(p) + ":device" + ip1);

#endif

	} else if ( vendor == "anagate"){
		indexA = 0; indexB = 2; // local port numbering, for the reception handlers
		std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " OK need two anagate bridges " << std::endl;
		std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " OK using " << ip0 <<
				" (2ports) and " << ip1  << " (4 ports)" << std::endl;
		vlib = "an";
		namesv.clear();
		for ( int p = 0; p < 2; p++){
			namesv.push_back("an:" + std::to_string(p) + ":" + ip0);
		}
		for ( int p = 0; p < 4; p++){
			namesv.push_back("an:" + std::to_string(p) + ":" + ip1);
		}
	} else {
		std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " vendor " << vendor << " ERR unknown" << std::endl;
	}

	CanModule::CanLibLoader* lib = CanModule::CanLibLoader::createInstance( vlib );

	std::vector<CanModule::CCanAccess*> cca_vect;
	cca_vect.clear();

	bool fail = false;
	for ( unsigned int n = 0; n < namesv.size(); n++){
		std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " OK opening bus name= " << namesv[ n ] << std::endl;
		cca_vect.push_back( lib->openCanBus( namesv[ n ], param ) );

		if ( ! cca_vect[ n ] ){
			std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " ERR check: openCanBus name= " << namesv[ n ]
			  << " has failed" << std::endl;
			fail = true;
		}
	}
	// to avoid segfaults
	if ( fail ) {
		std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " ERR at least one openCanBus failed";
		exit(-1);
	}



	// connect both first ports on bridges to the same handler. Naja...not very portable
	cca_vect[ indexA ]->canMessageCame.connect( &SpecialCommands::reception0 );
	cca_vect[ indexB ]->canMessageCame.connect( &SpecialCommands::reception1 );

	// test both handlers with signal
	{
		std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " testing handlers:" << std::endl;
		CanMessage msg = createCanMsg();
		cca_vect[ indexA ]->canMessageCame(msg);
		cca_vect[ indexB ]->canMessageCame(msg);
		std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " testing handlers done" << std::endl;
	}

#if 0
	// we stay with the defaults
	// reconnection behavior
	for ( unsigned int iv = 0; iv < cca_vect.size(); iv++){
		cca_vect[ iv ]->setReconnectBehavior( CanModule::ReconnectAutoCondition::sendFail, CanModule::ReconnectAction::singleBus );
		cca_vect[ iv ]->setReconnectFailedSendCount( 20 );
	}
#endif

	for ( unsigned int iv = 0; iv < cca_vect.size(); iv++){
		cca_vect[ iv ]->setReconnectBehavior( CanModule::ReconnectAutoCondition::sendFail, CanModule::ReconnectAction::singleBus );
		cca_vect[ iv ]->setReconnectFailedSendCount( 3 );
	}

	// check
	{
		showConnections();
		for ( std::vector<CanModule::CCanAccess*>::iterator it = cca_vect.begin() ; it != cca_vect.end(); ++it ){
			showBusStats( *it );
		}
	}

	// ~a 10sec VERY gentle blast 5Hz on all ports with 2 receiving
	int kmax = 10;
	for ( int k = 0; k < kmax; k++ ){
		std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " OK *** countdown k= " << kmax-k << std::endl;

		for ( unsigned int i = 0; i < cca_vect.size(); i++ ){
			CanMessage msg = createCanMsg();
			cca_vect[ i ]->sendMessage( &msg );
			ms_sleep( 200 );
		}
	}


	// check
	{
		showConnections();
		for ( std::vector<CanModule::CCanAccess*>::iterator it = cca_vect.begin() ; it != cca_vect.end(); ++it ){
			showBusStats( *it );
		}
	}
	// test both handlers with signal
	{
		std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " OK testing handlers:" << std::endl;
		CanMessage msg = createCanMsg();
		cca_vect[ indexA ]->canMessageCame(msg);
		cca_vect[ indexB ]->canMessageCame(msg);
		std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " OK testing handlers done" << std::endl;
	}

	ms_sleep( 200 );
	// ~a 15sec blast, faster, on all ports with 2 receiving, for peak@windows we take longer
	if ( vendor == "peak"){
		kmax = 15000;
	} else {
		kmax = 100;
	}
	for ( int k = 0; k < kmax; k++ ){
		std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " OK *** countdown k= " << kmax-k << std::endl;
		for ( unsigned int i = 0; i < cca_vect.size(); i++ ){
			CanMessage msg = createCanMsg();
			cca_vect[ i ]->sendMessage( &msg );
			ms_sleep( 1 );
		}
	}

	// check
	{
		showConnections();
		for ( std::vector<CanModule::CCanAccess*>::iterator it = cca_vect.begin() ; it != cca_vect.end(); ++it ){
			showBusStats( *it );
		}
	}
	// test both handlers with signal
	{
		std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " OK testing handlers:" << std::endl;
		CanMessage msg = createCanMsg();
		cca_vect[ indexA ]->canMessageCame(msg);
		cca_vect[ indexB ]->canMessageCame(msg);
		std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " OK testing handlers done" << std::endl;
	}
	ms_sleep( 2000 );

	// ~a few sec blast full speed on all ports with 2 receiving
	kmax = 1000;
	for ( int k = 0; k < 1000; k++ ){
		std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " OK *** countdown k= " << kmax-k << std::endl;
		for ( unsigned int i = 0; i < cca_vect.size(); i++ ){
			CanMessage msg = createCanMsg();
			cca_vect[ i ]->sendMessage( &msg );
		}
	}

	// check
	{
		showConnections();
		for ( std::vector<CanModule::CCanAccess*>::iterator it = cca_vect.begin() ; it != cca_vect.end(); ++it ){
			showBusStats( *it );
		}
	}
	// test both handlers with signal
	{
		std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " OK testing handlers:" << std::endl;
		CanMessage msg = createCanMsg();
		cca_vect[ indexA ]->canMessageCame(msg);
		cca_vect[ indexB ]->canMessageCame(msg);
		std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " OK testing handlers done" << std::endl;
	}

	for ( unsigned int k = 0; k < cca_vect.size(); k++ ){
		lib->closeCanBus( cca_vect[ k ] );
	}
	std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " OK finished" << std::endl;
	exit(0);
}

/**
 * mixing all three implementations/vendors in one CanModule instance
 * * detect any driver incompatibilities, socketcan etc
 * * this scenario happens rarely but it **should** work
 * * no shared modules between tasks (mono task)
 * * one bridge from each vendor, one or two ports
 * * need 4 connections (can ports) for that to work
 * * cabling:
 * 	  * an:1 <->systec:5 [sock:5]
 * 	  * systec:3 [sock:3] <-> peak:can1:device8910 [sock:17, but extended id: sock:can1:device8910]
 * * all settings default
 * * * 10 frames sent by all ports, super slow
 * * * then 1000 frames at full blast
 */
void SpecialCommands::mixed_mono_reserved0(std::string ip ){
	std::string analib;
	std::string peaklib;
	std::string systeclib;
	std::string systec3;
	std::string systec5;
	std::string peak1;
	std::string ana1;
	std::string param = "Unspecified"; // defaults
	std::cout << __FUNCTION__ << " OK need one bridge from each vendor. anagate ip= " << ip << std::endl;
	analib = "an";
	ana1 = std::string("an:1:") + ip;
#ifdef _WIN32
	systeclib = "st";
	peaklib = "pk";
	systec3 = "st:3";
	systec5 = "st:5";
	peak1 = "pk:1";
#else
	std::cout << __FUNCTION__ << " OK need one systec16 USB0 and one peak duo, plus an anagate " << std::endl;
	systeclib = "sock";
	peaklib = "sock";
	systec3 = "sock:3"; // systec16 USB0
	systec5 = "sock:5"; // systec16 USB0
	peak1 = "sock:can1:device8910"; // peak can1 USB3, extended id
#endif

	CanModule::CanLibLoader* libana = CanModule::CanLibLoader::createInstance( analib );
	CanModule::CanLibLoader* libpeak = CanModule::CanLibLoader::createInstance( peaklib );
	CanModule::CanLibLoader* libsystec = CanModule::CanLibLoader::createInstance( systeclib );

	std::vector<CanModule::CCanAccess*> cca_vect;
	cca_vect.clear();

	CanModule::CCanAccess* cansystec3 = libsystec->openCanBus( systec3, param );
	CanModule::CCanAccess* cansystec5 = libsystec->openCanBus( systec5, param );
	CanModule::CCanAccess* canpeak1 = libpeak->openCanBus( peak1, param );
	CanModule::CCanAccess* canana1 = libana->openCanBus( ana1, param );
	cca_vect.push_back( cansystec3 );
	cca_vect.push_back( cansystec5 );
	cca_vect.push_back( canpeak1 );
	cca_vect.push_back( canana1 );

#if 0
	for (std::vector<CanModule::CCanAccess*>::iterator it = cca_vect.begin() ; it != cca_vect.end(); ++it){
		if ( !(*it) ) 	{
			std::cout << __FUNCTION__ << " openCanBus failed" << std::endl;
			exit(-1);
		}
	}
#endif

	std::cout << __FUNCTION__ << " OK cansystec3= " << cansystec3->getBusName() << std::endl;
	std::cout << __FUNCTION__ << " OK cansystec5= " << cansystec5->getBusName() << std::endl;
	std::cout << __FUNCTION__ << " OK canpeak1= " << canpeak1->getBusName() << std::endl;
	std::cout << __FUNCTION__ << " OK canana1= " << canana1->getBusName() << std::endl;



	// connect 4 handlers
	cansystec3->canMessageCame.connect( &SpecialCommands::reception0 );
	cansystec5->canMessageCame.connect( &SpecialCommands::reception1 );
	canpeak1->canMessageCame.connect( &SpecialCommands::reception2 );
	canana1->canMessageCame.connect( &SpecialCommands::reception3 );

	// test all handlers with signal
	{
		std::cout << __FUNCTION__ << " OK testing handlers:" << std::endl;
		CanMessage msg = createCanMsg();
		cansystec3->canMessageCame(msg);
		cansystec5->canMessageCame(msg);
		canpeak1->canMessageCame(msg);
		canana1->canMessageCame(msg);
		std::cout << __FUNCTION__ << " OK testing handlers done" << std::endl;
	}
	boost::this_thread::sleep(boost::posix_time::microseconds( 2000000 ));

	// check
	{
		showConnections();
		for ( std::vector<CanModule::CCanAccess*>::iterator it = cca_vect.begin() ; it != cca_vect.end(); ++it ){
			showBusStats( *it );
		}
	}

	/**
	 * go super slow at 5Hz
	 */
	for ( int i = 0; i < 10; i++ ){
		CanMessage msg = createCanMsg();
		for ( unsigned int k = 0; k < cca_vect.size(); k++ ){
			cca_vect[k]->sendMessage( &msg );
			boost::this_thread::sleep(boost::posix_time::microseconds( 200000 ));
		}
	}
	// check
	{
		showConnections();
		for ( std::vector<CanModule::CCanAccess*>::iterator it = cca_vect.begin() ; it != cca_vect.end(); ++it ){
			showBusStats( *it );
		}
	}
	std::cout << __FUNCTION__ << " OK now we go full blast" << std::endl;

	/**
	 * go full blast
	 */
	for ( int i = 0; i < 1000; i++ ){
		CanMessage msg = createCanMsg();
		for ( unsigned int k = 0; k < cca_vect.size(); k++ ){
			cca_vect[k]->sendMessage( &msg );
		}
	}
	// check
	{
		showConnections();
		for ( std::vector<CanModule::CCanAccess*>::iterator it = cca_vect.begin() ; it != cca_vect.end(); ++it ){
			showBusStats( *it );
		}
	}
	boost::this_thread::sleep(boost::posix_time::microseconds( 2000000 )); // 2 sec
	{
		showConnections();
		for ( std::vector<CanModule::CCanAccess*>::iterator it = cca_vect.begin() ; it != cca_vect.end(); ++it ){
			showBusStats( *it );
		}
	}

	libana->closeCanBus( canana1 );
	libpeak->closeCanBus( canpeak1 );
	libsystec->closeCanBus( cansystec3 );
	libsystec->closeCanBus( cansystec5 );
	std::cout << __FUNCTION__ << " OK finished" << std::endl;
	exit(0);
}

/**
 * - access one bridge two ports from two different tasks, same vendor
 * - 2 ports per task, so 4 ports all together for the complete test, crossed
 * - bridge sharing but no vendor mixing
 * - test reconnection behavior:
 *      per single port
 *      per all connected ports per task
 *      on condition: reception timeout (with different timeouts)
 *                    sendFail (with a counter)
 *                    never
 *                    and power cut
 *
 * - check sending messages and reception handler reconnection
 */
void SpecialCommands::uni_multi_shared0(std::string vendor, std::string ip0, int porta,  std::string ip1, int portb, int scenario ){
	std::string vlib;
	std::string name0;
	std::string name1;
	std::string param = "Unspecified"; // defaults
	if ( vendor == "systec" ){
		std::cout << __FUNCTION__ << " OK need two systec bridges can0,1 and can8,9 (one systec16)"	 << std::endl;
#ifdef _WIN32
		vlib = "st";
#else
		vlib = "sock";
#endif
	} else if ( vendor == "peak"){
		std::cout << __FUNCTION__ << " OK need two peak duo bridge can0,1 and can 2,3 " << std::endl;
#ifdef _WIN32
		vlib = "pk";
#else
		vlib = "sock";
#endif
	} else if ( vendor == "anagate"){
		std::cout << __FUNCTION__ << " OK need one anagate duo and one X4 using " << ip0 << " " << ip1 << std::endl;
		vlib = "an";
	} else {
		std::cout << __FUNCTION__ << " ERR vendor " << vendor << " unknown, exiting." << std::endl;
		exit(-1);
	}

	name0 = vlib + ":" + std::to_string( porta );
	name1 = vlib + ":" + std::to_string( portb );
	if ( vendor == "anagate"){
		name0 = name0 + ":" + ip0;
		name1 = name1 + ":" + ip1;
	}
	if ( vendor == "peak" ){
		name0 = name0 + ":device"+ip0;
		name1 = name1 + ":device"+ip1;
	}
	CanModule::CanLibLoader* lib = CanModule::CanLibLoader::createInstance( vlib );
	std::vector<CanModule::CCanAccess*> cca_vect;
	cca_vect.clear();
	CanModule::CCanAccess* can0 = lib->openCanBus( name0, param );
	cca_vect.push_back( can0 );
	CanModule::CCanAccess* can1 = lib->openCanBus( name1, param );
	cca_vect.push_back( can1 );
	for (std::vector<CanModule::CCanAccess*>::iterator it = cca_vect.begin() ; it != cca_vect.end(); ++it){
		if ( !(*it) ) 	{
			std::cout << __FUNCTION__ << " ERR openCanBus failed" << std::endl;
			exit(-1);
		}
	}

	// connect 2 handlers
	can0->canMessageCame.connect( &SpecialCommands::reception0 );
	can1->canMessageCame.connect( &SpecialCommands::reception1 );

	// test both handler with direct signal
	{
		std::cout << __FUNCTION__ << " OK testing handlers:" << std::endl;
		CanMessage msg = createCanMsg();
		can0->canMessageCame(msg);
		can1->canMessageCame(msg);
		std::cout << __FUNCTION__ << " OK testing handlers done" << std::endl;
	}



	int delay_us = 5000;
	int loopCounter = 10000;
	switch ( scenario ){
	case 0:{
		can0->setReconnectBehavior( CanModule::ReconnectAutoCondition::sendFail, CanModule::ReconnectAction::singleBus );
		can0->setReconnectFailedSendCount( 3 );
		can1->setReconnectBehavior( CanModule::ReconnectAutoCondition::never, CanModule::ReconnectAction::singleBus );
		break;
	}
	case 1:{
		can0->setReconnectBehavior( CanModule::ReconnectAutoCondition::timeoutOnReception, CanModule::ReconnectAction::singleBus );
		can1->setReconnectReceptionTimeout( 5 );
		can1->setReconnectBehavior( CanModule::ReconnectAutoCondition::never, CanModule::ReconnectAction::singleBus );
		break;
	}
	case 2:{
		if ( vendor == "sock" ) {
			std::cout << __FUNCTION__ << " OK sock implementation does not support action \"allBusesOnBridge\" " << std::endl;
			exit(0);
		}
		can0->setReconnectBehavior( CanModule::ReconnectAutoCondition::timeoutOnReception, CanModule::ReconnectAction::allBusesOnBridge );
		can0->setReconnectReceptionTimeout( 120 );
		can1->setReconnectBehavior( CanModule::ReconnectAutoCondition::never, CanModule::ReconnectAction::allBusesOnBridge );
		break;
	}
	default:{
		std::cout << __FUNCTION__ << " ERR unknown scenario " << scenario << " , exiting.." << std::endl;
		exit(-1);
	}
	}

	// check
	{
		showConnections();
		for ( std::vector<CanModule::CCanAccess*>::iterator it = cca_vect.begin() ; it != cca_vect.end(); ++it ){
			showBusStats( (*it) );
		}
		std::cout << __FUNCTION__ << " condition0= " << (int) can0->getReconnectCondition() << " "
				<< CanModule::CCanAccess::reconnectConditionString( can0->getReconnectCondition()) << std::endl;
		std::cout << __FUNCTION__ << " action0= " << (int) can0->getReconnectAction()<< " "
				<< CanModule::CCanAccess::reconnectActionString( can0->getReconnectAction()) << std::endl;
		std::cout << __FUNCTION__ << " condition1= " << (int) can1->getReconnectCondition()<< " "
				<< CanModule::CCanAccess::reconnectConditionString( can1->getReconnectCondition()) << std::endl;
		std::cout << __FUNCTION__ << " action1= " << (int) can1->getReconnectAction()<< " "
				<< CanModule::CCanAccess::reconnectActionString( can1->getReconnectAction()) << std::endl;
	}
	// exit(0);

	for ( int i = 0; i < loopCounter; i++ ){
		std::cout << __FILE__ << " " << __LINE__ << " OK *** countdown= " << loopCounter - i << " delay_ms= " << delay_us/1000 << std::endl;
		CanMessage msg = createCanMsg();
		can0->sendMessage( &msg );
		boost::this_thread::sleep(boost::posix_time::microseconds( delay_us ));
		can1->sendMessage( &msg );
		boost::this_thread::sleep(boost::posix_time::microseconds( delay_us ));
	}
	// check
	{
		showConnections();
		for ( std::vector<CanModule::CCanAccess*>::iterator it = cca_vect.begin() ; it != cca_vect.end(); ++it ){
			showBusStats( (*it) );
		}
	}

	lib->closeCanBus( can0 );
	lib->closeCanBus( can1 );
	std::cout << __FUNCTION__ << " OK finished" << std::endl;
	exit(0);
}


/**
 * access two bridges of the same vendor, shared bridges
 * - access two bridges two ports each from two different tasks, crossed over, same vendor
 * - makes 4 ports to access in total, crossed between tasks
 * - bridge sharing, multiple bridges and ports but no vendor mixing
 * - test reconnection behavior:
 *      per single port
 *      per all connected ports per task
 *      on condition: reception timeout (with different timeouts)
 *                    send1Fail
 *                    send10Fail
 *                    never
 *                    and power cut
 *
 * - check sending messages and reception handler reconnection
 * - can also check library reloading
 *
 * example:
 * task0: CANXtester -T -uni_multi_shared1 anagate 0 anaethcanduo anaethcan8x
 * task1: CANXtester -T -uni_multi_shared1 anagate 1 anaethcanduo anaethcan8x
 *
 */
void SpecialCommands::uni_multi_shared1(std::string vendor, int threadNo /* 0 or 1 */, std::string ip0, std::string ip1 ){
	std::string vlib;
	std::string name0;
	std::string name1;
	std::string name2;
	std::string name3;
	std::string param = "Unspecified"; // defaults


	std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " vendor= " << vendor << std::endl;

	if ( vendor == "systec"){
		std::cout << __FUNCTION__ << " need two systec bridges (one systec16)  can0, can1, can8, can9 "	 << std::endl;
#ifdef _WIN32
		vlib = "st";
		name0 = "st:0"; // 1st port first systec16
		name1 = "st:1"; // 2nd port first systec16
		name2 = "st:8"; // 1st port second systec16
		name3 = "st:9"; // 2nd port second systec16
#else
		vlib = "sock";
		name0 = "sock:0";
		name1 = "sock:1";
		name0 = "sock:8";
		name1 = "sock:9";
#endif
	} else if ( vendor == "peak"){
		std::cout << __FUNCTION__ << " need two peak duo bridges " << std::endl;
		std::cout << __FUNCTION__ << " using can0, can1, can2, can3" << std::endl;
#ifdef _WIN32
		vlib = "pk";
		name0 = "pk:0"; // 1st port first systec16
		name1 = "pk:1"; // 2st port first systec16
		name2 = "pk:2"; // 1st port second systec16
		name3 = "pk:3"; // 2nd port second systec16
#else
		vlib = "sock";
		name0 = "sock:0";
		name1 = "sock:1";
		name0 = "sock:2";
		name1 = "sock:3";
#endif
	} else if ( vendor == "anagate"){
		std::cout << __FUNCTION__ << " need two anagate bridges " << std::endl;
		std::cout << __FUNCTION__ << " using " << ip0 << " " << ip1 << std::endl;
		vlib = "an";
		name0 = "an:0:" + ip0; // 1st port anagate0
		name1 = "an:1:" + ip0; // 2nd port anagate0
		name2 = "an:0:" + ip1; // 1st port anagate1
		name3 = "an:1:" + ip1; // 2nd port anagate1
	} else {
		std::cout << __FUNCTION__ << " vendor " << vendor << " unknown, exiting." << std::endl;
		exit(-1);
	}

	CanModule::CanLibLoader* lib = CanModule::CanLibLoader::createInstance( vlib );
	std::string namea;
	std::string nameb;
	switch ( threadNo ){
	case 0:{
		namea = name0; // first bridge, first thread
		nameb = name2; // second bridge, first thread
		break;
	}
	case 1:{
		namea = name1; // first bridge, second thread
		nameb = name3; // second bridge, second thread
		break;
	}
	default:{
		std::cout << __FUNCTION__ << " threadNo " << threadNo << " unknown, exiting." << std::endl;
		exit(-1);
	}
	}
	std::vector<CanModule::CCanAccess*> cca_vect;
	cca_vect.clear();

	CanModule::CCanAccess* cana = lib->openCanBus( namea, param );
	cca_vect.push_back( cana );
	CanModule::CCanAccess* canb = lib->openCanBus( nameb, param );
	cca_vect.push_back( canb );
	for (std::vector<CanModule::CCanAccess*>::iterator it = cca_vect.begin() ; it != cca_vect.end(); ++it){
		if ( !(*it) ) 	{
			std::cout << __FUNCTION__ << " openCanBus failed" << std::endl;
			exit(-1);
		}
	}

	// check
	{
		showConnections();
		for ( std::vector<CanModule::CCanAccess*>::iterator it = cca_vect.begin() ; it != cca_vect.end(); ++it ){
			showBusStats( *it );
		}
	}

	for ( int i = 0; i < 30000; i++ ){
		CanMessage msg = createCanMsg();
		cana->sendMessage( &msg );
		canb->sendMessage( &msg );
		boost::this_thread::sleep(boost::posix_time::microseconds( 10000 ));
	}
	lib->closeCanBus( cana );
	lib->closeCanBus( canb );
	std::cout << __FUNCTION__ << " finished" << std::endl;
	exit(0);
}

/**
 * access one bridge of EACH vendor, shared bridges
 * - two ports each bridge from two different tasks
 * - bridge sharing, multiple bridges and vendor mixing
 * - can test reconnection behavior
 * - just check sending messages, no reception
 * - disconnect any bridge and see if it is reconnected on both tasks
 */
void SpecialCommands::mixed_multi_shared0( int threadNo /* 0 or 1 */, std::string ip0, std::string ip1 ){
	std::string vlibx;
	std::string vliby;
	std::string vlibz;
	std::string name0;
	std::string name1;
	std::string name2;
	std::string name3;
	std::string name4;
	std::string name5;
	std::string param = "Unspecified"; // defaults
#ifdef _WIN32
	vlibx = "st";
	vliby = "pk";
	name0 = "st:0";
	name1 = "st:1";
	name2 = "pk:0";
	name3 = "pk:1";
#else
	// need a systec16 USB0 and a peak duo
	vlibx = "sock";
	vliby = "sock";
	name0 = "sock:0";
	name1 = "sock:1";
	name2 = "sock:8";
	name3 = "sock:9";
#endif
	vlibz = "an";
	name4 = "an:0:" + ip0; // 1st port anagate0
	name5 = "an:1:" + ip0; // 2nd port anagate0

	CanModule::CanLibLoader* libx = CanModule::CanLibLoader::createInstance( vlibx );
	CanModule::CanLibLoader* liby = CanModule::CanLibLoader::createInstance( vliby );
	CanModule::CanLibLoader* libz = CanModule::CanLibLoader::createInstance( vlibz );
	std::string namea;
	std::string nameb;
	std::string namec;
	switch ( threadNo ){
	case 0:{
		// first thread all bridges
		namea = name0;
		nameb = name2;
		namec = name4;
		break;
	}
	case 1:{
		// second thread all bridges
		namea = name1;
		nameb = name3;
		namec = name5;
		break;
	}
	default:{
		std::cout << __FUNCTION__ << " threadNo " << threadNo << " unknown, exiting." << std::endl;
		exit(-1);
	}
	}
	std::vector<CanModule::CCanAccess*> cca_vect;
	cca_vect.clear();

	CanModule::CCanAccess* cana = libx->openCanBus( namea, param );
	cca_vect.push_back( cana );
	CanModule::CCanAccess* canb = liby->openCanBus( nameb, param );
	cca_vect.push_back( canb );
	CanModule::CCanAccess* canc = libz->openCanBus( namec, param );
	cca_vect.push_back( canc );

	for (std::vector<CanModule::CCanAccess*>::iterator it = cca_vect.begin() ; it != cca_vect.end(); ++it){
		if ( !(*it) ) 	{
			std::cout << __FUNCTION__ << " openCanBus failed" << std::endl;
			exit(-1);
		}
	}

	CanMessage msg = createCanMsg();

	// check
	{
		showConnections();
		for ( std::vector<CanModule::CCanAccess*>::iterator it = cca_vect.begin() ; it != cca_vect.end(); ++it ){
			showBusStats( *it );
		}
	}

	for ( int i = 0; i < 30000; i++ ){
		cana->sendMessage( &msg );
		canb->sendMessage( &msg );
		canc->sendMessage( &msg );
		boost::this_thread::sleep(boost::posix_time::microseconds( 10000 ));
	}
	libx->closeCanBus( cana );
	liby->closeCanBus( canb );
	libz->closeCanBus( canc );

	std::cout << __FUNCTION__ << " finished" << std::endl;
	exit(0);
}

// very random crap message, but keep within standard CAN ID 11 bits, 0x7ff
CanMessage SpecialCommands::createCanMsg(){
	// int16_t m_id = 0;
	static uint8_t counter;
	const std::string allowed="abcdefghijklmnopqrstuvwxyz0123456789";

	CanMessage msg = CanMessage();
	for ( int i = 0; i < 8; i++ ){
		msg.c_data[ i ] = allowed[ counter ];
		if ( counter < allowed.size() - 1 ){
			counter++;
		} else {
			counter = 0;
		}
	}
	msg.c_dlc = 8;
	msg.c_id = m_id;
	msg.c_time.tv_sec = 0;
	msg.c_time.tv_usec = 0;
	msg.c_ff = 0;
	msg.c_rtr = 0;
	if ( m_id < 0x7ff ) {
		m_id++;
	} else {
		m_id = 0;
	}
	return (msg );
}

/**
 * testing CanAccess (filimonov) wrapper
 */
void SpecialCommands::api_canaccess( std::string vendor, std::string ip0, std::string ip1 ){
	std::cout << __FUNCTION__ << std::endl;
	std::string vlib;
	std::string name0;
	std::string name1;
	std::string param = "Unspecified"; // defaults
	if ( vendor == "systec"){
		std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__
				<< " need two systec bridges "
				<< " (or one systec16) with 2 USB" << std::endl;
		std::cout << __FUNCTION__ << " using can0 and can8" << std::endl;
#ifdef _WIN32
		vlib = "st";
		name0 = "st:0"; // 1st port first systec16
		name1 = "st:8"; // 1st port second systec16
#else
		vlib = "sock";
		name0 = "sock:0";
		name1 = "sock:8";
#endif
	} else if ( vendor == "peak"){
		std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " need two peak duo bridges "
				<< " with 2 USB" << std::endl;
		std::cout << __FUNCTION__ << " using can0 and can2" << std::endl;
#ifdef _WIN32
		vlib = "pk";
		name0 = "pk:0"; // 1st port first systec16
		name1 = "pk:2"; // 1st port second systec16
#else
		vlib = "sock";
		// need extended deviceID for peak, use decimal number and LOCAL PORT NUMBERS
		name0 = "sock:0:device"+ip0;
		name1 = "sock:0:device"+ip1;
#endif
	} else if ( vendor == "anagate"){
		std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " need two anagate bridges " << std::endl;
		std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " using " << ip0 << " and " << ip1  << std::endl;
		vlib = "an";
		name0 = "an:0:" + ip0; // 1st port first anagate
		name1 = "an:0:" + ip1; // 1st port second anagate
	} else {
		std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " vendor " << vendor << " unknown, exiting." << std::endl;
		exit(-1);
	}
	std::vector<CanModule::CCanAccess*> cca_vect;
	CanModule::CanBusAccess *ca = new CanModule::CanBusAccess();
	CCanAccess *can0 = ca->openCanBus( name0, param);
	CCanAccess *can1 = ca->openCanBus( name1, param);
	cca_vect.push_back( can0 );
	cca_vect.push_back( can1 );
	for (std::vector<CanModule::CCanAccess*>::iterator it = cca_vect.begin() ; it != cca_vect.end(); ++it){
		if ( !(*it) ) 	{
			std::cout << __FUNCTION__ << " openCanBus failed" << std::endl;
			exit(-1);
		}
	}

	can0->canMessageCame.connect( &SpecialCommands::reception0 );
	can1->canMessageCame.connect( &SpecialCommands::reception1 );

	// test them by sending the signal with a fake message
	std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " test can0 reception0 with direct signal" << std::endl;
	CanMessage msg = createCanMsg();
	can0->canMessageCame(msg);
	boost::this_thread::sleep(boost::posix_time::microseconds( 10000 ));

	std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " test can1 reception1 with direct signal" << std::endl;
	msg = createCanMsg();
	can1->canMessageCame(msg);
	boost::this_thread::sleep(boost::posix_time::microseconds( 10000 ));

	// check
	{
		showConnections();
		for ( std::vector<CanModule::CCanAccess*>::iterator it = cca_vect.begin() ; it != cca_vect.end(); ++it ){
			showBusStats( *it );
		}
	}

	// go at 5Hz, receptions should be in the same order as sends
	for ( int i = 0; i < 3; i++ ){
		msg = createCanMsg();
		if ( !can0->sendMessage( &msg ) ){
			std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " send failed can0" << std::endl;
			exit(-1);
		} else {
			std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " send can0 " << i << std::endl;
		}
		boost::this_thread::sleep(boost::posix_time::microseconds( 200000 ));

		if ( !can1->sendMessage( &msg ) ){
			std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " send failed can1" << std::endl;
			exit(-1);
		} else {
			std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " send can1 " << i << std::endl;
		}
		boost::this_thread::sleep(boost::posix_time::microseconds( 200000 ));
	}

	ca->closeCanBus( can0 );
	ca->closeCanBus( can1 );

	std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " finished" << std::endl;
	exit(0);
}

/**
SHARED_LIB_EXPORT_DEFN struct HARDWARE_DIAG_t {
	int temperature; // in deg C
	int uptime;      // in seconds
	unsigned int clientCount; // connected clients for this IP/module/submodule
	std::vector<std::string> clientIps; // decoded into strings, from unsigned int
	std::vector<unsigned int> clientPorts; // array of client ports
	std::vector<long int> clientConnectionTimestamps; // (Unix time) of initial client connection
};
*/
void SpecialCommands::showHwDiag(CanModule::HARDWARE_DIAG_t d){
	std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << std::dec << " d.temperature= " << d.temperature << " degC" << std::endl;
	std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << std::dec << " d.uptime= " << d.uptime << " sec" << std::endl;
	std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << std::dec << " d.clientCount= " << d.clientCount << std::endl;
	for ( unsigned int i = 0; i < d.clientIps.size(); i++ ){
		std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " d.clientIps[ " << i << " ]= " << d.clientIps[ i ] << std::endl;
	}
	for ( unsigned int i = 0; i < d.clientPorts.size(); i++ ){
		std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << std::dec << " d.clientPorts[ " << i << " ]= " << d.clientPorts[ i ] << std::endl;
	}
	for ( unsigned int i = 0; i < d.clientConnectionTimestamps.size(); i++ ){
		std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << std::dec << " d.clientConnectionTimestamps[ " << i << " ]= " << d.clientConnectionTimestamps[ i ] << std::endl;
	}
}

/**SHARED_LIB_EXPORT_DEFN struct PORT_COUNTERS_t {
	unsigned int countTCPRx; // TCP Received counter.
	unsigned int countTCPTx; // TCP Transmitted counter.
	unsigned int countCANRx; // CAN Received counter.
	unsigned int countCANTx; // CAN Transmitted counter.
	unsigned int countCANRxErr; // CAN Bus Receive Error counter.
	unsigned int countCANTxErr; // CAN Bus Transmit Error counter.
	unsigned int countCANRxDisc; // CAN Discarded Rx Full Queue counter.
	unsigned int countCANTxDisc; // CAN Discarded Tx Full Queue counter.
	unsigned int countCANTimeout; // CAN Transmit Timeout counter.
	*/
void SpecialCommands::showHwCounters(CanModule::PORT_COUNTERS_t c ){
	std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " c.countTCPRx= " << c.countTCPRx << std::endl;
	std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " c.countTCPTx= " << c.countTCPTx << std::endl;
	std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " c.countCANRx= " << c.countCANRx << std::endl;
	std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " c.countCANTx= " << c.countCANTx << std::endl;
	std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " c.countCANRxErr= " << c.countCANRxErr << std::endl;
	std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " c.countCANTxErr= " << c.countCANTxErr << std::endl;
	std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " c.countCANRxDisc= " << c.countCANRxDisc << std::endl;
	std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " c.countCANTxDisc= " << c.countCANTxDisc << std::endl;
	std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " c.countCANTimeout= " << c.countCANTimeout << std::endl;
}

/** SHARED_LIB_EXPORT_DEFN struct  PORT_LOG_ITEM_t {
	std::string message;
	long int timestamp;
};
*/
void SpecialCommands::showHwLog(std::vector<CanModule::PORT_LOG_ITEM_t> log){
	for ( unsigned int i = 0; i < log.size(); i++ ){
		std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " log[ " << i << " ]= "
				<< log[ i ].timestamp << " "
				<< log[ i ].message << std::endl;
	}
}


/**
 * super simple read the diags from port of the anagate2 implementation. Stay in a loop for 30 secs
 * so that we can connect several instances
 */
void SpecialCommands::anagate_diag0( int p, std::string ip0 ){
	std::cout << __FUNCTION__ << std::endl;
	CanModule::CanLibLoader* lib0 = CanModule::CanLibLoader::createInstance( "an2" );
	std::string param = "125000 0 1 0 0";
	std::string  name0 = "an2:" + std::to_string( p ) + ":" + ip0;
	CanModule::CCanAccess* can0 = lib0->openCanBus( name0, param );



	for ( int i = 0; i < 300; i++ ){
		// read all diags
		CanModule::HARDWARE_DIAG_t hw_diag0 = can0->getHwDiagnostics();
		CanModule::PORT_COUNTERS_t hw_counters0 = can0->getHwCounters();
		std::vector<CanModule::PORT_LOG_ITEM_t> portLog0 = can0->getHwLogMessages( 0 );

		// show
		showHwDiag( hw_diag0 );
		showHwCounters( hw_counters0 );
		showHwLog( portLog0 );

		ms_sleep( 1000 );

	}
	lib0->closeCanBus( can0 );

	std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " OK finished" << std::endl;
	exit(0);
}

/**
 * testing new diagnostic functionality beta.v6
 * use two separate anagate bridges(ips), with nb of ports on each
 * ip0: prototype, ip1, any other anagate bridge without the diag firmware
 * port cable connections : ip0:p0<->ip1:p0, ip0:p1<->ip1:p1 etc..
 * up to 16 ports in a stress test, pstart to including pend (can0..can15)
 * specify frame delay in ms (0 for full speed)
 */
void SpecialCommands::anagate_diag1( int pstart, int pend, int delay_ms, std::string ip0, std::string ip1 ){
	std::cout << __FUNCTION__ << std::endl;
	std::string vlib;
	std::string name0;
	std::string name1;
	/** @param name 3 parameters separated by ":" like "n0:n1:n2"
	 * 		n0 = "an" for anagate
	 * 		n1 = port number on the module, 0=A, 1=B, etc etc
	 * 		n2 = ip number
	 * 		ex.: "an:can1:137.138.12.99" speaks to port B (=1) on anagate module at the ip
	 * 		ex.: "an:1:137.138.12.99" works as well
	 *
	 *
	 * @param parameters up to 5 parameters separated by whitespaces : "p0 p1 p2 p3 p4" in THAT order, positive integers
	 * 				* "Unspecified" (or empty): using defaults = "125000 0 0 0 0" // all params missing
	 * 				* p0: bitrate: 10000, 20000, 50000, 62000, 100000, 125000, 250000, 500000, 800000, 1000000 bit/s
	 * 				* p1: operatingMode: 0=default mode, values 1 (loop back) and 2 (listen) are not supported by CanModule
	 * 				* p2: termination: 0=not terminated (default), 1=terminated (120 Ohm for CAN bus)
	 * 				* p3: highSpeed: 0=deactivated (default), 1=activated. If activated, confirmation and filtering of CAN traffic are switched off
	 * 				      for baud rates > 125000 this flag is needed, and it is set automatically for you if you forgot it (WRN)
	 * 				* p4: TimeStamp: 0=deactivated (default), 1=activated. If activated, a timestamp is added to the CAN frame. Not all modules support this.
	 *				  i.e. "250000 0 1 0 0"
	 * 				(see anagate manual for more details)
	 */
	std::string param = "125000 0 1 0 0";

	// before doing anything, we connect a handler to the global errors and test it
	CanModule::GlobalErrorSignaler *gsig = CanModule::GlobalErrorSignaler::getInstance();
	gsig->connectHandler( &SpecialCommands::globalErrorHandler0 );
	{
		std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " testing global error signal handler:" << std::endl;
		gsig->fireSignal( 11, "test global error 11" );
		std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " testing global error signal handler done" << std::endl;
	}

	std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " OK need two anagate bridges, first bridge CERN prototype. using " << ip0 << " and " << ip1  << std::endl;

	std::vector<CanModule::CCanAccess*> cca_vect0;
	std::vector<CanModule::CCanAccess*> cca_vect1;
	std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " creating lib instances.." << std::endl;
	CanModule::CanLibLoader* lib0 = CanModule::CanLibLoader::createInstance( "an2" ); // prototype
	CanModule::CanLibLoader* lib1 = CanModule::CanLibLoader::createInstance( "an" );

	for ( int p = pstart; p <= pend; p++ ){
		name0 = "an2:" + std::to_string( p ) + ":" + ip0; // n port first anagate CERN prototype
		CanModule::CCanAccess* can0 = lib0->openCanBus( name0, param );
		std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " opening bus " << name0 << " " << param << std::endl;
		if ( can0 ){
			cca_vect0.push_back( can0 );
		} else {
			std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " ERR can't open port " << name0 << ", test failed" << std::endl;
		}

		name1 = "an:" + std::to_string( p )  + ":" + ip1; // n port first anagate NOT CERN prototype
		CanModule::CCanAccess* can1 = lib1->openCanBus( name1, param );
		std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " opening bus " << name1 << " " << param << std::endl;
		if ( can1 ){
			cca_vect1.push_back( can1 );
		} else {
			std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " ERR can't open port " << name1 << ", test failed" << std::endl;
		}

		// connect THE SAME reception handler to all ports of ip0, and ONE MORE to all ports of ip1
		std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " OK connecting reception methods" << std::endl;
		can0->canMessageCame.connect( &SpecialCommands::receptionAll0 );
		can1->canMessageCame.connect( &SpecialCommands::receptionAll1 );

		// connect port error handler THE SAME and ONE MORE
		can0->canMessageError.connect( &SpecialCommands::errorAll0 );
		can1->canMessageError.connect( &SpecialCommands::errorAll1 );

		// connect port status change handler THE SAME and ONE MORE
		can0->canPortStateChanged.connect( &SpecialCommands::portStatusChangedAll0 );
		can1->canPortStateChanged.connect( &SpecialCommands::portStatusChangedAll1 );
	}

	// BEFORE: read all diags both ip0 and ip1
	for ( unsigned int i = 0; i < cca_vect0.size(); i++ ){
		CanModule::HARDWARE_DIAG_t hw_diag0 = cca_vect0[ i ]->getHwDiagnostics();
		CanModule::PORT_COUNTERS_t hw_counters0 = cca_vect0[ i ]->getHwCounters();
		std::vector<CanModule::PORT_LOG_ITEM_t> portLog0 = cca_vect0[ i ]->getHwLogMessages( 0 );

		CanModule::HARDWARE_DIAG_t hw_diag1 = cca_vect1[ i ]->getHwDiagnostics();
		CanModule::PORT_COUNTERS_t hw_counters1 = cca_vect1[ i ]->getHwCounters();
		std::vector<CanModule::PORT_LOG_ITEM_t> portLog1 = cca_vect1[ i ]->getHwLogMessages( 0 );

		// show
		std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " hw diag ip0 " << cca_vect0[ i ]->getBusName() << " : " << std::endl;
		showHwDiag( hw_diag0 );
		showHwCounters( hw_counters0 );
		showHwLog( portLog0 );

		std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " hw diag ip1 " << cca_vect1[ i ]->getBusName() << " : " << std::endl;
		showHwDiag( hw_diag1 );
		showHwCounters( hw_counters1 );
		showHwLog( portLog1 );
	}

	// now, send a burst of messages from ip0 to ip1
	// same message on all ports for each round
	// ip1 should just receive all messages on the handler for All
	int nbRounds = 10000 / (delay_ms + 1);
	std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " sending" << nbRounds << " frames on all ports of ip0 " << ip0 << std::endl;
	for ( int i = 0; i < nbRounds; i++ ){
		CanMessage msg = createCanMsg();
		for ( unsigned int i = 0; i < cca_vect0.size(); i++ ){
			if ( !cca_vect0[ i ]->sendMessage( &msg ) ){
				std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " ERR send failed " << cca_vect0[ i ]->getBusName() << std::endl;
			}
			ms_sleep( delay_ms );
		}
	}

//	ms_sleep( 3000 ); // message buffers empty

	// now, send a burst of messages from ip1 to ip0
	// same message on all ports for each round
	// ip0 should just receive all messages on the handler for All
	std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " sending" << nbRounds << " frames on all ports of ip1 " << ip1 << std::endl;
	for ( int i = 0; i < nbRounds; i++ ){
		CanMessage msg = createCanMsg();
		for ( unsigned int i = 0; i < cca_vect1.size(); i++ ){
			if ( !cca_vect1[ i ]->sendMessage( &msg ) ){
				std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " ERR send failed " << cca_vect1[ i ]->getBusName() << std::endl;
			}
			ms_sleep( delay_ms );
		}
	}

	ms_sleep( 3000 ); // message buffers empty

	// AFTER: read all diags both ip0 and ip1
	for ( unsigned int i = 0; i < cca_vect0.size(); i++ ){
		CanModule::HARDWARE_DIAG_t hw_diag0 = cca_vect0[ i ]->getHwDiagnostics();
		CanModule::PORT_COUNTERS_t hw_counters0 = cca_vect0[ i ]->getHwCounters();
		std::vector<CanModule::PORT_LOG_ITEM_t> portLog0 = cca_vect0[ i ]->getHwLogMessages( 0 );

		CanModule::HARDWARE_DIAG_t hw_diag1 = cca_vect1[ i ]->getHwDiagnostics();
		CanModule::PORT_COUNTERS_t hw_counters1 = cca_vect1[ i ]->getHwCounters();
		std::vector<CanModule::PORT_LOG_ITEM_t> portLog1 = cca_vect1[ i ]->getHwLogMessages( 0 );

		// show
		std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " hw diag ip0 " << cca_vect0[ i ]->getBusName() << " : " << std::endl;
		showHwDiag( hw_diag0 );
		showHwCounters( hw_counters0 );
		showHwLog( portLog0 );

		std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " hw diag ip1 " << cca_vect1[ i ]->getBusName() << " : " << std::endl;
		showHwDiag( hw_diag1 );
		showHwCounters( hw_counters1 );
		showHwLog( portLog1 );
	}

	// check and stats
	showConnections();
	for ( std::vector<CanModule::CCanAccess*>::iterator it = cca_vect0.begin() ; it != cca_vect0.end(); ++it ){
		showBusStats( *it );
	}
	for ( std::vector<CanModule::CCanAccess*>::iterator it = cca_vect1.begin() ; it != cca_vect1.end(); ++it ){
		showBusStats( *it );
	}

	// we can close all of them
	for ( unsigned int i = 0; i < cca_vect0.size(); i++ ){
		lib0->closeCanBus( cca_vect0[ i ] );
	}
	for ( unsigned int i = 0; i < cca_vect1.size(); i++ ){
		lib1->closeCanBus( cca_vect1[ i ] );
	}

	std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " OK finished" << std::endl;
	exit(0);

}

/**
 * **objectives:**
 * * anagate test new functionality from v6.beta api, on new module only of course
 * * access two anagate bridges: bridge0=new, bridge1=old, both using the new API
 * * send a couple of frames in both directions
 * * check port statis and error handlers
 * * sending messages, receiving messages, statistics, using short circuit by flatband cable btween both ports
 * * all vendors work with all OSes
 * * check boost signal for the reception handler
 * * no reconnection tests, just a few slow messages
 */
void SpecialCommands::anagate0( std::string ip0, std::string ip1 ){
	std::cout << __FUNCTION__ << std::endl;
	std::string vlib;
	std::string name0;
	std::string name1;
	std::string param = "Unspecified"; // defaults

	std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " OK need two anagate bridges, first bridge CERN prototype " << std::endl;
	std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " OK using " << ip0 << " and " << ip1  << std::endl;
	vlib = "an";
	name0 = "an:0:" + ip0; // 1st port first anagate CERN prototype
	name1 = "an:0:" + ip1; // 1st port second anagate whatever


	std::vector<CanModule::CCanAccess*> cca_vect;
	std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " creating lib instance.." << std::endl;
	CanModule::CanLibLoader* lib0 = CanModule::CanLibLoader::createInstance( vlib );
	std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " OK created lib instance" << std::endl;
	std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " opening bus " << name0 << " " << param << std::endl;
	CanModule::CCanAccess* can0 = lib0->openCanBus( name0, param );
	std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " opening bus " << name0 << " " << param << std::endl;
	CanModule::CCanAccess* can1 = lib0->openCanBus( name1, param );
	if ( can0 ){
		cca_vect.push_back( can0 );
	} else {
		std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " ERR can't open port " << name0 << ", test failed" << std::endl;
	}

	if ( can1 ) {
		cca_vect.push_back( can1 );
	} else {
		std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " ERR can't open port " << name1 << ", test failed" << std::endl;
	}

	for (std::vector<CanModule::CCanAccess*>::iterator it = cca_vect.begin() ; it != cca_vect.end(); ++it){
		if ( !(*it) ) 	{
			std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " ERR openCanBus failed" << std::endl;
		}
	}


	// connect reception methods
	std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " OK connecting reception methods" << std::endl;
	can0->canMessageCame.connect( &SpecialCommands::reception0 );
	can1->canMessageCame.connect( &SpecialCommands::reception1 );

	// disable reconnection behavior
	can0->setReconnectBehavior( CanModule::ReconnectAutoCondition::never, CanModule::ReconnectAction::singleBus );
	can1->setReconnectBehavior( CanModule::ReconnectAutoCondition::never, CanModule::ReconnectAction::singleBus );


	// test handlers by sending the signal directly
	CanMessage msg = createCanMsg();
	can0->canMessageCame(msg);
	std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " OK test can0 reception0 with direct signal" << std::endl;
	msg = createCanMsg();
	can1->canMessageCame(msg);
	std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " OK test can1 reception1 with direct signal" << std::endl;

	// check
	{
		showConnections();
		for ( std::vector<CanModule::CCanAccess*>::iterator it = cca_vect.begin() ; it != cca_vect.end(); ++it ){
			showBusStats( *it );
		}
	}

	ms_sleep( 10 );
	std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " OK start sending/receiving frames both ports" << std::endl;

	// go at 5Hz, receptions should be in the same order as sends
	std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__
		<< " sending msg to opened ports" << std::endl;
	for ( int i = 0; i < 3; i++ ){
		msg = createCanMsg();
		if ( !can0->sendMessage( &msg ) ){
			std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " ERR send failed can0" << std::endl;
		} else {
			std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " OK send can0 " << i << std::endl;
		}

		ms_sleep( 200 );

		if ( !can1->sendMessage( &msg ) ){
			std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " ERR send failed can1" << std::endl;
		} else {
			std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " OK send can1 " << i << std::endl;
		}
		ms_sleep( 200 );
	}

	// check
	{
		showConnections();
		for ( std::vector<CanModule::CCanAccess*>::iterator it = cca_vect.begin() ; it != cca_vect.end(); ++it ){
			showBusStats( *it );
		}
	}

	// we can close all of them
	lib0->closeCanBus( can0 );
	lib0->closeCanBus( can1 );
	std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " OK finished" << std::endl;
	exit(0);
}

/**
 * **objectives:**
 * * send messages on first 2 ports of one module of one vendor
 * * like uni_mono_reserved0, but error reporting mechanism testing
 * * check statistics on open, send and receive for all ports
 * * check the bitrate settings for each port
 * * max speed
 *
 * * peak:
 * peak 9053
 */
void SpecialCommands::uni_mono_signals(std::string vendor, std::string ip0 ){
	// std::string param = "Unspecified"; // don't touch existing settings
	std::string param = "125000"; // low bitrate for everyone, no other settings
	std::string vlib;
	std::vector<std::string> namesv;
	unsigned int nbPorts = 2;
	if ( vendor == "systec"){
#ifdef _WIN32
		vlib = "st";
#else
		vlib = "sock";
#endif
		for ( unsigned int p = 0; p < nbPorts; p++){
			namesv.push_back( vlib + ":" + std::to_string(p));
		}
	} else if ( vendor == "peak"){
#ifdef _WIN32
		vlib = "pk";
		for ( unsigned int p = 0; p < nbPorts; p++){
			namesv.push_back( vlib + ":" + std::to_string(p));
		}
#else
		vlib = "sock";
		// need extended deviceID for peak, use decimal number and LOCAL PORT NUMBERS
		//name0 = "sock:0:device:"+ip0;
		//name1 = "sock:0:device:"+ip1;
		for ( unsigned int p = 0; p < nbPorts; p++)
			namesv.push_back( vlib + ":" + std::to_string(p) + ":device" + ip0);
#endif
	} else if ( vendor == "anagate"){
		std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " OK using " << ip0 << std::endl;
		vlib = "an";
		namesv.clear();
		for ( unsigned int p = 0; p < nbPorts; p++){
			namesv.push_back("an:" + std::to_string(p) + ":" + ip0);
		}
	} else {
		std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " vendor " << vendor << " ERR unknown" << std::endl;
		exit(-1);
	}

	// before doing anything, we connect a handler to the global errors and test it
	CanModule::GlobalErrorSignaler *gsig = CanModule::GlobalErrorSignaler::getInstance();
	gsig->connectHandler( &SpecialCommands::globalErrorHandler0 );
	gsig->connectHandler( &SpecialCommands::globalErrorHandler1 );
	{
		std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " testing global error signal handler:" << std::endl;
		gsig->fireSignal( 11, "test global error 11" );
		// we should have both handlers invoked: both should receive global error 11
		std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " testing global error signal handler done" << std::endl;
	}


	CanModule::CanLibLoader* lib = CanModule::CanLibLoader::createInstance( vlib );

	// create the can accesses. the bus open might still fail, and in this case we would like to have an error signal.
	std::vector<CanModule::CCanAccess*> cca_vect;
	cca_vect.clear();
	bool fail = false;
	for ( unsigned int n = 0; n < namesv.size(); n++){
		std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " OK opening bus name= " << namesv[ n ] << std::endl;
		cca_vect.push_back( lib->openCanBus( namesv[ n ], param ) );

		if ( ! cca_vect[ n ] ){
			std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " ERR check: openCanBus name= " << namesv[ n ]
			  << " has failed" << std::endl;
			fail = true;
		}
	}
	// to avoid segfaults on NULL
	if ( fail ) {
		std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " ERR at least one openCanBus failed";
		exit(-1);
	}

	// connect reception handlers to the first 2 ports and test the signals. Take also the mute handlers for max. speed
	cca_vect[ 0 ]->canMessageCame.connect( &SpecialCommands::reception0 );
	cca_vect[ 1 ]->canMessageCame.connect( &SpecialCommands::reception1 );
	{
		std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " testing reception handlers:" << std::endl;
		CanMessage msg = createCanMsg();
		cca_vect[ 0 ]->canMessageCame(msg);
		cca_vect[ 1 ]->canMessageCame(msg);
		std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " testing reception handlers done" << std::endl;
	}

	// connect error handlers to the first 2 ports and test the signals
	cca_vect[ 0 ]->canMessageError.connect( &SpecialCommands::error0 );
	cca_vect[ 1 ]->canMessageError.connect( &SpecialCommands::error1 );
	{
		std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " testing error handlers:" << std::endl;
		timeval ftTimeStamp = timevalTestSignalCanMessageError();
		cca_vect[ 0 ]->canMessageError(98, "test error 98", ftTimeStamp );
		cca_vect[ 1 ]->canMessageError(99, "test error 99", ftTimeStamp );
		std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " testing error handlers done" << std::endl;
	}

	// disconnect global handler now to avoid being flooded, keep object
	gsig->disconnectHandler( &SpecialCommands::globalErrorHandler0 );
	gsig->fireSignal( 12, "test global error 12" );
	// we should see handler1
	gsig->disconnectAllHandlers();
	gsig->fireSignal( 13, "test global error 13" );
	// we should see nothing


	// connect port status change  handlers to the first 2 ports and test the signals
	cca_vect[ 0 ]->canPortStateChanged.connect( &SpecialCommands::portStatusChanged0 );
	cca_vect[ 1 ]->canPortStateChanged.connect( &SpecialCommands::portStatusChanged1 );
	{
		std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " testing port state changed handlers:" << std::endl;
		timeval ftTimeStamp = timevalTestSignalCanMessageError();
		cca_vect[ 0 ]->canPortStateChanged(77, "test port status changed 77", ftTimeStamp );
		cca_vect[ 1 ]->canPortStateChanged(78, "test port status changed 78", ftTimeStamp );
		std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " testing  port state changed handlers done" << std::endl;
	}


	// set reconnection behavior
	for ( unsigned int iv = 0; iv < cca_vect.size(); iv++){
		cca_vect[ iv ]->setReconnectBehavior( CanModule::ReconnectAutoCondition::sendFail, CanModule::ReconnectAction::singleBus );
		cca_vect[ iv ]->setReconnectFailedSendCount( 3 );
	}

	// check
	{
		showConnections();
		for ( std::vector<CanModule::CCanAccess*>::iterator it = cca_vect.begin() ; it != cca_vect.end(); ++it ){
			showBusStats( *it );
		}
	}

	// ~a 2min 50Hz blast can0->can1, reverse ping-pong. No frames lost
	int kmax = 6000;
	for ( int k = 0; k < kmax; k++ ){
		if ( (k % 1000) == 0 ){
			std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " countdown k= " << kmax-k << std::endl;
		}

		for ( unsigned int i = 0; i < cca_vect.size(); i++ ){
			CanMessage msg = createCanMsg();
			cca_vect[ i ]->sendMessage( &msg );
		}
		ms_sleep( 20 );
	}


	// check
	{
		showConnections();
		for ( std::vector<CanModule::CCanAccess*>::iterator it = cca_vect.begin() ; it != cca_vect.end(); ++it ){
			showBusStats( *it );
		}
	}
	for ( unsigned int k = 0; k < cca_vect.size(); k++ ){
		lib->closeCanBus( cca_vect[ k ] );
	}
	std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " OK finished" << std::endl;
	exit(0);
}

timeval SpecialCommands::timevalTestSignalCanMessageError()
{
	timeval ftTimeStamp;
	// auto now = std::chrono::system_clock::now();
	auto now = std::chrono::high_resolution_clock::now();
	auto nMicrosecs =
			std::chrono::duration_cast<std::chrono::microseconds>( now.time_since_epoch());
	ftTimeStamp.tv_sec = nMicrosecs.count() / 1000000L;
	ftTimeStamp.tv_usec = (nMicrosecs.count() % 1000000L) ;
	return( ftTimeStamp );
}


/**
 * use 2 ports which are connected by a flatband cable for rapid pingpong. For full performance
 * with 16 ports call in 8 parallel tasks.
 *
 * vendor / implementation specific parameters to be set to max speed of course
 * this is an "uni_multi" test: one implementation, multiple tasks/ports
 * ex:
 * an2: ./CANX-tester -pingpong16 anagate2 128.141.159.242 128.141.159.218 "250000 0 1 1 0 0 7000" 0 0 3.2
 * with 3.2us delay between sends

 */
void SpecialCommands::pingpong(std::string vendor,
		std::string ip0, std::string ip1,
		std::string parameters,
		unsigned int portA, unsigned int portB,
		float delay_us,
		int kmax ){
	// std::string param = "Unspecified"; // don't touch existing settings
	// std::string param = "125000"; // low bitrate for everyone, no other settings
	// std::string params = parameters;
	std::string vlib;
	std::vector<std::string> namesv;
	unsigned int nbPorts = 2;
	if ( vendor == "systec"){
		std::cout << "not yet implemented" << std::endl;
		exit(0);

#ifdef _WIN32
		vlib = "st";
#else
		vlib = "sock";
#endif
		for ( unsigned int p = 0; p < nbPorts; p++){
			namesv.push_back( vlib + ":" + std::to_string(p));
		}
	} else if ( vendor == "peak"){
#ifdef _WIN32
		vlib = "pk";
		for ( unsigned int p = 0; p < nbPorts; p++){
			namesv.push_back( vlib + ":" + std::to_string(p));
		}
#else
		std::cout << "not yet implemented" << std::endl;
		exit(0);

		vlib = "sock";
		// need extended deviceID for peak, use decimal number and LOCAL PORT NUMBERS
		//name0 = "sock:0:device:"+ip0;
		//name1 = "sock:0:device:"+ip1;
		for ( unsigned int p = 0; p < nbPorts; p++)
			namesv.push_back( vlib + ":" + std::to_string(p) + ":device" + ip0);
#endif
	} else if ( vendor == "anagate"){
		std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " OK using " << ip0 << std::endl;
		vlib = "an";
		namesv.clear();
		namesv.push_back("an:" + std::to_string( portA ) + ":" + ip0);
		namesv.push_back("an:" + std::to_string( portB ) + ":" + ip1);
	} else if ( vendor == "anagate2"){
		std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " OK using " << ip0 << std::endl;
		vlib = "an2";
		namesv.clear();
		namesv.push_back("an:" + std::to_string( portA ) + ":" + ip0);
		namesv.push_back("an:" + std::to_string( portB ) + ":" + ip1);
	} else {
		std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " vendor " << vendor << " ERR unknown" << std::endl;
		exit(-1);
	}

	// before doing anything, we connect a handler to the global errors
	CanModule::GlobalErrorSignaler *gsig = CanModule::GlobalErrorSignaler::getInstance();
	gsig->connectHandler( &SpecialCommands::globalErrorHandler0 );
	gsig->connectHandler( &SpecialCommands::globalErrorHandler1 );

	CanModule::CanLibLoader* lib = CanModule::CanLibLoader::createInstance( vlib );

	// create the can accesses. the bus open might still fail, and in this case we would like to have an error signal.
	std::vector<CanModule::CCanAccess*> cca_vect;
	cca_vect.clear();
	bool fail = false;
	std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " parameters= " << parameters << std::endl;
	for ( unsigned int n = 0; n < namesv.size(); n++){
		std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " OK opening bus name= " << namesv[ n ] << std::endl;
		cca_vect.push_back( lib->openCanBus( namesv[ n ], parameters ) );

		if ( ! cca_vect[ n ] ){
			std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " ERR check: openCanBus name= " << namesv[ n ]
			  << " has failed" << std::endl;
			fail = true;
		}
	}
	// to avoid segfaults on NULL
	if ( fail ) {
		std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " ERR at least one openCanBus failed";
		exit(-1);
	}

	// connect reception handlers
	cca_vect[ 0 ]->canMessageCame.connect( &SpecialCommands::reception0 );
	cca_vect[ 1 ]->canMessageCame.connect( &SpecialCommands::reception1 );

	// connect error handlers
	cca_vect[ 0 ]->canMessageError.connect( &SpecialCommands::error0 );
	cca_vect[ 1 ]->canMessageError.connect( &SpecialCommands::error1 );

	// connect port status change handlers
	cca_vect[ 0 ]->canPortStateChanged.connect( &SpecialCommands::portStatusChanged0 );
	cca_vect[ 1 ]->canPortStateChanged.connect( &SpecialCommands::portStatusChanged1 );

	// set reconnection behavior
	for ( unsigned int iv = 0; iv < cca_vect.size(); iv++){
		cca_vect[ iv ]->setReconnectBehavior( CanModule::ReconnectAutoCondition::sendFail, CanModule::ReconnectAction::singleBus );
		cca_vect[ iv ]->setReconnectFailedSendCount( 3 );
	}

	// check
	{
		showConnections();
		for ( std::vector<CanModule::CCanAccess*>::iterator it = cca_vect.begin() ; it != cca_vect.end(); ++it ){
			showBusStats( *it );
		}
	}


	// pingpong blast
	//int kmax = 15000;
	std::vector<int> count_vect;
	count_vect.push_back(0);
	count_vect.push_back(0);

	std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " doing " << kmax << " pingpongs" << std::endl;
	std::chrono::high_resolution_clock::time_point tstart = std::chrono::high_resolution_clock::now();

	for ( int k = 0; k < kmax; k++ ){
		if ( (k % 1000) == 0 ){
			std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " countdown k= " << kmax-k << std::endl;
		}
		for ( unsigned int i = 0; i < cca_vect.size(); i++ ){
			CanMessage msg = createCanMsg();
			cca_vect[ i ]->sendMessage( &msg );
			us_sleep( delay_us );
			count_vect[ i ] += 1;
			std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " send message i= " << i << " count= " << count_vect[ i ] << std::endl;
		}
	}
	auto nDiff = std::chrono::high_resolution_clock::now() - tstart;
	auto period = std::chrono::duration_cast<std::chrono::milliseconds>(nDiff).count();
	std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << "pingpong took "
			<< period << " seconds for kmax= " << kmax << std::endl;
	std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " sleep 15 secs to finish reception/sending" << std::endl;
	ms_sleep( 2000 );

	// check
	{
		showConnections();
		for ( std::vector<CanModule::CCanAccess*>::iterator it = cca_vect.begin() ; it != cca_vect.end(); ++it ){
			showBusStats( *it );
		}
	}


	for ( unsigned int k = 0; k < cca_vect.size(); k++ ){
		lib->closeCanBus( cca_vect[ k ] );
	}

	std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " took "
			<< (double) period << " ms for kmax= " << kmax << std::endl;
	std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " sleep 15 secs to finish reception/sending" << std::endl;

	std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " OK finished" << std::endl;
	exit(0);
}

/**
 * read out versioning info for systec windows, both ports, not using CanModule at all
 */
void SpecialCommands::winversionsystec(){


#ifndef _WIN32
	std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " ONLY available for systec/windows. exiting.." << std::endl;
	exit(0);
#else
	BYTE systecCallReturn = USBCAN_SUCCESSFUL;
	tUcanHandle		canModuleHandle;
	int m_moduleNumber = 0;
	systecCallReturn = ::UcanInitHardware(&canModuleHandle, USBCAN_ANY_MODULE, NULL); // the first device
	std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " UcanInitHardware returns [ 0x" <<
			std::hex << (int) systecCallReturn << std::dec << "]" << std::endl;
	if (systecCallReturn != USBCAN_SUCCESSFUL ) 	{
		std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " UcanDeinitHardware, return code = [ 0x"
				<< std::hex << (int) systecCallReturn << std::dec << "]" << std::endl;
		::UcanDeinitHardware(canModuleHandle);
		return;
	}

	DWORD dwVersion = ::UcanGetVersion();
	std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " dwVersion=[ 0x"
			<< std::hex << (int) dwVersion << std::dec << "]" << std::endl;;

	/**
	VerTypeUserDll
	kVerTypeUserLib
	0x0001 Gibt die Version der USBCAN-Library zurück.
	kVerTypeSysDrv 0x0002 Gibt die Version der USBCAN.SYS zurück.
	kVerTypeNetDrv 0x0004 Gibt die Version der UCANNET.SYS zurück
	(Netzwerktreiber).
	kVerTypeSysLd 0x0005 Gibt die Version des Loaders USBCANLD.SYS
	für das GW-001 zurück.
	kVerTypeSysL2 0x0006 Gibt die Version des Loaders USBCANL2.SYS
	für das GW-002 zurück.
	kVerTypeSysL3 0x0007 Gibt die Version des Loaders USBCANL3.SYS
	für das Multiport CAN-to-USB zurück.
	kVerTypeSysL4 0x0008 Gibt die Version des Loaders USBCANL4.SYS
	für das USB-CANmodul1 zurück.
	kVerTypeSysL5 0x0009 Gibt die Version des Loaders USBCANL5.SYS
	für das USB-CANmodul2 zurück.
	kVerTypeCpl 0x000A Gibt die Version des USB-C
	*/
	DWORD type = 0x0001;
	DWORD dwVersionEx = ::UcanGetVersionEx( type );
	std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " type= "
			<< type << " dwVersionEx=[ 0x" << std::hex << (int) dwVersion << std::dec << "]" << std::endl;

	type = 0x0002;
	dwVersionEx = ::UcanGetVersionEx( type );
	std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " type= "
			<< type << " dwVersionEx=[ 0x" << std::hex << (int) dwVersionEx << std::dec << "]" << std::endl;

	type = 0x0004;
	dwVersionEx = ::UcanGetVersionEx( type );
	std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " type= "
			<< type << " dwVersionEx=[ 0x" << std::hex << (int) dwVersionEx << std::dec << "]" << std::endl;

	type = 0x0005;
	dwVersionEx = ::UcanGetVersionEx( type );
	std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " type= " << type
			<< " dwVersionEx=[ 0x" << std::hex << (int) dwVersionEx << std::dec << "]"<< std::endl;

	type = 0x0006;
	dwVersionEx = ::UcanGetVersionEx( type );
	std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " type= "
			<< type << " dwVersionEx=[ 0x" << std::hex << (int) dwVersionEx << std::dec << "]"<< std::endl;

	type = 0x0007;
	dwVersionEx = ::UcanGetVersionEx( type );
	std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " type= "
			<< type << " dwVersionEx=[ 0x" << std::hex << (int) dwVersionEx << std::dec << "]"<< std::endl;

	type = 0x0008;
	dwVersionEx = ::UcanGetVersionEx( type );
	std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " type= "
			<< type << " dwVersionEx=[ 0x" << std::hex << (int) dwVersionEx << std::dec << "]"<< std::endl;

	type = 0x0009;
	dwVersionEx = ::UcanGetVersionEx( type );
	std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " type= "
			<< type << " dwVersionEx=[ 0x" << std::hex << (int) dwVersionEx << std::dec << "]"<< std::endl;


	DWORD dwFwVersion = ::UcanGetFwVersion(canModuleHandle);
	std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__
			<< " UcanGetFwVersion=[ 0x" << std::hex << (int) dwFwVersion << std::dec << "]"<< std::endl;


	tUcanHardwareInfo HwInfo;
	DWORD dwHwInfo = ::UcanGetHardwareInfo(canModuleHandle, &HwInfo);
	std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " HwInfo.m_dwSerialNr= [0x"
			<< std::hex << HwInfo.m_dwSerialNr << "]" << std::dec << std::endl;

#if 0
	// may be fully implemented if really needed. manual p87
	tUcanHardwareInfoEx pHwInfoEx_p;
	tUcanChannelInfo pCanInfoCh0_p;
	tUcanChannelInfo pCanInfoCh1_p;
	DWORD dwHwInfoEx2= ::UcanGetHardwareInfoEx2(canModuleHandle,
			&pHwInfoEx_p, &pCanInfoCh0_p, &pCanInfoCh1_p);
#endif
#endif
	std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " OK finished" << std::endl;
	exit(0);
}


} /* namespace SpecialCommands_ns */

