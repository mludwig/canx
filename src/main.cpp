/* © Copyright CERN, Geneva, Switzerland, 2016.  All rights not expressly granted are reserved.
 *
 *  Created on: Wed Aug 30 10:15:57 CEST 2017
 *
 * 		Author: Michael Ludwig <michael.ludwig@cern.ch>
 *      Contributors and prior art: Benjamin Farnham, Piotr Nikiel, Viacheslav Filimonov
 *
 * This file is part of the CAN Common Tools project and is the property of CERN, Geneva, Switzerland,
 * and is not free software, since it builds on top of vendor
 * specific communication interfaces and architectures, which are generally non-free and
 * are subject to licensing and/or registration. Please refer to the relevant
 * collaboration agreements between CERN ICS and the vendors for further details.
 *
 * The non-vendor specific parts of the software can be made available on request
 * under the GNU Lesser General Public Licence,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public Licence for more details <http://www.gnu.org/licenses/>.
 */

// atlas systec driver modified (&kernel) from:
// atlas CanModule devel version branch: https://github.com/quasar-team/CanModule.git branch CanModule-rev

/** \mainpage CANX-tester
 * \section intro_sec Overview
 * CANX-tester is a cross architectue/vendor/platform tool for CAN testing, for CAN bridges from various vendors.
 * CANX-tester is build and available (bins, libs) for cc7 and all windows platforms.
 * We can send frames in a random, fixed or determined way on many ports in parallel and also receive, record, replay
 * and mirror frames. The primary purpose is to fully test CAN gateways of different vendors systec, anagate, peak, concerning
 * platform, hardware, firmware, driver, api, and their integration into the CERN SW environment.
 *
 * Since the tool can record specific frames on specific ports in a timed way and replay them as well, it can also
 * be used to produce a detailed (=meaningful for CAN bus devices) and synchronized CAN traffic on many ports
 * in parallel, for complex test scenarios.
 *
 *
 * main features:
 *    - Each can port is served by two threads (objects) for sending and receiving
 *    - Create threads=objects for the ports used only, mapping
 *    - send CAN messages in a time controlled way
 *    - receive messages
 *    - mirror messages (=send them) on the same port or a different port
 *    - the traffic patterns are specified in a sequence sheet or on the command line
 *    - detailed diagnostic output: dropped frames sender/receiver, problems, measured frame-rates
 *    - detailed traffic logging to detect electronics problems
 *
 * \section specification_sec Specification and main Documentation
 *  - https://readthedocs.web.cern.ch/display/ICKB/CANCompUtils%253A+UR+specifications
 *  - https://readthedocs.web.cern.ch/display/CANDev/CANX-tester
 *
 */



#include <iostream>

using namespace std;
#ifndef _WIN32
#include <unistd.h>
#endif

// #include <CanBusAccess.h>
#include <CanLibLoader.h>
#include <LogIt.h>

#include "../include/main.h"
#include "../include/Configuration.h"
#include "../include/connection.h"
#include "../include/SpecialCommands.h"


#include <generated/VERSION.h> // CANX_VERSION

using namespace connection_ns;
using namespace configuration_ns;

Log::LogComponentHandle canxHandle = 0;
Log::LogComponentHandle canModuleHandle = 0;


void usage( void ){
	cout << "CANX-tester: send and receive CAN messages on ports," << endl;
	cout << "generate specific multi-threaded scenarios and test sequences." << endl;
	cout << "cross platform, multiple vendors." << endl;
	cout << "USAGE:" << endl;
	cout << " loglevel: [-T,-D,-I,-W,-E] = T(TRC), D(DBG), I(INF), W(WRN), E(ERR)" << endl;
	cout << " configuration: [-cfg <config.xml>]" << endl;
	cout << " special commands: [-anagateSoftwareReset <ip>]" << endl;
	cout << " -uni_mono_reserved0 [anagate <ip0> <ip1> | systec 0 0 | peak <ext.id0> <ext.id1> ]" << endl;
	cout << " -uni_mono_reserved1 [anagate <ip0> <ip1> | systec 0 0 | peak <ext.id0> <ext.id1> ]" << endl;
	cout << " -mixed_mono_reserved0 <ip0>" << endl;
	cout << " -uni_multi_shared0 [anagate <ip0> <port0> <ip1> <port1> | systec <port0> <port1> | peak <ext.id0> <port0> <ext.id1> <port1> ]" << endl;
	cout << " -uni_multi_shared1 [anagate <ip0> <port0> <ip1> <port1> | systec <port0> <port1> | peak <ext.id0> <port0> <ext.id1> <port1> ]" << endl;
	cout << " -uni_mono_errors [anagate <ip0> | systec | peak <ext.id0> ]" << endl;
	cout << " ===VENDOR SPECIFIC ANAGATE===" << endl;
	cout << " -anagate0 <ip0> <ip1> : anagate specific test 2 bridges each CAN0" << endl;
	cout << " -anagate_diag0  <p> <ip0> : anagate super simple test for new diags on a port  p, in a loop" << endl;
	cout << " -anagate_diag1 <nbPorts> <delay_ms> <ip0> <ip1> : anagate specific test for diagnostics 2 bridges N ports" << endl;
	cout << endl;
	cout << " -api_canaccess [anagate <ip0> <ip1> | systec 0 0 | peak <ext.id0> <ext.id1> ]" << endl;
	cout << " -uni_mono_signals [ anagate | systec | peak ] ip0 : error handler tests on one bridge." << endl;
	cout << " -pingpong [ anagate | anagate2 | systec | peak ] ip0 ip1 parameters portA portB delay_us kmax: torture pingpong on 2 ports with optional delay between sending." << endl;
	cout << " -winversionsystec: reads out versioning info for systec under windows (other OS/vendors: do nothing)" << endl;

	exit(0);
}

CanxThreads::ThreadData_t configureConnection( configuration_ns::Configuration::CFG_THREAD_t ct ){
	CONNECTION::ThreadData_t td;
	td.sender_enabled = ct.sender.enabled;

	// this works for systec, but not for the extended device ID for peak
	// we need a string here in that case, or another number for the device ID for peak
	td.connection.canPport = ct.canPort;
	td.peak.deviceId =  ct.peakDeviceId;

	td.connection.canLport = -1;                        // logical port used by driver
	td.msgGenerationType = ct.sender.messageGeneration;
	td.nbMsgToGeneratePerTick = ct.sender.generatedNbFramesPerTick;
	td.canIdType = ct.sender.messageStandard;
	td.bitrate = ct.bitrate;
	td.sbitrate = CanxThreads::codeBR_2_string( ct.bitrate );
	td.framerate_limit_factor = ct.sender.framerateLimitFactor;

	if ( ct.moduleType == string("systec") ) {
		td.vendor = CanxThreads::Vendor_t::VENDOR_SYSTEC;
		td.connection.usbport = ct.usbPort;

		cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " systec usbport= " << td.connection.usbport << endl;
	} else if ( ct.moduleType == string("peak") ) {
		td.vendor = CanxThreads::Vendor_t::VENDOR_PEAK;
		td.connection.usbport = ct.usbPort;

		cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " peak usbport= " << td.connection.usbport << endl;
	} else if ( ct.moduleType == string("anagate") ) {
		td.vendor = CanxThreads::Vendor_t::VENDOR_ANAGATE;
		td.anagate.operationalMode = ct.operationMode;
		td.anagate.termination = ct.termination;
		td.anagate.highSpeed = ct.highSpeed;
		td.anagate.timeStamp = ct.timeStamp;
		td.anagate.syncMode = ct.syncMode;
		td.connection.ipNumber = ct.ipNumber;

		td.anagate.timeout = ct.anagateTimeout;
	} else if ( ct.moduleType == string("anagate2") ) {
		td.vendor = CanxThreads::Vendor_t::VENDOR_ANAGATE2;
		td.anagate.operationalMode = ct.operationMode;
		td.anagate.termination = ct.termination;
		td.anagate.highSpeed = ct.highSpeed;
		td.anagate.timeStamp = ct.timeStamp;
		td.anagate.syncMode = ct.syncMode;
		td.connection.ipNumber = ct.ipNumber;

		td.anagate.timeout = ct.anagateTimeout;
	} else {
		td.vendor = CanxThreads::Vendor_t::VENDOR_UNKNOWN;
	}

	// receiver
	td.receiver_delay_us = ct.receiver.waitBetweenFrames;
	td.receiverIsMirror = ct.receiver.mirror;
	td.receiverIsRecorder = ct.receiver.recorder;
	td.objIndex = ct.threadIndex;
	td.sender_delay_us = ct.sender.waitBetweenFrames;
	td.receiver_enabled = ct.receiver.enabled;
	td.connection_mirror.canPport = ct.receiver.mirrorCanPort;
	td.connection_mirror.canPport = ct.receiver.mirrorCanPort;
	td.connection_mirror.mirrorIncrementMsgId = ct.receiver.mirrorIncrementMsgId;

	std::ostringstream os;
	os << "connection." << ct.moduleType << "." <<  ct.threadIndex;
	td.threadID = os.str();

	// set up the mirror port as the same port, even if unused for non USB
	td.connection_mirror.usbport = td.connection.usbport;
	td.connection_mirror.canPport = td.connection.canPport;
	td.connection_mirror.canLport = td.connection.canLport;
	LOG(Log::TRC, canxHandle ) << __FUNCTION__ << " configured " << td.threadID;
	return( td );
}

CONNECTION::CanBitrates_t convertToBitrate( int rawBitrate ){
	CONNECTION::CanBitrates_t bitrate = CONNECTION::CANBR_125kbs;
	switch( rawBitrate){
	case 10: bitrate = CONNECTION::CANBR_10kbs; break;
	case 20: bitrate = CONNECTION::CANBR_20kbs; break;
	case 50: bitrate = CONNECTION::CANBR_50kbs; break;
	case 125: bitrate = CONNECTION::CANBR_125kbs; break;
	case 250: bitrate = CONNECTION::CANBR_250kbs; break;
	case 500: bitrate = CONNECTION::CANBR_500kbs; break;
	case 800: bitrate = CONNECTION::CANBR_800kbs; break;
	case 1000: bitrate = CONNECTION::CANBR_1000kbs; break;
	default: usage();
	}
	return( bitrate );
}

void clockThread(boost::barrier& cur_barrier, int counter, int tick_ms, int x_ms, int x_us )
{
	LOG(Log::TRC, canxHandle ) << "clockThread: counter= " << counter;
	int cc = counter;
	//int tick = tick_ms * 1000;
	LOG(Log::TRC, canxHandle ) << "clock sleeping 100 ms, before firing all threads";
	CanModule::ms_sleep( 100 );
	while( cc-- > 0 ){

		// since we have slept while the sender threads were working, we should be the last thread
		// arriving at the barrier. We should therefore never have to wait here. As soon as we have passed
		// the barrier all other sender threads make one tick, since everyone else was waiting already.
		LOG(Log::INF, canxHandle ) << "next tick, countdown= " << cc;
		cur_barrier.wait();

		LOG(Log::TRC, canxHandle ) << "clock sleeping " << tick_ms << " ms, tick countdown= " << cc;
		CanModule::ms_sleep( tick_ms );
	}
}

/**
 * static thread for sending, reception handler gets connected as well
 */
/* static */ void senderThread( boost::barrier& cur_barrier, CONNECTION *sender, int counter, int sequenceRepeat, CanxThreads::ThreadData_t td ){
	LOG(Log::TRC, canxHandle ) << __FUNCTION__;

	int cc = counter;
	int seq = sequenceRepeat + 1;
	sender->initialize( td );

	LOG(Log::TRC, canxHandle ) << __FUNCTION__ << " starting " << sender->getCCA()->getBusName();

	LOG(Log::TRC, canxHandle ) << __FUNCTION__ << " start " << sender->id() << " start tick, counter= " << counter << " repeat= " << sequenceRepeat;
	sender->executeStartTick();

	while ( seq > 0 ){

		unsigned int ctick = 0;
		while( cc-- > 0 ){
			LOG(Log::TRC, canxHandle) << __FUNCTION__<< " " << sender->id() << " waiting for next tick ";
			cur_barrier.wait();
			LOG(Log::TRC, canxHandle) << __FUNCTION__<< " " << sender->id() << " barrier released, executing tick ";
			sender->executeClockTick( ctick );
			ctick++;
		}

		if ( seq > 1 ){
			sender->resetSequence();
			cc = counter;
		}
		seq--;
		LOG(Log::TRC, canxHandle) << __FUNCTION__ << " " << sender->id() << " repeat sequence another " << seq << " times";
	}
	// xxx don't call this
	// LOG(Log::TRC, canxHandle) << __FUNCTION__ << " " << sender->id() << " OPCUA-1536 deinitialize";
	// sender->deinitialize( td );
	LOG(Log::TRC, canxHandle) << __FUNCTION__ << " " << sender->id() << " finished sequence, keep thread alive a bit longer for stat.";
	int countdown = 10;
	while( countdown-- ){
		ms_sleep( 200 );
	}
	LOG(Log::TRC, canxHandle) << __FUNCTION__ << " " << sender->id() << " finishing thread.";
}


int anagateSoftwareReset( string ip ){
	SpecialCommands_ns::SpecialCommands cmd = SpecialCommands_ns::SpecialCommands();
	AnaInt32 ret = cmd.anagateSoftwareReset( ip );
	if ( ret != 0 ) {
		cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " ERROR " << endl;
		return(-1);
	}
	return(0);
}


int main( int argc, char* argv[] )
{
	char special_ip[127];
	int specialFunction = 0;
	Log::LOG_LEVEL loglevel = Log::TRC;
	bool ret = Log::initializeLogging( loglevel );
	if ( ret ) cout << "CANX: LogIt initialized OK" << endl;
	else std::cout << "CANX: LogIt problem at initialisation" << std::endl;

	LogItInstance *logIt = LogItInstance::getInstance();
	CanLibLoader::initializeLogging( logIt );
	Log::setNonComponentLogLevel( loglevel );
	std::cout << "Log level set to " << loglevel << std::endl;


	/**
	 * LogIt component CANX for main, same task. They all init with TRC
	 */
	// Log::initializeDllLogging( logIt );
	Log::registerLoggingComponent( "CANX", loglevel );
	logIt->getComponentHandle( "CANX", canxHandle );
	Log::setComponentLogLevel( canxHandle, loglevel );
	LOG(Log::INF, canxHandle) << argv[ 0 ] << CANX_VERSION << " " << __DATE__ << " " << __TIME__ << " logging for component CANX";

	/**
	 * LogIt component CanModule for generic CanModule, threads
	 */
	string CanModule_LogItComponentName = "CanModule";
	Log::registerLoggingComponent( CanModule_LogItComponentName, loglevel );
	logIt->getComponentHandle( CanModule_LogItComponentName, canModuleHandle );
	Log::setComponentLogLevel( canModuleHandle, loglevel );
	LOG(Log::INF, canModuleHandle) << " CanModule version " << CanModule_VERSION << " logging for component " << CanModule_LogItComponentName;


	/**
	 * lets get clear about the Logit components
	 */
	std::map<Log::LogComponentHandle, std::string> log_comp_map = Log::getComponentLogsList();
	std::map<Log::LogComponentHandle, std::string>::iterator it;
	for ( it = log_comp_map.begin(); it != log_comp_map.end(); it++ )
	{
		Log::LOG_LEVEL level;
		Log::getComponentLogLevel( it->first, level);
		std::cout << __FILE__ << " " << __LINE__ << " *** " << " LogIt component " << it->second << " level= " << level << std::endl;
	}


	string shortHandSeq;
	Configuration *cfg = new Configuration(); // defaults are assumed
	cfg->setLogLevel( loglevel );
	std::string ip0;
	std::string ip1;

#if 0
	// hack for windows debugger
	std::cout << __FILE__ << " " << __LINE__ << " hack for windows debugger " << std::endl;

	loglevel = Log::TRC;
	SpecialCommands_ns::SpecialCommands cmd = SpecialCommands_ns::SpecialCommands();
	cmd.uni_mono_signals(  "anagate", "ANACANFZ16-01F4-131E" );
	std::cout << __FILE__ << " " << __LINE__ << " hack for windows debugger " << std::endl;
	return(0);
#endif

	for ( int i = 1; i < argc; i++ ){
		if ( std::string::npos != std::string(argv[i]).find("help") ) usage();
		if ( std::string::npos != std::string(argv[i]).find("-h") ) usage();
		if ( 0 == strcmp( argv[i], "-T")) {
			loglevel = Log::TRC;
			Log::setNonComponentLogLevel( loglevel );
			Log::setComponentLogLevel( canxHandle, loglevel );
			Log::setComponentLogLevel( canModuleHandle, loglevel );
		}
		if ( 0 == strcmp( argv[i], "-D")) {
			loglevel = Log::DBG;
			Log::setNonComponentLogLevel( loglevel );
			Log::setComponentLogLevel( canxHandle, loglevel );
			Log::setComponentLogLevel( canModuleHandle, loglevel );
		}
		if ( 0 == strcmp( argv[i], "-I")) {
			loglevel = Log::INF;
			Log::setNonComponentLogLevel( loglevel );
			Log::setComponentLogLevel( canxHandle, loglevel );
			Log::setComponentLogLevel( canModuleHandle, loglevel );
		}
		if ( 0 == strcmp( argv[i], "-W")) {
			loglevel = Log::WRN;
			Log::setNonComponentLogLevel( loglevel );
			Log::setComponentLogLevel( canxHandle, loglevel );
			Log::setComponentLogLevel( canModuleHandle, loglevel );
		}
		if ( 0 == strcmp( argv[i], "-E")) {
			loglevel = Log::ERR;
			Log::setNonComponentLogLevel( loglevel );
			Log::setComponentLogLevel( canxHandle, loglevel );
			Log::setComponentLogLevel( canModuleHandle, loglevel );
		}

		if ( 0 == strcmp( argv[i], "-cfg")) {
			if ( argc >= i + 1 ) {
				char ff[127];
				sscanf( argv[ i + 1 ], "%s", (char *) &ff );
				string configfile = ff;
				Log::setNonComponentLogLevel( loglevel );
				Log::setComponentLogLevel( canxHandle, loglevel );
				Log::setComponentLogLevel( canModuleHandle, loglevel );
				cfg->setLogLevel( loglevel );
				cfg->read( configfile ); // overwrite defaults with specific config
			} else usage();
		}

		if ( 0 == strcmp( argv[i], "-anagateSoftwareReset")) {
			if ( argc >= i + 1 ) {
				sscanf( argv[ i + 1 ], "%s", (char *) &special_ip );
				std::cout << "-anagateSoftwareReset : got special_ip= " << special_ip << endl;
				specialFunction = 100;
			} else {
				std::cout << "-anagateSoftwareReset : missing argument <ip>" << endl;
				usage();
			}
		}
		if ( 0 == strcmp( argv[i], "-uni_mono_reserved0")) {
			if ( argc >= i + 3 ) {
				char vendor[256];
				char ip0[256];
				char ip1[256];
				sscanf( argv[ i + 1 ], "%s", vendor );
				sscanf( argv[ i + 2 ], "%s", ip0 );
				sscanf( argv[ i + 3 ], "%s", ip1 );
				std::cout << __FILE__ << " " << __LINE__ << " " << vendor << " " << ip0 << " " << ip1 << std::endl;
				SpecialCommands_ns::SpecialCommands cmd = SpecialCommands_ns::SpecialCommands();
				cmd.uni_mono_reserved0( vendor, ip0, ip1 );
			} else {
				usage();
			}
		}
		if ( 0 == strcmp( argv[i], "-uni_mono_reserved1")) {
			if ( argc >= i + 3 ) {
				char vendor[256];
				char ip0[256];
				char ip1[256];
				sscanf( argv[ i + 1 ], "%s", vendor );
				sscanf( argv[ i + 2 ], "%s", ip0 );
				sscanf( argv[ i + 3 ], "%s", ip1 );
				std::cout << __FILE__ << " " << __LINE__ << " " << vendor << " " << ip0 << " " << ip1 << std::endl;
				SpecialCommands_ns::SpecialCommands().uni_mono_reserved1( vendor, ip0, ip1 );
			} else {
				usage();
			}
		}
		if ( 0 == strcmp( argv[i], "-mixed_mono_reserved0")) {
			if ( argc >= i + 1 ) {
				char ip0[256];
				sscanf( argv[ i + 1 ], "%s", (char *) ip0 );
				SpecialCommands_ns::SpecialCommands().mixed_mono_reserved0( ip0 );
			} else {
				usage();
			}
		}
		if ( 0 == strcmp( argv[i], "-uni_multi_shared0")) {
			if ( argc >= i + 6 ) {
				char vendor[256];
				char ip0[256];
				char ip1[256];
				int porta = 0;
				int portb = 0;
				int scenario = 0;
				sscanf( argv[ i + 1 ], "%s", vendor );
				sscanf( argv[ i + 2 ], "%s", ip0 );
				sscanf( argv[ i + 3 ], "%d", &porta );
				sscanf( argv[ i + 4 ], "%s", ip1 );
				sscanf( argv[ i + 5 ], "%d", &portb );
				sscanf( argv[ i + 6 ], "%d", &scenario );
				std::cout << "-uni_multi_shared0 vendor= " << vendor
						<< " ip0= " << ip0
						<< " porta= " << porta
						<< " ip1= " << ip1
						<< " portb= " << portb
						<< " scenario= " << scenario  << std::endl;
				SpecialCommands_ns::SpecialCommands().uni_multi_shared0( vendor, ip0, porta, ip1, portb, scenario );
			} else {
				std::cout << "ERROR parameters -uni_multi_shared0" << std::endl;
				usage();
			}
		}

		if ( 0 == strcmp( argv[i], "-uni_multi_shared1")) {
			std::cout << __FILE__ << " " << __LINE__ << " argc=  " << argc << " i= " << i << std::endl;
			for ( int k = 0; k < 7; k++){
				std::cout << __FILE__ << " " << __LINE__ << " " << k << " " << argv[ k ] << std::endl;
			}
			if ( argc >= i + 4 ) {
				char vendor[256];
				char ip0[256];
				char ip1[256];
				int thread = 0;
				sscanf( argv[ i + 1 ], "%s", (char *) &vendor );
				sscanf( argv[ i + 2 ], "%d", &thread );
				sscanf( argv[ i + 3 ], "%s", (char *) &ip0 );
				sscanf( argv[ i + 4 ], "%s", (char *) &ip1 );
				std::cout << __FILE__ << " " << __LINE__ << " vendor=  " << vendor
						<< " thread= " << thread
						<< " ip0= " << ip0
						<< " ip1= " << ip1
						<< std::endl;

				SpecialCommands_ns::SpecialCommands().uni_multi_shared1( vendor, thread, ip0, ip1 );
			} else {
				usage();
			}
		}

		if ( 0 == strcmp( argv[i], "-mixed_multi_shared0")) {
			if ( argc >= i + 3 ) {
				std::string ip0 = "";
				std::string ip1 = "";
				int thread = 0;
				sscanf( argv[ i + 1 ], "%d", &thread );
				sscanf( argv[ i + 2 ], "%s", (char *) &ip0 );
				sscanf( argv[ i + 3 ], "%s", (char *) &ip1 );
				SpecialCommands_ns::SpecialCommands().mixed_multi_shared0( thread, ip0, ip1 );
			} else {
				usage();
			}
		}
		if ( 0 == strcmp( argv[i], "-uni_mono_signals")) {
			std::cout << __FILE__ << " " << __LINE__ << " i= " << i << " argc= " << argc << std::endl;
			if ( argc >= i + 2 ) {
				char vendor[256];
				char ip0[256];
				sscanf( argv[ i + 1 ], "%s", vendor );
				sscanf( argv[ i + 2 ], "%s", ip0 );
				std::cout << __FILE__ << " " << __LINE__ << " " << vendor << " " << ip0 << std::endl;
				SpecialCommands_ns::SpecialCommands cmd = SpecialCommands_ns::SpecialCommands();
				cmd.uni_mono_signals( vendor, ip0 );
			} else {
				usage();
			}
		}

		if ( 0 == strcmp( argv[i], "-api_canaccess")) {
			if ( argc >= i + 3 ) {
				char vendor[256];
				char ip0[256];
				char ip1[256];
				sscanf( argv[ i + 1 ], "%s", vendor );
				sscanf( argv[ i + 2 ], "%s", ip0 );
				sscanf( argv[ i + 3 ], "%s", ip1 );
				std::cout << __FILE__ << " " << __LINE__ << " " << vendor << " " << ip0 << " " << ip1 << std::endl;
				SpecialCommands_ns::SpecialCommands cmd = SpecialCommands_ns::SpecialCommands();
				cmd.api_canaccess( vendor, ip0, ip1 );
			} else {
				usage();
			}
		}
		if ( 0 == strcmp( argv[i], "-anagate0")) {
			if ( argc >= i + 2 ) {
				char ip0[256];
				char ip1[256];
				sscanf( argv[ i + 1 ], "%s", ip0 );
				sscanf( argv[ i + 2 ], "%s", ip1 );
				std::cout << __FILE__ << " " << __LINE__ << " " << ip0 << " " << ip1 << std::endl;
				SpecialCommands_ns::SpecialCommands cmd = SpecialCommands_ns::SpecialCommands();
				cmd.anagate0( ip0, ip1 );
			} else {
				usage();
			}
		}
		if ( 0 == strcmp( argv[i], "-pingpong")) {
			if ( argc >= i + 6 ) {
				char vendor[256];
				char ip0[256];
				char ip1[256];
				unsigned int portA;
				unsigned int portB;
				float delay;
				int kmax;
				sscanf( argv[ i + 1 ], "%s", vendor );
				sscanf( argv[ i + 2 ], "%s", ip0 );
				sscanf( argv[ i + 3 ], "%s", ip1 );
				std::string parameters = argv[ i + 4 ];
				sscanf( argv[ i + 5 ], "%d", &portA );
				sscanf( argv[ i + 6 ], "%d", &portB );
				sscanf( argv[ i + 7 ], "%f", &delay );
				sscanf( argv[ i + 8 ], "%d", &kmax );
				std::cout << __FILE__ << " " << __LINE__ << " " << vendor << " "
						<< ip0 << " " << ip1 << " " << " parameters= " << parameters
						<< " " << portA << " " << portB << " delay= " << delay << std::endl;
				SpecialCommands_ns::SpecialCommands cmd = SpecialCommands_ns::SpecialCommands();
				cmd.pingpong( vendor, ip0, ip1, parameters, portA, portB, delay, kmax );
			} else {
				usage();
			}
		}

		// ports 0, 8
		if ( 0 == strcmp( argv[i], "-winversionsystec")) {
			if ( argc >= i ) {
				SpecialCommands_ns::SpecialCommands cmd = SpecialCommands_ns::SpecialCommands();
				cmd.winversionsystec();
			} else {
				usage();
			}
		}

	}
	Log::setNonComponentLogLevel( loglevel );
	Log::setComponentLogLevel( canxHandle, loglevel );
	Log::setComponentLogLevel( canModuleHandle, loglevel );


	/**
	 * special functions
	 */
	switch( specialFunction ){
	case 100: {
		// anagate one shot SW reset on the ip number without using CanModule
		anagateSoftwareReset( special_ip );
		std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " @linux: gives a mem corruption if mod is present" << std::endl;
		return(0);
	}
	default: break;
	}

	/**
	 * one thread per sender per CAN port, with a receiver thread attached
	 */
	typedef std::map<int, CONNECTION *> senderMap_t;
	senderMap_t sender_map;
	vector<CanxThreads::ThreadData_t> sender_initv;
	unsigned int nbGlobalTicks = cfg->global().nbTicks;

	/**
	 * set up a barrier for sender thread synchronization. Each sender has it's own thread, and in order to use the
	 * barrier between threads as a central clocking mechanism we need one extra thread, the clock.
	 */
	// https://stackoverflow.com/questions/11407577/how-to-use-boost-barrier#11408125
	boost::barrier bar( cfg->nbThreads() + 1 );
	vector<boost::thread *> threadsv;


	/**
	 *  prepare the configs and create the objects
	 *  we always instantiate everything, and the disable/enable is just on the level of
	 *  sending and receiving. All objects follow the clock.
	 */

	for ( unsigned int threadNb = 0; threadNb < cfg->nbThreads(); threadNb++ ){
		cout << endl << "---- prepare thread" << threadNb << "-----" << endl;
		LOG( Log::DBG , canxHandle) << "creating config sender" << threadNb
				<< " enabled= " << cfg->thread( threadNb ).sender.enabled;
		cout << endl << "configuring connection thread" << threadNb << "-----" << endl;
		CanxThreads::ThreadData_t td_send = configureConnection( cfg->thread( threadNb ) );

		sender_initv.push_back( td_send );

		/**
		 * each connection instance loads the vendor lib, if not yet done. If you have several bridges
		 * of the same vendor the shared libs are NOT loaded for each bridge again, the OS only needs
		 * one instance. All instances of CONNECTION then would point to the same shared lib instance.
		 *
		 * One CONNECTION is one thread, and each thread talks to one CAN port: we have many of those therefore.
		 */
		pair<int, CONNECTION *> sp = make_pair(threadNb, new CONNECTION());
		sender_map.insert( sp );
		sp.second->storeObjPtr();
		sp.second->setLogLevel( loglevel );
		sp.second->setGlobalTickTime( cfg->global().tickTime );

		LOG( Log::DBG, canxHandle ) << "starting thread sender" << threadNb;
		boost::thread *t = new  boost::thread(boost::bind( &senderThread, boost::ref(bar),
				sp.second,
				cfg->global().nbTicks,
				cfg->global().repeat,
				td_send));
		threadsv.push_back( t );

		CanModule::ms_sleep( 100 );

		cout << "----prepare thread" << threadNb << " done-----" << endl;
	}

	/** let all senders reach the first barrier, 1000msec
	 * we could improve this to make sure all can buses are created properly, but that is good enough
	 */

	// this is slow enough to trust windows
	CanModule::ms_sleep( 1000 );
	cout << endl << "----wait for common synchro-----" << endl;

	try {
		/**
		 * load sender configs: we want each sender thread to create its own CanModule Access point
		 * For this we need to push down the configurations to each thread.
		 */
		cout << endl << "----loading configs into senders-----" << endl;
		int nrepeat = cfg->global().repeat;
		LOG( Log::DBG, canxHandle ) << "repeat sequence " << nrepeat << " times in total";
		for ( unsigned int threadNb = 0; threadNb < sender_map.size(); threadNb++){

			CONNECTION *conn = CONNECTION::getObjPtr( threadNb );
			conn->enableSender( cfg->thread( threadNb ).sender.enabled );
			conn->enableReceiver( cfg->thread( threadNb ).receiver.enabled );
			if ( ! cfg->thread( threadNb ).sender.enabled ){
				LOG(Log::WRN, canxHandle) << " disabled sender" << threadNb;
			}
			if ( ! cfg->thread( threadNb ).receiver.enabled ){
				LOG(Log::WRN, canxHandle) << " disabled receiver" << threadNb;
			}
			LOG( Log::TRC) << "---- prepare thread " << threadNb << " -----";
			conn->showConfig();

			// add all configured frames for that tick
			LOG( Log::TRC) << "adding ticks " << threadNb << " -----";
			vector<Configuration::CFG_TICK_t> ctickv = cfg->thread( threadNb ).sender.tick_v;
			for ( unsigned int itick = 0; itick < ctickv.size(); itick++ ){
				CONNECTION::CanThreadFrame_t frame;
				frame.tick = ctickv[ itick ].index;
				for ( unsigned int k = 0; k < ctickv[ itick ].frame_v.size(); k++ ){
					frame.canFrame = ctickv[ itick ].frame_v[ k ];
					/*
					 * The sender waits same time between all frames. Lets keep it simple
					 * for now, this can be of course refined further.
					 */
					frame.wait_us = cfg->thread( threadNb ).sender.waitBetweenFrames;
					conn->addFrame( frame );
				}
			}

			LOG( Log::TRC) << __FILE__ << " " << __LINE__ << " adding ticks thread= " << threadNb << " -----";
			conn->setRepeat( nrepeat );
			conn->setGeneratedTicksSender( cfg->global().nbTicks );
			if ( nbGlobalTicks < conn->getTotalTicksSender() ){
				nbGlobalTicks = conn->getTotalTicksSender();
			}

			// set reconnection behavior of the bus: never for these type of runs. we have special tests for that
			LOG( Log::TRC) << __FILE__ << " " << __LINE__ << " setting reconnection behavior thread= " << threadNb << " -----";
			conn->getCCA()->setReconnectBehavior( CanModule::ReconnectAutoCondition::never, CanModule::ReconnectAction::singleBus );
			conn->getCCA()->setReconnectFailedSendCount( 10000 ); // very high value to be sure for fun

			LOG( Log::TRC) << __FILE__ << " " << __LINE__ << " thread looks good " << threadNb << " -----";

		}
	}
	catch ( std::runtime_error &e ){
		LOG( Log::ERR, canxHandle ) << "launching thread runtime error: " << e.what();
		exit(0);
	}
	catch(std::exception& e)	{
		LOG( Log::ERR, canxHandle  ) << "launching thread standard exception: " << e.what();
		exit(0);
	}
	catch ( ... ){
		LOG( Log::ERR, canxHandle  ) << "launching thread other exception, unknown";
		exit(0);
	}

	LOG( Log::TRC) << __FILE__ << " " << __LINE__ << "----threads are configured and launched, starting clock ticks in 1000ms----";
	//cout << endl << "----threads are configured and launched, starting clock ticks in 1000ms----" << endl;
	CanModule::ms_sleep( 1000 );

	// start the clock thread
	cout << endl << "----starting clock ticks----" << endl;
	int nbTotalTicks = ( cfg->global().repeat + 1 )* cfg->global().nbTicks;
	threadsv.push_back( new  boost::thread(boost::bind( &clockThread, boost::ref(bar),
			nbTotalTicks,
			cfg->global().tickTime,
			0, 0)));
	// ... clock ticks are executed in all threads synchonized ....wait for finish

	CanModule::ms_sleep( 200 );
	for ( unsigned int it = 0; it < threadsv.size(); it++ ){
		threadsv[ it ]->join();
	}


	int w_ms = 1000 * sender_map.size();
	LOG( Log::INF, canxHandle ) << "letting receivers consume their buffers (if any) 1000ms per thread = " << w_ms << " ----";
	CanModule::ms_sleep( w_ms );
	LOG( Log::INF, canxHandle ) << "OK finished I think, saving any recorded traces and deleting senders, good luck ;-)";

	// dump stats to a file
	std::vector<CanModule::CCanAccess*> cca_vect;
	for ( unsigned int threadNb = 0; threadNb < sender_map.size(); threadNb++){
		CONNECTION *sender = CONNECTION::getObjPtr( threadNb );
		cca_vect.push_back( sender->getCCA() );
	}
	SpecialCommands_ns::SpecialCommands::dumpBusVectorStatsToFile( cca_vect );


	//CONNECTION::allSendersShutup();
	for ( unsigned int threadNb = 0; threadNb < sender_map.size(); threadNb++){
		CONNECTION *sender = CONNECTION::getObjPtr( threadNb );
		sender->storeRecordingToFile();
		sender->showStatistics();

		sender->showExtendedDiagnostics();

		//LOG( Log::DBG , canxHandle ) << "deleting sender " << threadNb;
		//delete( sender );
	}


	// cout << endl << "===shut up all senders===" << endl;
	// CONNECTION::allSendersShutup();
	// double free fasttop corruption because of dangling threads
	cout << endl << "===finished===" << endl;

	return 0;
}
