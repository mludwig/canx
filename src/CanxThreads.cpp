/*
 * CanxThreads.cpp
 *
 *  Created on: Aug 31, 2017
 *      Author: mludwig
 */

#include <CanLibLoader.h>

#include "CanxThreads.h"

#include "../include/connection.h"


using namespace connection_ns;

namespace canxthreads_ns {


/* static */ int CanxThreads::s_instanceCounter = 0;
/* static */ string CanxThreads::ms_synchronizationMessage = "";

/* static */ CanModule::CanLibLoader *CanxThreads::s_libloader = NULL;
/* static */ vector<string> CanxThreads::ms_openCanPorts;

/* static */ LogItInstance *CanxThreads::st_logIt = NULL;
/* static */ Log::LogComponentHandle CanxThreads::st_canxHandle = 0;
/* static */ Log::LogComponentHandle CanxThreads::st_canModuleHandle = 0;


/* static */ bool CanxThreads::isCanPortOpen( string port ){
	for ( unsigned int i = 0; i < CanxThreads::ms_openCanPorts.size(); i++ ){
		if ( port == CanxThreads::ms_openCanPorts[ i ] ) {
			LOG( Log::DBG, st_canxHandle ) << "already open port= " << port;
			return( true );
		}
	}
	LOG( Log::DBG ) << "not yet open port= " << port;
	return( false );
}
/* static */ void CanxThreads::addOpenPort( string port ){
	LOG( Log::DBG, st_canxHandle ) << "adding port= " << port;
	CanxThreads::ms_openCanPorts.push_back( port );
}

CanxThreads::CanxThreads() {
	s_threadLaunched = false;
	s_instanceNbCanx = CanxThreads::s_instanceCounter;
	s_stop = false;
	_clearThreadData();
	s_threadData.objIndex = s_instanceNbCanx;
	CanxThreads::st_logIt = LogItInstance::getInstance();
	CanxThreads::st_logIt->getComponentHandle( "CANX", CanxThreads::st_canxHandle );
	CanxThreads::st_logIt->getComponentHandle(CanModule::LogItComponentName, CanxThreads::st_canModuleHandle );
	CanxThreads::ms_synchronizationMessage.clear();
	CanxThreads::s_instanceCounter++;
}


CanxThreads::~CanxThreads() {
  	LOG(Log::TRC, st_canxHandle) << "thread: closing " << s_threadData.objIndex;
}

void CanxThreads::_clearThreadData( void ){
	s_threadData.sender_enabled = true;
	s_threadData.receiver_enabled = true;
	s_threadData.vendor = VENDOR_UNKNOWN;
	s_threadData.anagate.highSpeed = 0;
	s_threadData.anagate.operationalMode = 0;
	s_threadData.anagate.termination = 0;
	s_threadData.anagate.syncMode = 0;
	s_threadData.anagate.timeStamp = 0;

	//threadData.systec.dummy = 0;
	s_threadData.threadID = "unknown";
	s_threadData.objIndex = -1;

	s_threadData.connection.canLport = -1;
	s_threadData.connection.canPport = -1;
	s_threadData.connection.ipNumber = "192.168.1.1";
	s_threadData.connection.socketName = "unknown";
	s_threadData.connection.usbport = -1;

	s_threadData.connection_mirror.canLport = -1;
	s_threadData.connection_mirror.canPport = -1;
	s_threadData.connection_mirror.ipNumber = "192.168.1.1";
	s_threadData.connection_mirror.socketName = "unknown";
	s_threadData.connection_mirror.usbport = -1;
	s_threadData.connection_mirror.mirrorIncrementMsgId = false;

	s_threadData.msgGenerationType = MSG_COUNTING;
	s_threadData.canIdType = CANID_STANDARD;
	s_threadData.bitrate = CANBR_125kbs;
	s_threadData.sbitrate = "CANBR_125kbs";
	s_threadData.receiver_delay_us = 0;
	s_threadData.receiverIsRecorder = false;
	s_threadData.receiverIsMirror = false;
	s_threadData.nbMsgToGeneratePerTick = 1;

	// for generated messages: counting, fixed, random (NOT sequentially
	// specified) we can slow down. For sequentially specified messages
	// each frame can have it's own delay
	s_threadData.sender_delay_us = 0;


}

/**
 * systec16 has 2 USB ports: usb0[can0..7] and usb1 [can8..15]
 * we specify the usb [0..1] and the can [0..7] port, and have to calculate the logical can port therefore which
 * in this case is from 0..15
 */
string CanxThreads::getSystecSocketName( int usbport /* 0..1 */, int canPport /* physical 0..7 */){
	ostringstream convert;
#ifdef _WIN32
	convert << canPport;
	LOG(Log::TRC, st_canxHandle) << "getSystecSocketName: for usbport= " << usbport
			<< " canPport= " << canPport << " the systec socket is st:" << convert.str();
	return( "st:" + convert.str() );
#else
	int n = usbport * 8 + canPport;
	convert << "can" << n;
	LOG(Log::TRC, st_canxHandle) << "getSystecSocketName: for usbport= " << usbport
			<< " canPport= " << canPport << " the systec socket is sock:" << convert.str();
	return( "sock:" + convert.str() );
#endif
}

/**
 * PCAN USB-Pro has 2 CAN ports
 * we have to deal with the udev remapping for the device:
 */
string CanxThreads::getPeakSocketName( int usbport /* 0..1 */, int canPport /* physical 0..7 */, int deviceId ){
	ostringstream convert;
#ifdef _WIN32
	convert << canPport;
	LOG(Log::TRC, st_canxHandle) << "getPeakSocketName: for usbport= " << usbport
			<< " canPport= " << canPport << " the peak socket is pk:" << convert.str();
	return( "pk:" + convert.str() );
#else
	int n = usbport * 2 + canPport;
	convert << "can" << n << ":device" << deviceId;
	LOG(Log::TRC, st_canxHandle) << "getPeakSocketName: for usbport= " << usbport
			<< " canPport= " << canPport << " the peak socket is sock:" << convert.str();
	return( "sock:" + convert.str() );
#endif
}


string CanxThreads::codeBR_2_string( CanBitrates_t br ){
	std::string sbitrate = "125000";	// just  silly default
	switch ( br ){
	case CANBR_1000kbs: sbitrate = "1000000"; break;
	case CANBR_800kbs: sbitrate = "800000"; break;
	case CANBR_500kbs: sbitrate = "500000"; break;
	case CANBR_250kbs: sbitrate = "250000"; break;
	case CANBR_125kbs: sbitrate = "125000"; break;
	case CANBR_100kbs: sbitrate = "100000"; break;
	case CANBR_50kbs: sbitrate = "50000"; break;
	case CANBR_20kbs: sbitrate = "20000"; break;
	case CANBR_10kbs: sbitrate = "10000"; break;
	default: {
		LOG(Log::WRN, st_canxHandle) << " codeBR_2_string: unknown bitrate " << br << " found in configuration, assuming default 125000";
		break;
	}
	}
	//std::cout << __FILE__ << " " << __LINE__ << " br= " << br << " sbitrate= " << sbitrate << std::endl;
	return( sbitrate );
}

canxthreads_ns::CanxThreads::CanBitrates_t CanxThreads::codeString_2_BR( string sbr ){
	if ( sbr == "1000000") return( CANBR_1000kbs );
	if ( sbr == "800000") return( CANBR_800kbs );
	if ( sbr == "500000") return( CANBR_500kbs );
	if ( sbr == "250000") return( CANBR_250kbs );
	if ( sbr == "125000") return( CANBR_125kbs );
	if ( sbr == "100000") return( CANBR_100kbs );
	if ( sbr == "50000") return( CANBR_50kbs );
	if ( sbr == "20000") return( CANBR_20kbs );
	if ( sbr == "10000") return( CANBR_10kbs );
	LOG(Log::WRN, st_canxHandle) << " codeString_2_BR: unknown sbitrate " << sbr << " found in configuration, assuming default 0 (125000)";
	return( CANBR_125kbs );
}

/**
 * anagate ip number (instead of usb port)
 */
std::string CanxThreads::getAnagateIpNumber( ThreadData_t td  ){
	return( td.connection.ipNumber );
}

/**
 * for multiport modules A, B, ... we number them 0, 1, ...
 * even if they are strings
 * this is the physical port number on the module, NOT the
 * logical port number used by the driver
 * we prefix with "can" like for systec and peak
 */
std::string CanxThreads::getAnagatePport( ThreadData_t td  ){
	return( "can" + std::to_string( td.connection.canPport ));
}


// block until a message of data content wm has been received
void CanxThreads::waitForMessage( string wm ){
	LOG(Log::INF, st_canxHandle) << "_waitForMessage block until " << wm << " is received";
	while ( wm != CanxThreads::ms_synchronizationMessage ){
		canxthreads_ns::us_sleep( 100 );
	}
	LOG(Log::INF, st_canxHandle) << "_waitForMessage: received " << wm << " , continue";
}

bool CanxThreads::detectMessage( string wm ){
	LOG(Log::INF, st_canxHandle) << "_detectMessage " << wm;
	if ( wm == CanxThreads::ms_synchronizationMessage ) return( true );
	else return( false );
}
} /* namespace canxthreads_ns */
