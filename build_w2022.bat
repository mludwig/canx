
set CMAKEBIN="C:\Program Files\Microsoft Visual Studio\2022\Community\Common7\IDE\CommonExtensions\Microsoft\CMake\CMake\bin\cmake.exe"
set CANMODULE_AS_STATIC_AS_POSSIBLE=false
%CMAKEBIN% -S . -B ./build -DCMAKE_TOOLCHAIN_FILE=build_w2022s.cmake . -G "Visual Studio 17 2022" && cd ./build && %CMAKEBIN% --build .
